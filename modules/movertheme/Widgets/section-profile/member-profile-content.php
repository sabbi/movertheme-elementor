<?php

class Member_Profile {
	public static function basic_and_bio($that) {
		$that_opt = $that;
		$settings = $that_opt->get_settings_for_display();

		/*Meta Option Var*/
		$member_id          = $settings['select_team_member'];

		$post_args = array(
			'post_type'      => 'team',
			'post_status'    => 'publish',
			'posts_per_page' => '-1',
			'post__in' => array($member_id),
		);
		$loop       = new WP_Query( $post_args );

		ob_start(); ?>
		<div class="profile-glimps_wrap">
			<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<?php
					$designation                = get_post_meta( get_the_ID(), '_movertheme_member_designation', true );
					$institute                  = get_post_meta( get_the_ID(), '_movertheme_member_institute', true );
				?>
				<article class="profile-glimps">
					<h2 class="entry-title title-foc-md"><?php echo the_title(); ?></h2>
					<p class="text-foc-md member-designation"><?php echo $designation; ?></p>
					<p class="text-foc-md member-institute"><?php echo $institute; ?></p>
					<div class="stage-content-biog">
						<?php the_content(); ?>
					</div><!-- /.stage-content-biog -->
				</article>
			<?php endwhile; ?>
		</div>
		<?php $buffer = ob_get_flush();
	}
	public static function get_education_timeline($that) {
		$that_opt = $that;
		$settings = $that_opt->get_settings_for_display();

		/*Meta Option Var*/
		$member_id          = $settings['select_team_member'];

		$post_args = array(
			'post_type'      => 'team',
			'post_status'    => 'publish',
			'posts_per_page' => '-1',
			'post__in' => array($member_id),
		);
		$loop       = new WP_Query( $post_args );

		ob_start(); ?>
		<div class="education_timeline_wrap">
			<ol class="ol-timeline">
				<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
					<?php
					$education_timelines             = get_post_meta( get_the_ID(), '_movertheme_team_education', true );
					?>
					<?php foreach ($education_timelines as $education_timeline_id => $education_timeline ) :  ?>
						<li class="tl-item with-icon">
							<p><span class="item-section"><?php echo $education_timeline['_movertheme_education_year']; ?></span></p>
							<div class="content-wrapper">
								<h3 class="title"><?php echo $education_timeline['_movertheme_education_degree']; ?></h3>
								<div class="description"><?php echo $education_timeline['_movertheme_education_institute']; ?></div>
							</div>
						</li>
					<?php endforeach; ?>
				<?php endwhile; ?>
			</ol>
		</div>
		<?php $buffer = ob_get_flush();
	}
	public static function professional_experience($that) {
		$that_opt = $that;
		$settings = $that_opt->get_settings_for_display();

		/*Meta Option Var*/
		$member_id          = $settings['select_team_member'];

		$post_args = array(
			'post_type'      => 'team',
			'post_status'    => 'publish',
			'posts_per_page' => '-1',
			'post__in' => array($member_id),
		);
		$loop       = new WP_Query( $post_args );

		ob_start(); ?>
		<div class="appoint-timeline_wrap">
			<ol class="appoint-timeline  list-unstyled">
				<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
					<?php
					$professional_appoinments             = get_post_meta( get_the_ID(), '_movertheme_team_professional_appoinments', true );
					?>
					<?php foreach ($professional_appoinments as $professional_appoinment_id => $professional_appoinment ) :  ?>
						<li>
							<span class="year"><?php echo $professional_appoinment['_movertheme_pa_year'];?></span>
							<div class="appoint-meta">
								<h5 class="meta-title"><?php echo $professional_appoinment['_movertheme_pa_designation'];?></h5>
								<div class="meta-span"><?php echo $professional_appoinment['_movertheme_pa_institute'];?></div>
							</div>
						</li>
					<?php endforeach; ?>
				<?php endwhile; ?>
			</ol>
		</div>
		<?php $buffer = ob_get_flush();
	}
	public static function awards_prizes($that) {
		$that_opt = $that;
		$settings = $that_opt->get_settings_for_display();

		/*Meta Option Var*/
		$member_id          = $settings['select_team_member'];

		$post_args = array(
			'post_type'      => 'team',
			'post_status'    => 'publish',
			'posts_per_page' => '-1',
			'post__in' => array($member_id),
		);
		$loop       = new WP_Query( $post_args );

		ob_start(); ?>
		<div class="awards-list_wrap">
			<ul class="awards-list list-unstyled">
				<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
					<?php
						$awards_prizes             = get_post_meta( get_the_ID(), '_movertheme_team_awards_prizes', true );
					?>
					<?php foreach ($awards_prizes as $awards_prize_id => $awards_prize) :  ?>
						<li>
							<span class="year"><?php echo $awards_prize['_movertheme_award_prize_year']; ?></span>
							<div class="awards-meta">
								<h5 class="awards-title"><?php echo $awards_prize['_movertheme_award_prize_designation']; ?></h5>
								<div class="awards-meta"><span><?php echo $awards_prize['_movertheme_award_prize_organization']; ?></span></div>
							</div>
						</li>
					<?php endforeach; ?>
				<?php endwhile; ?>
			</ul>
		</div>
		<?php $buffer = ob_get_flush();
	}
}
