<?php
namespace MoverthemeElementor\Modules\Movertheme\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Border;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Elementor Fluent Form
 *
 * Elementor widget for fluent form
 *
 * @since 1.0.0
 */
 
class Movertheme_fluentformwidget extends Widget_Base {

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 * 
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'movertheme-fluent-form';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'ESX - Fluent Form', 'movertheme-elementor' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-form-horizontal';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'elementor-movertheme-widgets' ];
	}

	/**
	 * Retrieve the list of scripts the widget depended on.
	 *
	 * Used to set scripts dependencies required to run the widget.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget scripts dependencies.
	 */
	/*public function get_script_depends() {
		return [ 'elementor-kits-main'];
	}*/

	protected function getFluentFormNames(){
		if( !defined('FLUENTFORM')){
			return;
		}
		global $wpdb;
		$table_name = $wpdb->prefix . 'fluentform_forms';
		$fluent_cf_list = $wpdb->get_results(
			"	
				SELECT id, title 
				FROM `$table_name`
			"
		);

		$fluent_cf_value = array();

		if( $fluent_cf_list ){
			$fluent_cf_value[] = esc_html__( 'Select Contact Form', 'movertheme-elementor');

			foreach ( $fluent_cf_list as $value ) {
				$fluent_cf_value[$value->id] = $value->title;
			}
		} else {
			$fluent_cf_value[0] = esc_html__( 'No contact forms found', 'movertheme-elementor');
		}

		return $fluent_cf_value;

	}
	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'section_headings',
			[
				'label' => __( 'ESX - Fluent Form', 'movertheme-elementor' ),
			]
		);


		$this->add_control(
			'contact_form_id',
			[
				'label' => __( 'Select Contact Form', 'movertheme-elementor' ),
				'type' => Controls_Manager::SELECT,
				'options' => $this->getFluentFormNames(),
				'default' => '',
			]
		);

		$this->add_control(
			'title_tag',
			[
				'label' => __( 'Caption HTML Tag', 'movertheme-elementor' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'h1' => __( 'H1', 'movertheme-elementor' ),
					'h2' => __( 'H2', 'movertheme-elementor' ),
					'h3' => __( 'H3', 'movertheme-elementor' ),
					'h4' => __( 'H4', 'movertheme-elementor' ),
					'h5' => __( 'H5', 'movertheme-elementor' ),
					'h6' => __( 'H6', 'movertheme-elementor' ),
					'div' => __( 'Div', 'movertheme-elementor' ),
				],
				'default' => 'h2',
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment', 'movertheme-elementor' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'movertheme-elementor' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'movertheme-elementor' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'movertheme-elementor' ),
						'icon' => 'fa fa-align-right',
					],
					'justify' => [
						'title' => __( 'Justified', 'movertheme-elementor' ),
						'icon' => 'fa fa-align-justify',
					],
				],
				'default' => '',
				'selectors' => [
					'{{WRAPPER}}' => 'text-align: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_heading',
			[
				'label' => __( 'ESX - Form Header', 'movertheme-elementor' ),
			]
		);

		$this->add_control(
			'title_style',
			[
				'label' => __( 'Title Style', 'movertheme-elementor' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'style-1',
				'options' => [
					'style-1'  => __( 'Style 1', 'movertheme-elementor' ),
					'style-2' => __( 'Style 2', 'movertheme-elementor' ),
				],
			]
		);
		$this->add_control(
			'title',
			[
				'label' => __( 'Title:', 'movertheme-elementor' ),
				'type' => Controls_Manager::TEXTAREA,
				'default' => __('Send A Message', 'movertheme-elementor'),
				'placeholder' => __( 'Enter your title', 'movertheme-elementor' ),
				'label_block' => true,
				'dynamic' => [
					'active' => true,
				],
			]
		);
		$this->add_control(
			'title_before',
			[
				'label' => __( 'Title Before Text:', 'movertheme-elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => __('Enter your email below &', 'movertheme-elementor'),
				'placeholder' => __( 'Enter text for before title ', 'movertheme-elementor' ),
				'dynamic' => [
					'active' => true,
				],
				'condition' => [
					'title_style!' => 'style-1',
				],
			]
		);
		$this->add_control(
			'title_after',
			[
				'label' => __( 'Title After Text:', 'movertheme-elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => __('We promise, We will not spam', 'movertheme-elementor'),
				'placeholder' => __( 'Enter text for after title ', 'movertheme-elementor' ),
				'dynamic' => [
					'active' => true,
				],
			]
		);
		$this->end_controls_section();



		$this->_title_style();
		$this->_title_before_style();
		$this->_title_after_style();

		$this->start_controls_section(
			'section_fluent_form_input_style',
			[
				'label' => __( 'Input', 'movertheme-elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'fluent_form_input_typography',
				'selector' => '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-el-input--content input',
			]
		);

		$this->add_responsive_control(
            'fluent_form_input_height',
            [
                'label' => __( 'Height', 'movertheme-elementor' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%', 'em' ],
                'range' => [
                    'px' => [
                        'min' => 30,
                        'max' => 150,
                    ],
                ],
                'default' => [
					'unit' => 'px',
					'size' => 56,
				],
                'selectors' => [
                    '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-el-input--content input' => 'height: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

		$this->add_responsive_control(
            'fluent_form_input_padding',
            [
                'label' => __('Padding', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-el-input--content input' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'fluent_form_input_margin',
            [
                'label' => __('Margin', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-el-input--content input' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

		$this->start_controls_tabs( 'tabs_input_style' );

		$this->start_controls_tab(
			'tab_input_normal',
			[
				'label' => __( 'Normal', 'movertheme-elementor' ),
			]
		);

		$this->add_control(
            'fluent_form_input_background_color',
            [
                'label' => __( 'Background Color', 'movertheme-elementor' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-el-input--content input' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'input_border',
				'selector' => '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-el-input--content input',
			]
		);

        $this->add_control(
			'fluent_form_input_border_radius',
			[
				'label' => __( 'Border Radius', 'movertheme-elementor' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-el-input--content input' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

        $this->end_controls_tab();

        $this->start_controls_tab(
			'tab_input_hover',
			[
				'label' => __( 'Hover', 'movertheme-elementor' ),
			]
		);

		$this->add_control(
            'fluent_form_input_hover_background_color',
            [
                'label' => __( 'Background Color', 'movertheme-elementor' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-el-input--content input:hover' => 'background-color: {{VALUE}};',
                ],
            ]
        );

         $this->add_control(
			'fluent_form_input_hover_border_color',
			[
				'label' => __( 'Border Color', 'movertheme-elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-el-input--content input:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

        $this->add_control(
			'fluent_form_input_hover_border_radius',
			[
				'label' => __( 'Border Radius', 'movertheme-elementor' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-el-input--content input:hover' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

        $this->end_controls_tab();

        $this->end_controls_tabs();

		$this->end_controls_section();


		$this->start_controls_section(
			'section_fluent_form_textarea_style',
			[
				'label' => __( 'Textarea', 'movertheme-elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'fluent_form_textarea_typography',
				'selector' => '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-el-input--content textarea',
			]
		);

		$this->add_responsive_control(
            'fluent_form_textarea_height',
            [
                'label' => __( 'Height', 'movertheme-elementor' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%', 'em' ],
                'range' => [
                    'px' => [
                        'min' => 50,
                        'max' => 500,
                    ],
                ],
                'default' => [
					'unit' => 'px',
					'size' => 150,
				],
                'selectors' => [
                    '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-el-input--content textarea' => 'height: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

		$this->add_responsive_control(
            'fluent_form_textarea_padding',
            [
                'label' => __('Padding', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-el-input--content textarea' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'fluent_form_textarea_margin',
            [
                'label' => __('Margin', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-el-input--content textarea' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->start_controls_tabs( 'tabs_textarea_style' );

		$this->start_controls_tab(
			'tab_textarea_normal',
			[
				'label' => __( 'Normal', 'movertheme-elementor' ),
			]
		);

		$this->add_control(
            'fluent_form_textarea_background_color',
            [
                'label' => __( 'Background Color', 'movertheme-elementor' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-el-input--content textarea' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'textarea_border',
				'selector' => '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-el-input--content textarea',
			]
		);

        $this->add_control(
			'fluent_form_textarea_border_radius',
			[
				'label' => __( 'Border Radius', 'movertheme-elementor' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-el-input--content textarea' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_textarea_hover',
			[
				'label' => __( 'Hover', 'movertheme-elementor' ),
			]
		);
		$this->add_control(
            'fluent_form_textarea_hover_background_color',
            [
                'label' => __( 'Background Color', 'movertheme-elementor' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-el-input--content textarea:hover' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
			'fluent_form_textarea_hover_border_color',
			[
				'label' => __( 'Border Color', 'movertheme-elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-el-input--content textarea:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();
		$this->end_controls_section();

		$this->start_controls_section(
			'section_fluent_form_label_style',
			[
				'label' => __( 'Label', 'movertheme-elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

	   $this->add_control(
            'fluent_form_label_color',
            [
                'label' => __( 'Color', 'movertheme-elementor' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-el-input--label label' => 'color: {{VALUE}};',
                ],
            ]
        );

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'fluent_form_label_typography',
				'selector' => '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-el-input--label label',
			]
		);

		$this->add_responsive_control(
            'fluent_form_label_padding',
            [
                'label' => __('Padding', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-el-input--label label' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'fluent_form_label_margin',
            [
                'label' => __('Margin', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-el-input--label label' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

		$this->end_controls_section();

		$this->start_controls_section(
			'section_fluent_form_select_style',
			[
				'label' => __( 'Select', 'movertheme-elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
            'fluent_form_select_height',
            [
                'label' => __( 'Height', 'movertheme-elementor' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%', 'em' ],
                'range' => [
                    'px' => [
                        'min' => 30,
                        'max' => 150,
                    ],
                ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-el-input--content select.ff-el-form-control:not([size]):not([multiple])' => 'height: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

  		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'select_border',
				'selector' => '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-el-input--content select.ff-el-form-control:not([size]):not([multiple])',
			]
		);
		$this->end_controls_section();


		$this->start_controls_section(
			'section_fluent_form_submit_style',
			[
				'label' => __( 'Submit', 'movertheme-elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'fluent_form_button_typography',
				'selector' => '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-btn',
			]
		);

		$this->add_responsive_control(
            'fluent_form_button_height',
            [
                'label' => __( 'Height', 'movertheme-elementor' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%', 'em' ],
                'range' => [
                    'px' => [
                        'min' => 30,
                        'max' => 150,
                    ],
                ],
                'default' => [
					'unit' => 'px',
					'size' => 56,
				],
                'selectors' => [
                    '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-btn' => 'height: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'fluent_form_button_width',
            [
                'label' => __( 'Width', 'movertheme-elementor' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%', 'em' ],
                'range' => [
                    'px' => [
                        'min' => 10,
                        'max' => 100,
                    ],
                ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-btn' => 'width: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'fluent_form_submit_padding',
            [
                'label' => __('Padding', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-btn' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'fluent_form_submit_margin',
            [
                'label' => __('Margin', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-btn' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );


        $this->start_controls_tabs( 'tabs_submit_style' );

		$this->start_controls_tab(
			'tab_submit_normal',
			[
				'label' => __( 'Normal', 'movertheme-elementor' ),
			]
		);

		$this->add_control(
            'fluent_form_submit_color',
            [
                'label' => __( 'Color', 'movertheme-elementor' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-btn' => 'color: {{VALUE}} !important;',
                ],
            ]
        );

		$this->add_control(
            'fluent_form_submit_background_color',
            [
                'label' => __( 'Background Color', 'movertheme-elementor' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-btn' => 'background: {{VALUE}} !important;',
                ],
            ]
        );

        $this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'submit_border',
				'selector' => '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-btn',
			]
		);

        $this->add_control(
			'fluent_form_submit_border_radius',
			[
				'label' => __( 'Border Radius', 'movertheme-elementor' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-btn' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

        $this->end_controls_tab();

        $this->start_controls_tab(
			'tab_submit_hover',
			[
				'label' => __( 'Hover', 'movertheme-elementor' ),
			]
		);

		$this->add_control(
            'fluent_form_submit_hover_color',
            [
                'label' => __( 'Color', 'movertheme-elementor' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-btn:hover' => 'color: {{VALUE}};',
                ],
            ]
        );

		$this->add_control(
            'fluent_form_submit_hover_background_color',
            [
                'label' => __( 'Background Color', 'movertheme-elementor' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-btn:hover' => 'background: {{VALUE}} !important;',
                ],
            ]
        );

        $this->add_control(
			'fluent_form_submit_hover_border_color',
			[
				'label' => __( 'Border Color', 'movertheme-elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-btn:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

        $this->add_control(
			'fluent_form_submit_hover_border_radius',
			[
				'label' => __( 'Border Radius', 'movertheme-elementor' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .elementor_movertheme-contact-form .frm-fluent-form .ff-btn:hover' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

        $this->end_controls_tab();

        $this->end_controls_tabs();



		$this->end_controls_section();


	}
	protected function _title_style() {
		$this->start_controls_section(
			'section_fluent_form_title_style',
			[
				'label' => __( 'Title', 'movertheme-elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
            'fluent_form_title_color',
            [
                'label' => __( 'Color', 'movertheme-elementor' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .title' => 'color: {{VALUE}};',
                ],
            ]
        );

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'fluent_form_title_typography',
				'selector' => '{{WRAPPER}} .title',
			]
		);

		$this->add_responsive_control(
            'fluent_form_title_padding',
            [
                'label' => __('Padding', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} .title' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'fluent_form_title_margin',
            [
                'label' => __('Margin', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} .title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );
		$this->end_controls_section();
	}
	protected function _title_before_style() {
		$this->start_controls_section(
			'section_fluent_form_title_before_style',
			[
				'label' => __( 'Title Before Style', 'movertheme-elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
            'fluent_form_title_before_color',
            [
                'label' => __( 'Color', 'movertheme-elementor' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .title-before' => 'color: {{VALUE}};',
                ],
            ]
        );

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'fluent_form_title_before_typography',
				'selector' => '{{WRAPPER}} .title-before',
			]
		);

		$this->add_responsive_control(
            'fluent_form_title_before_padding',
            [
                'label' => __('Padding', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} .title-before' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'fluent_form_title_before_margin',
            [
                'label' => __('Margin', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} .title-before' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );
		$this->end_controls_section();
	}
	protected function _title_after_style() {
		$this->start_controls_section(
			'section_fluent_form_title_after_style',
			[
				'label' => __( 'Title After Style', 'movertheme-elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
            'fluent_form_title_after_color',
            [
                'label' => __( 'Color', 'movertheme-elementor' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .title-after' => 'color: {{VALUE}};',
                ],
            ]
        );

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'fluent_form_title_after_typography',
				'selector' => '{{WRAPPER}} .title-after',
			]
		);

		$this->add_responsive_control(
            'fluent_form_title_after_padding',
            [
                'label' => __('Padding', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} .title-after' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'fluent_form_title_after_margin',
            [
                'label' => __('Margin', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} .title-after' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );
		$this->end_controls_section();
	}
	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();
		$this->add_render_attribute( 'title', 'class', 'title' );
		$this->add_inline_editing_attributes( 'title', 'basic' );

		//title before content
		$this->add_render_attribute( 'title_before', 'class', 'title-before' );
		$this->add_inline_editing_attributes( 'title_before', 'basic' );
		$title_before_html = sprintf( '<%1$s %2$s>%3$s</%1$s>', 'div', $this->get_render_attribute_string( 'title_before' ),$settings['title_before'] );
        ($settings['title_style']=='style-2')? $title_before_html : $title_before_html = '';

		//title after content
		$this->add_render_attribute( 'title_after', 'class', 'title-after' );
		$this->add_inline_editing_attributes( 'title_after', 'basic' );
		$title_after_html = sprintf( '<%1$s %2$s>%3$s</%1$s>', 'div', $this->get_render_attribute_string( 'title_after' ),$settings['title_after'] );

			extract($settings);

		    global $wpdb;
		    $table_name = $wpdb->prefix . 'fluentform_forms';
		    $form = $wpdb->get_results(
		        "   
		            SELECT id, title 
		            FROM `$table_name`
		            WHERE id = '".esc_attr($contact_form_id)."' LIMIT 1
		        "
		    );
		?>

		<?php if( !empty($form) ) { ?>
		    <div class="elementor_movertheme-contact-form">

		        <?php if( $title ) : ?>

		        	<div class="title-group">
		        		<?php echo $title_before_html; ?>
		         			<h3 <?php echo $this->get_render_attribute_string( 'title' ); ?>><?php echo $title; ?></h3>
		         		<?php echo $title_after_html; ?>
		        	</div>
		        <?php endif; ?>

		    	<?php echo do_shortcode('[fluentform id="'.esc_attr($form[0]->id).'"]'); ?>
		    </div>
		<?php } else { ?>
		    <p class="select-cf"><?php esc_html__('Please select one of contact form for display.', 'movertheme-elementor') ?></p>
		<?php }
	}

}


