<?php
namespace MoverthemeElementor\Modules\Movertheme\Widgets;

// You can add to or remove from this list - it's not conclusive! Chop & change to fit your needs.
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Background;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Repeater;
use Elementor\Scheme_Typography;
use Elementor\Widget_Base;
use DateTime;
use WP_Query;
use Utils;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Movertheme_Book extends Widget_Base {

	/* Uncomment the line below if you do not wish to use the function _content_template() - leave that section empty if this is uncommented! */
	//protected $_has_template_content = false;

	public function get_name() {
		return 'movertheme-bookss';
	}

	public function get_title() {
		return __( 'Book Lists', 'movertheme-elementor' );
	}

	public function get_icon() {
		return ' eicon-posts-group';
	}

	public function get_categories() {
		return [ 'elementor-movertheme-widgets'];
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content Box', 'movertheme-elementor' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'book_list_background',
				'label' => __( 'Background', 'movertheme-elementor' ),
				'types' => [ 'classic', 'gradient', 'video' ],
				'selector' => '{{WRAPPER}} .moverthemebook_timeline-segment',
                'separator' => 'after',
			]
		);
		$this->add_control(
			'book_list_explore_text',
			[
				'label' => __( 'Explore Button', 'movertheme-elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Explore More', 'movertheme-elementor' ),
				'placeholder' => __( 'Type your title here', 'movertheme-elementor' ),
				'separator' => 'before',
			]
		);

		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings_for_display();
		$terms = get_terms( 'book_cat', array(
			'hide_empty' => 0,
		) );

		if( !empty( $terms ) ){
			$term_array = array();
			foreach ($terms as $term ) {
				$term_array[] = $term->name;
			}

			rsort($term_array);
			$year_array = array();

			foreach($term_array as $category){
				$year_array[] = $category;
			}
		} else{
			$year_array = array();
		} 
		?>

		<section class="book-timeline-section">
            <div class="moverthemebook_timeline-segment">
                <ul class="moverthemebook_timeline list-unstyled">
	                <?php foreach ( $year_array as $year ) :

                        $args = array(
                            'post_type'       => 'book',
                            'posts_per_page'  => -1,
                            'book_cat'   => $year,
                            'orderby' => 'ID',
                            'post_status' => 'publish',
                        );
                        $book_lists = new WP_Query( $args );
                        $post_counter = 0;
                        if( $book_lists->have_posts() ): ?>
                            <li><span class="year"><?php echo $year; ?></span>

                                <?php while ( $book_lists->have_posts() ) : $book_lists->the_post(); $post_counter++ ?>
                                    <?php
	                                    $book_description = get_post_meta( get_the_ID(), '_movertheme_book_description', true );
	                                    $book_author = get_post_meta( get_the_ID(), '_movertheme_book_author_name', true );
	                                    $book_links = get_post_meta( get_the_ID(), '_movertheme_book_link', true );
                                        if( wp_get_attachment_image( get_post_meta( get_the_ID(), '_movertheme_book_image_id', true ), 'movertheme-team-thumbnail', false, array( 'class' => 'img-responsive', 'data-alt' => 'byu byu' ) ) ){
                                            $profile_image = wp_get_attachment_image( get_post_meta( get_the_ID(),'_movertheme_book_image_id', true ), 'movertheme-team-thumbnail', false, array( 'class' => 'img-responsive' ) );
                                        } else{
                                            $profile_image = '';
                                        }

                                        if( get_post_meta( get_the_ID() , '_movertheme_book_link_target', true) ){
                                            $book_link_target = get_post_meta(get_the_ID() ,'_movertheme_book_link_target', true);
                                        } else{
                                            $book_link_target = '';
                                        }

                                        $link_target = ( $book_link_target == 0 ) ? '_self' : '_blank';
                                    ?>

                                    <ul class="book-list list-unstyled">
                                        <?php $onexpan = ($post_counter>4) ? ' class=onexpan' : '';  ?>
                                        <li <?php echo esc_attr( $onexpan ); ?>>
                                            <figure>
                                                <?php echo $profile_image; ?>
                                            </figure>
                                            <div class="book-list-meta">
                                                <h3 class="book-list-title"><a href="<?php echo esc_attr($book_links); ?>" <?php echo esc_attr($link_target); ?>><?php echo the_title(); ?></a> </h3>
                                                <div class="book-list-brand"><em><?php echo $book_description; ?></em></div>
                                                <p class="book-author"><?php echo $book_author ?></p>
                                            </div>
                                        </li>
                                    </ul>
                                <?php endwhile; ?>

	                            <?php if($post_counter>4) {  ?>
                                    <a href="#" class="btn btn-unsolemn btn-expand" data-text="<?php echo esc_attr($settings['book_list_explore_text']);?>"><?php echo _e($settings['book_list_explore_text'],'movertheme-elementor'); ?></a>
	                            <?php } ?>
                            </li>
                        <?php endif;
                    endforeach; ?>
                </ul>
            </div>
        </section><!-- /.book-timeline -->
		<?php
	}

	protected function _content_template() {
	}

}
