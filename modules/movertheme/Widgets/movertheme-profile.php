<?php
namespace MoverthemeElementor\Modules\Movertheme\Widgets;

// You can add to or remove from this list - it's not conclusive! Chop & change to fit your needs.
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Background;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Repeater;
use Elementor\Scheme_Typography;
use Elementor\Widget_Base;
use MoverthemeElementor\Classes\Utils;
use Member_Profile;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

require ELEMENTOR_MOVERTHEME_MODULES_PATH.'movertheme/widgets/section-profile/member-profile-content.php';

class Movertheme_Profile extends Widget_Base {

	/* Uncomment the line below if you do not wish to use the function _content_template() - leave that section empty if this is uncommented! */
	//protected $_has_template_content = false; 
	
	public function get_name() {
		return 'movertheme-profile';
	}

	public function get_title() {
		return __( 'Widget Profile Section', 'movertheme-elementor' );
	}

	public function get_icon() {
		return 'eicon-time-line';
	}

	public function get_categories() {
		return [ 'elementor-movertheme-widgets'];
	}
	
	protected function _register_controls() {
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'movertheme-elementor' ),
			]
		);


		$this->add_control(
			'stage_title',
			[
				'label' => __( 'Box Title', 'movertheme-elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Team Profile', 'movertheme-elementor' ),
			]
		);
		$this->add_control(
			'select_team_member',
			[
				'label' => __( 'Select Member:', 'movertheme-elementor' ),
				'type' => Controls_Manager::SELECT,
				'multiple' => true,
				'options' => Utils::movertheme_get_postId_list_by_team(),
				'default' => [ 'NULL'],
			]
		);
		$this->add_control(
			'select_team_option',
			[
				'label' => __( 'Select Profile Section:', 'movertheme-elementor' ),
				'type' => Controls_Manager::SELECT,
				'multiple' => true,
				'options' => Utils::movertheme_get_team_option_fields(),
				'default' => [ 'basic_and_bio'],
			]
		);
		
		$this->add_control(
			'view',
			[
				'label' => __( 'View', 'movertheme-elementor' ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);
		
		$this->end_controls_section();
	}
		
	protected function render() {
		$settings = $this->get_settings_for_display();

		if (!empty($settings['stage_title'])) {
			echo '<h4>'.wp_kses_post($settings['stage_title']).'</h4>';
		}
		?>

        <?php if($settings['select_team_option'] == 'basic_and_bio') :
			Member_Profile::basic_and_bio($this);

        elseif($settings['select_team_option'] == 'education_timeline'):
			Member_Profile::get_education_timeline($this);

        elseif($settings['select_team_option'] == 'pro_experience'):
			Member_Profile::professional_experience($this);

        elseif($settings['select_team_option'] == 'awards_prizes'):
			Member_Profile::awards_prizes($this);

        endif; ?>
        <?php
	}

}
