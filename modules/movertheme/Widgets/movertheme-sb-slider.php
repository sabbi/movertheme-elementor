<?php
namespace MoverthemeElementor\Modules\Movertheme\Widgets;

// You can add to or remove from this list - it's not conclusive! Chop & change to fit your needs.
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Background;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Repeater;
use Elementor\Scheme_Typography;
use Elementor\Widget_Base;
use DateTime;
use Elementor\Pluign;
use Elementor\Icons_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Movertheme_Sb_slider extends Widget_Base {

	/* Uncomment the line below if you do not wish to use the function _content_template() - leave that section empty if this is uncommented! */
	//protected $_has_template_content = false;

	public function get_name() {
		return 'movertheme-slider';
	}

	public function get_title() {
		return __( 'Movertheme Header Slider', 'movertheme-elementor' );
	}

	public function get_icon() {
		return 'eicon-slideshow';
	}

	public function get_script_depends() {
		return [
			'elementor-movertheme-common-js',
		];
	}

	public function get_categories() {
		return [ 'elementor-movertheme-widgets'];
	}




	protected function _register_controls() {
		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content', 'movertheme-elementor' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

		$repeater = new Repeater();


		$repeater->add_control(
			'sb_sliders_slider_title', [
				'label' => __( 'Slider Title', 'movertheme-elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Designs Xesilay Kawser' , 'movertheme-elementor' ),
				'label_block' => true,

			]
		);

		$repeater->add_control(
			'sb_sliders_subtitle', [
				'label' => __( 'Sub Title', 'movertheme-elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Be Easy For You' , 'movertheme-elementor' ),
				'show_label' => true,
				'separator'     => 'after',
			]
		);

		$repeater->add_control(
			'sb_sliders_cta_button_text', [
				'label' => __( 'CTA Button Text', 'movertheme-elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Go with us' , 'movertheme-elementor' ),
				'label_block' => true,
				'separator'     => 'before',
			]
		);


		$repeater->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'background',
				'label' => __( 'Background', 'plugin-domain' ),
				'types' => [ 'classic'],
				'selector' => '{{WRAPPER}} {{CURRENT_ITEM}}',
			]
		);

		$this->add_control(
			'sb_sliders_data',
			[
				'label' => __( 'Slider Items', 'movertheme-elementor' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'sb_sliders_slider_title' => __( 'Desilay Smith', 'movertheme-elementor' ),
						'sb_sliders_author_position' => __( 'CTO of London Mover', 'movertheme-elementor' ),
						'sb_sliders_subtitle' => __( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ac ornare odio, id laoreet est. Nullam fringilla mi in nulla malesuada, elementum molestie nisi condimentum. Nam maximus, purus consequat pharetra congue. Ornare odio, id laoreet est. Nullam fringilla mi in nulla malesuada.', 'movertheme-elementor' ),
						'sb_sliders_icon' => '',
						'sb_sliders_time' => __( '5:50 pm', 'movertheme-elementor' ),
					],
				],
				'title_field' => '{{{ sb_sliders_slider_title }}}',
			]
		);


		$this->end_controls_section();
		$this->_sb_sliders_carousel_option();

		// Add your widget/element styling controls here! - Below is an example style option
		$this->_sb_sliders_wrapper_box_style();
		$this->_sb_sliders_slider_title_style();
		$this->_sb_sliders_subtitle_style();
		$this->_sb_sliders_action_calle_style();
	}


	/*Style Elements Title*/
	protected function _sb_sliders_carousel_option() {
		$css_scheme_sb_sliders_slider_title = apply_filters(
			'movertheme-elementor/movertheme-sb_sliders/css-scheme/sb_sliders_carousel_option',
			array(
				'carousel_option'         => '{{WRAPPER}} .moverthemesite-header-meta-sliders .seq--kawsa .seq-step-item .seq-carousel_option',
			)
		);
		$this->start_controls_section(
			'sb_sliders_carousel_option',
			[ 'label' 	=> __( 'Carousel Options', 'movertheme-elementor' ), 'tab' => Controls_Manager::TAB_CONTENT,]
		);

		$this->add_control(
			'sb_sliders_slideautoplay',
			[
				'label' => __( 'Enable Auto Play', 'movertheme-elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => __( 'Yes', 'movertheme-elementor' ),
				'label_off' => __( 'No', 'movertheme-elementor' ),
				'frontend_available' => true,
				'return_value' => true,
				'default' => true,

			]
		);
		$this->add_control(
			'slideautoplayinterval',
			[
				'label' => __( 'Interval Time', 'movertheme-elementor' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 100,
						'max' => 15000,
						'step' => 1,
					],
				],
				'frontend_available' => true,
				'selectors' => [
					'{{WRAPPER}} .box' => 'slideautoplay-interval: {{SIZE}};',
				],
				'condition' => [
					'sb_sliders_slideautoplay' => 'true',
				],

			]
		);
		$this->add_control(
			'sb_sliders_hover_play',
			[
				'label' => __( 'Pause On Hover', 'movertheme-elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => __( 'Yes', 'movertheme-elementor' ),
				'label_off' => __( 'No', 'movertheme-elementor' ),
				'frontend_available' => true,
				'return_value' => true,
				'default' => true,
			]
		);

		/*$this->add_control(
			'sb_sliders_hover_play',
			[
				'label' => __( 'Pause On Hover', 'movertheme-elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => __( 'Yes', 'movertheme-elementor' ),
				'label_off' => __( 'No', 'movertheme-elementor' ),
				'return_value' => true,
				'default' => true,
			]
		);
		$this->add_control(
			'sb_sliders_hover_play',
			[
				'label' => __( 'Pause On Hover', 'movertheme-elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => __( 'Yes', 'movertheme-elementor' ),
				'label_off' => __( 'No', 'movertheme-elementor' ),
				'return_value' => true,
				'default' => true,
			]
		);*/
		$this->end_controls_section();
	}
	/*Style Elements Title*/
	protected function _sb_sliders_slider_title_style() {
		$css_scheme_sb_sliders_slider_title = apply_filters(
			'movertheme-elementor/movertheme-sb_sliders/css-scheme/sb_sliders_slider_title',
			array(
				'title'         => '{{WRAPPER}} .moverthemesite-header-meta-sliders .seq--kawsa .seq-step-item .seq-title',
			)
		);
		$this->start_controls_section(
			'sb_sliders_slider_title_style',
			[ 'label' 	=> __( 'Slider Title', 'movertheme-elementor' ), 'tab' 		=> Controls_Manager::TAB_STYLE,]
		);

		$this->add_control(
			'elementor_movertheme_title_color',
			[
				'label' => __( 'Color', 'movertheme-elementor' ),
				'type' => Controls_Manager::COLOR,

				'scheme'	=> [
					'type'	=> Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],

				'selectors' => [
					$css_scheme_sb_sliders_slider_title['title'] => 'color: {{VALUE}};',
					$css_scheme_sb_sliders_slider_title['title'].' a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'elementor_movertheme_title_typography',
				'selector' => $css_scheme_sb_sliders_slider_title['title'],
			]
		);
		$this->add_group_control(
			Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'elementor_movertheme_title_text_shadow',
				'label' => __( 'Text Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_sb_sliders_slider_title['title'],
			]
		);
		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'elementor_movertheme_title_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => $css_scheme_sb_sliders_slider_title['title'],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'elementor_movertheme_title_shadow',
				'label' => __( 'Box Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_sb_sliders_slider_title['title'],
			]
		);

		$this->add_responsive_control(
			'elementor_movertheme_title_spacing',
			[
				'label' => __('Spacing', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					$css_scheme_sb_sliders_slider_title['title'] => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);
		$this->end_controls_section();
	}
	/*Style Elements content*/
	protected function _sb_sliders_subtitle_style() {
		$css_scheme_sb_sliders_subtitle = apply_filters(
			'movertheme-elementor/movertheme-sb_sliders/css-scheme/sb_sliders_subtitle',
			array(
				'content'         => '{{WRAPPER}} .sabbi-site-header-meta-sliders .seq--kawsa .seq-step-item .seq-meta-text',
			)
		);

		$this->start_controls_section(
			'sb_sliders_subtitle_style',
			[ 'label' 	=> __( 'Slider Content', 'movertheme-elementor' ), 'tab' 		=> Controls_Manager::TAB_STYLE,]
		);

		$this->add_control(
			'elementor_movertheme_content_color',
			[
				'label' => __( 'Color', 'movertheme-elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					$css_scheme_sb_sliders_subtitle['content'] => 'color: {{VALUE}};',
					$css_scheme_sb_sliders_subtitle['content'].' a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'elementor_movertheme_content_typography',
				'selector' => $css_scheme_sb_sliders_subtitle['content'],
			]
		);
		$this->add_group_control(
			Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'elementor_movertheme_content_text_shadow',
				'label' => __( 'Text Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_sb_sliders_subtitle['content'],
			]
		);
		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'elementor_movertheme_content_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => $css_scheme_sb_sliders_subtitle['content'],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'elementor_movertheme_content_shadow',
				'label' => __( 'Box Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_sb_sliders_subtitle['content'],
			]
		);

		$this->add_responsive_control(
			'elementor_movertheme_content_spacing',
			[
				'label' => __('Spacing', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					$css_scheme_sb_sliders_subtitle['content'] => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);
		$this->end_controls_section();
	}
	/*Action Button link*/
	protected function _sb_sliders_action_calle_style() {
		$css_scheme_sb_sliders_action_button = apply_filters(
			'movertheme-elementor/movertheme-sb-sliders/css-scheme/sb_sliders_action_calle',
			array(
				'sb_sliders_action_calle'         => ' {{WRAPPER}} .moverthemesite-header-meta-sliders .seq--kawsa .seq-step-item .sb_sliders-action_calli-button',
				'sb_sliders_action_calle_hover'         => ' {{WRAPPER}} .moverthemesite-header-meta-sliders .seq--kawsa .seq-step-item .sb_sliders-action_calli-button:hover',
			)
		);
		$this->start_controls_section(

			'sb_sliders_style',
			[
				'label' 	=> __( 'Call To Action Button', 'movertheme-elementor' ),
				'tab' 		=> Controls_Manager::TAB_STYLE,
			]
		);
		$this->start_controls_tabs( 'sb_sliders_action_calle_style' );

		$this->start_controls_tab(
			'sb_sliders_btn_normal',
			[
				'label' => __( 'Normal', 'movertheme-elementor' ),
			]
		);

		$this->add_control(
			'sb_sliders_btn_color',
			[
				'label' => __( 'Color', 'movertheme-elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					$css_scheme_sb_sliders_action_button['sb_sliders_action_calle'] => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'sb_sliders_btn_typography',
				'selector' => $css_scheme_sb_sliders_action_button['sb_sliders_action_calle'],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'sb_sliders_btn_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => $css_scheme_sb_sliders_action_button['sb_sliders_action_calle'],
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'sb_sliders_btn_background',
				'label' => __( 'Background', 'movertheme-elementor' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => $css_scheme_sb_sliders_action_button['sb_sliders_action_calle'],
			]
		);
		$this->add_group_control(
			Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'sb_sliders_btn_text_shadow',
				'label' => __( 'Text Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_sb_sliders_action_button['sb_sliders_action_calle'],
			]
		);
		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'sb_sliders_btn_shadow',
				'label' => __( 'Box Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_sb_sliders_action_button['sb_sliders_action_calle'],
			]
		);
		$this->end_controls_tab();
		# End Normal Style Tab
		$this->start_controls_tab(
			'sb_sliders_btn_hover',
			[
				'label' => __( 'Hover', 'movertheme-elementor' ),
			]
		);

		$this->add_control(
			'sb_sliders_btn_hover_color',
			[
				'label' => __( 'Color', 'movertheme-elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					$css_scheme_sb_sliders_action_button['sb_sliders_action_calle_hover'] => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'sb_sliders_btn_text_shadow_hover',
				'label' => __( 'Text Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_sb_sliders_action_button['sb_sliders_action_calle_hover'],
			]
		);
		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'sb_sliders_btn_hover_shadow',
				'label' => __( 'Box Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_sb_sliders_action_button['sb_sliders_action_calle_hover'],
			]
		);
		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'sb_sliders_btn_hover_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => $css_scheme_sb_sliders_action_button['sb_sliders_action_calle_hover'],
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'sb_sliders_btn_hover_background',
				'label' => __( 'Background', 'movertheme-elementor' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => $css_scheme_sb_sliders_action_button['sb_sliders_action_calle_hover'],
			]
		);
		$this->end_controls_tab();
		# End Hover Style Tab
		$this->end_controls_tabs();
		#End Tabs


		$this->add_responsive_control(
			'sb_sliders_btn_padding',
			[
				'label' => __('Padding', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					$css_scheme_sb_sliders_action_button['sb_sliders_action_calle'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_responsive_control(
			'sb_sliders_btn_margin',
			[
				'label' => __('Margin', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					$css_scheme_sb_sliders_action_button['sb_sliders_action_calle'] => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();
	}

	/*Each repeter item box*/

	/*Slider Box*/
	protected function _sb_sliders_wrapper_box_style() {
		$css_scheme_sb_sliders_wrapper_box = apply_filters(
			'movertheme-elementor/movertheme-sb_sliders/css-scheme/sb_sliders_wrapper_box',
			array(
				'sb_sliders_wrapper_box'         => ' {{WRAPPER}} .moverthemesite-header-meta-sliders .seq--kawsa',
			)
		);
		$this->start_controls_section(

			'sb_sliders_wrapper_box_style',
			[
				'label' 	=> __( 'Slider Item Box', 'movertheme-elementor' ),
				'tab' 		=> Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'box_height',
			[
				'label' => __( 'Height', 'movertheme-elementor' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'vh','px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1500,
					],
				],
				'devices' => [ 'desktop', 'tablet', 'mobile' ],
				'desktop_default' => [
					'size' => 85,
					'unit' => 'vh',
				],
				'tablet_default' => [
					'size' => 55,
					'unit' => 'vh',
				],
				'mobile_default' => [
					'size' => 55,
					'unit' => 'vh',
				],
				'selectors' => [
					$css_scheme_sb_sliders_wrapper_box['sb_sliders_wrapper_box'] => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'min_height',
			[
				'label' => __( 'Min Height', 'movertheme-elementor' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'vh','px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1500,
					],
				],
				'devices' => [ 'desktop', 'tablet', 'mobile' ],
				'desktop_default' => [
					'size' => 600,
					'unit' => 'px',
				],
				'tablet_default' => [
					'size' => 420,
					'unit' => 'px',
				],
				'mobile_default' => [
					'size' => 385,
					'unit' => 'px',
				],
				'selectors' => [
					$css_scheme_sb_sliders_wrapper_box['sb_sliders_wrapper_box'] => 'min-height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'sb_sliders_wrapper_box_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => $css_scheme_sb_sliders_wrapper_box['sb_sliders_wrapper_box'],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'sb_sliders_wrapper_box_shadow',
				'label' => __( 'Box Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_sb_sliders_wrapper_box['sb_sliders_wrapper_box'],
			]
		);

		$this->add_responsive_control(
			'sb_sliders_wrapper_box_btn_padding',
			[
				'label' => __('Padding', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					$css_scheme_sb_sliders_wrapper_box['sb_sliders_wrapper_box'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_responsive_control(
			'sb_sliders_wrapper_box_btn_margin',
			[
				'label' => __('Margin', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					$css_scheme_sb_sliders_wrapper_box['sb_sliders_wrapper_box'] => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();
	}


	protected function render() {
		$settings = $this->get_settings_for_display();
		$sb_sliders_id = $this->get_id();
		$id_int = substr( $this->get_id_int(), 0, 3 );

		$sb_sliders_slideautoplay = $settings['sb_sliders_slideautoplay'];
		empty($sb_sliders_slideautoplay)? $sb_sliders_slideautoplay = false : $sb_sliders_slideautoplay;

		//$slideautoplayinterval = $settings['slideautoplayinterval']['size'];
		empty($slideautoplayinterval)? $slideautoplayinterval = '' : $slideautoplayinterval;

		//$sb_sliders_loop_data = $settings['sb_sliders_data'];
		//$question_answer_title_setting_key = $this->get_repeater_setting_key( 'sb_sliders_slider_title', 'sliders-slider', $index );

		//var_dump(Plugin::$instance->widgets_manager->get_widget_types());
		?>

        <section class="movertheme-sliders-wrap">
            <!-- Slider main container -->
            <div class="swiper-container movertheme-sliders-container">
                <div class="swiper-wrapper">
					<?php
					//var_dump($settings['question_answer_tabs']);
					foreach ( $settings['sb_sliders_data'] as $index => $item ) :

						$tab_count = $index + 1;
						/*Setup Slider Title*/
						$slider_title = $this->get_repeater_setting_key( 'sb_sliders_slider_title', 'sliders-slider', $index );
						$this->add_render_attribute( $slider_title, [
							'id' => 'headerslider-title-' .$id_int.'-'.++$index,
							'class' => [ 'headerslider-title'],
							'data-tab' => $index,
							'role' => 'tabpanel',
							'aria-labelledby' => 'headerslider-title-' . $id_int . $tab_count,
							'aria-label' => $item['sb_sliders_slider_title'],
						] );



						/*Setup Quote Icon*/
						$quote_icon = $this->get_repeater_setting_key( 'sb_sliders_quote_icon', 'sliders-slider', $index );
						$this->add_render_attribute( $quote_icon, [
							'id' => 'profile-name-' .$id_int.'-'.++$index,
							'class' => [ 'content-quout'],
						] );

						if (true) {
							$this->add_render_attribute( $slider_title, [
								'class' => [ 'flip-vr'],
							] );
						}
						if (true) {
							$this->add_render_attribute( $slider_title, [
								'class' => [ 'flip-hr'],
							] );
						}
						?>
                        <div class="swiper-slide movertheme-sliders-item <?php echo esc_attr('elementor-repeater-item-'.$item['_id']) ?>">
                            <div class="sliders-content-wrap">

                                <div class="sliders-content">
                                    <div <?php echo $this->get_render_attribute_string( $slider_title ); ?>>
                                        <h2  data-swiper-parallax-y="100"   data-swiper-parallax-opacity="0"  data-swiper-parallax-duration="800"><?php echo $item['sb_sliders_slider_title']; ?></h2>
                                        <div class="headerslider-sub-title"  data-swiper-parallax-y="8" data-swiper-parallax-duration="1000" data-swiper-parallax-opacity=".10" ><?php echo $item['sb_sliders_subtitle']; ?></div>
                                    </div>
                                    <div class="slider-cta_button-wrap">
                                        <div class="slider-cta_button"  data-swiper-parallax-x="-75" data-swiper-parallax-duration="1200"  data-swiper-parallax-opacity="0">
                                            <a class="slider-cta_button-anc btn" href="#">
                                                <span class="sp_text"><?php echo $item['sb_sliders_cta_button_text']; ?></span>
                                                <span class="elementor-button-icon elementor-align-icon-right">
                                                    <i aria-hidden="true" class="fas fa-arrow-alt-circle-right"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!--<blockquote class="sliders-content">

                                    <div class="content-text"><?php /*echo $item['sb_sliders_subtitle']; */?></div>

                                    <footer>
                                        <div class="testimoni-profile">
                                            <div <?php /*echo $this->get_render_attribute_string( $slider_title ); */?>>
                                                <cite><?php /*echo $item['sb_sliders_slider_title']; */?></cite>
                                            </div>
                                            <div class="profile-position"></div>
                                        </div>
                                    </footer>
                                </blockquote>-->
                            </div>
                        </div>
					<?php endforeach; ?>
                </div>
                <div class="swiper-pagination"></div>
            </div>
            <div class="swiper-navigation slider--swiper-navigation">
                <div class="slider--button-prev slider--swiper-navigation--button __icon-button swiper-button-prev">
                    <div class="__icon __svg">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 240.823 240.823"><path d="M57.633 129.007L165.93 237.268c4.752 4.74 12.451 4.74 17.215 0 4.752-4.74 4.752-12.439 0-17.179l-99.707-99.671 99.695-99.671c4.752-4.74 4.752-12.439 0-17.191-4.752-4.74-12.463-4.74-17.215 0L57.621 111.816c-4.679 4.691-4.679 12.511.012 17.191z"/></svg>
                    </div>
                </div>
                <div class="slider--button-next slider--swiper-navigation--button __icon-button swiper-button-next">
                    <div class="__icon __svg">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 240.823 240.823"><path d="M183.189 111.816L74.892 3.555c-4.752-4.74-12.451-4.74-17.215 0-4.752 4.74-4.752 12.439 0 17.179l99.707 99.671-99.695 99.671c-4.752 4.74-4.752 12.439 0 17.191 4.752 4.74 12.463 4.74 17.215 0l108.297-108.261c4.68-4.691 4.68-12.511-.012-17.19z"/></svg>
                    </div>
                </div>
            </div>
        </section><!-- /.movertheme-sliders -->
		<?php
	}
}
