<?php
namespace MoverthemeElementor\Modules\Movertheme\Widgets;

// You can add to or remove from this list - it's not conclusive! Chop & change to fit your needs.
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Background;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Repeater;
use Elementor\Scheme_Typography;
//use Elementor\Scheme_Color_Picker;
use Elementor\Widget_Base;
use Elementor\Icons_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Movertheme_Title extends Widget_Base {
	public function get_name() {
		return 'movertheme-title';
	}

	public function get_title() {
		return __( 'Movertheme Title', 'movertheme-elementor' );
	}

	public function get_icon() {
		return 'eicon-site-title';
	}
	public function get_categories() {
		return [ 'elementor-movertheme-widgets' ];
	}

	protected function _border_style() {
		$this->start_controls_section(
			'section_border_title_style',
			[
				'label' => __( 'Border Style', 'movertheme-core' ),
				'tab' => Controls_Manager::TAB_STYLE,

				'condition' => [
					'title_style' => 'border-bottom',
				],
			]
		);

		$this->add_control(
			'border_title_color',
			[
				'label' => __( 'Color', 'movertheme-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .movertheme-heading-title::before' => 'background: {{VALUE}};',
				],

				'scheme'	=> [
					'type'	=> Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
			]
		);

		$this->add_responsive_control(
			'border_title_width',
			[
				'label' => __( 'Max Width', 'movertheme-elementor' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 40,
				],
				'selectors' => [
					'{{WRAPPER}} .movertheme-heading-title::before' => 'width: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'border_title_padding',
			[
				'label' => __('Padding', 'movertheme-core'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					'{{WRAPPER}} .movertheme-heading-title::before' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'border_title_margin',
			[
				'label' => __('Margin', 'movertheme-core'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					'{{WRAPPER}} .movertheme-heading-title::before' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();
	}
	protected function _register_controls() {
		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Title Element', 'movertheme-elementor' )
			]
		);
		$this->add_control(
			'title_style',
			[
				'label' => __( 'Title Style', 'movertheme-elementor' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'with-background',
				'options' => [
					'with-background'  => __( 'Default Style', 'movertheme-elementor' ),
					'dropout-background' => __( 'Title With Description', 'movertheme-elementor' ),
					'title-to_take-icon' => __( 'Icon Title', 'movertheme-elementor' ),
					'border-bottom' => __( 'Style Border Hr', 'movertheme-elementor' ),
				],
				'prefix_class' 	=> 'movertheme-heading-title-',
			]
		);
		$this->add_control(
			'movertheme_title_txt',
			[
				'label' => __( 'Title', 'movertheme-elementor' ),
				'type' => Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => __( 'Enter title', 'movertheme-elementor' ),
				'default' => __( 'This is heading', 'movertheme-elementor' ),
			]
		);

		$this->add_control(
			'title_content',
			[
				'label' => __( 'Title Content', 'movertheme-elementor' ),
				'type' => Controls_Manager::TEXTAREA,
				'label_block' => true,
				'placeholder' => __( 'Enter Title Content', 'movertheme-elementor' ),
				'separator' => 'before',
				'condition'		=> [
					'title_style' => 'dropout-background',
				]
			]
		);
		$this->add_control(
			'selected_title_icon',
			[
				'label' => __( 'Title Icon', 'elementor' ),
				'type' => Controls_Manager::ICONS,
				'separator' => 'before',
				'fa4compatibility' => 'icon',
				'default' => [
					'value' => 'fas fa-truck-pickup',
				],

				'condition'		=> [
					'title_style' => 'title-to_take-icon',
				]
			]
		);

		/*$this->add_control(
			'title_type',
			[
				'label' => __( 'Title Type', 'movertheme-elementor' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'default',
				'options' => [
					'default'  => __( 'Default', 'movertheme-elementor' ),
					'lead-title' => __( 'Lead Title', 'movertheme-elementor' ),
				],
				'prefix_class' => '',
				'separator' => 'after',
			]
		);*/
		$this->add_control(
			'title_link',
			[
				'label' => __( 'Title Url', 'movertheme-elementor' ),
				'type' => Controls_Manager::TEXT,
				'label_block' => true,
				'default' => '',
			]
		);
		$this->add_responsive_control(
			'align',
			[
				'label' 	=> __( 'Alignment', 'movertheme-elementor' ),
				'type' 		=> Controls_Manager::CHOOSE,
				'options' 	=> [
					'left' 		=> [
						'title' => __( 'Left', 'movertheme-elementor' ),
						'icon' 	=> 'fa fa-align-left',
					],
					'center' 	=> [
						'title' => __( 'Center', 'movertheme-elementor' ),
						'icon' 	=> 'fa fa-align-center',
					],
					'right' 	=> [
						'title' => __( 'Right', 'movertheme-elementor' ),
						'icon' 	=> 'fa fa-align-right',
					],
					'justify' 	=> [
						'title' => __( 'Justified', 'movertheme-elementor' ),
						'icon' 	=> 'fa fa-align-justify',
					],
				],
				'default' 	=> '',
				'selectors' => [
					'{{WRAPPER}}' => 'text-align: {{VALUE}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'title_html_tag',
			[
				'label' => __( 'Title HTML Tag', 'movertheme-elementor' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'h1' => 'H1',
					'h2' => 'H2',
					'h3' => 'H3',
					'h4' => 'H4',
					'h5' => 'H5',
					'h6' => 'H6',
					'div' => 'div',
				],
				'default' => 'h2',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_title_style',
			[
				'label' 	=> __( 'Title', 'movertheme-elementor' ),
				'tab' 		=> Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' 		=> 'typography',
				'scheme' 	=> Scheme_Typography::TYPOGRAPHY_1,
				'selector' 	=> '{{WRAPPER}} .movertheme-heading-title',
			]
		);

		$this->add_control(
			'title_bg_color',
			[
				'label'		=> __( 'Background Color', 'movertheme-elementor' ),
				'type'		=> Controls_Manager::COLOR,
				'scheme'	=> [
					'type'	=> Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} h2.bordered span' => 'background: {{VALUE}};',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'image_border',
				'label' => __( 'Background Border', 'movertheme-elementor' ),
				'selector' => '{{WRAPPER}} h2.bordered:before',
				'condition' => [
					'title_border' => 'yes',
				],
			]
		);

		$this->add_control(
			'title_color',
			[
				'label'		=> __( 'Text Color', 'movertheme-elementor' ),
				'type'		=> Controls_Manager::COLOR,
				'scheme'	=> [
					'type'	=> Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_3,
				],
				'selectors' => [
					'{{WRAPPER}} .movertheme-heading-title' => 'color: {{VALUE}};',
				],
			]
		);



		$this->add_responsive_control(
			'text_padding',
			[
				'label' 		=> __( 'Title Padding', 'movertheme-elementor' ),
				'type' 			=> Controls_Manager::DIMENSIONS,
				'size_units' 	=> [ 'px', 'em', '%' ],
				'selectors' 	=> [
					'{{WRAPPER}} .movertheme-heading-title' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' 	=> 'before',
			]
		);
		$this->add_responsive_control(
			'text_margin',
			[
				'label' 		=> __( 'Title Margin', 'movertheme-elementor' ),
				'type' 			=> Controls_Manager::DIMENSIONS,
				'size_units' 	=> [ 'px', 'em', '%' ],
				'selectors' 	=> [
					'{{WRAPPER}} .movertheme-heading-title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' 	=> 'before',
			]
		);


		$this->end_controls_section();
		# Title Section end 1

		/*Title Icon Style*/
		$this->start_controls_section(
			'section_title_icon_style',
			[
				'label' 	=> __( 'Title Icon', 'movertheme-elementor' ),
				'tab' 		=> Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'title_icon_color',
			[
				'label'		=> __( 'Icon Color', 'movertheme-elementor' ),
				'type'		=> Controls_Manager::COLOR,
				'scheme'	=> [
					'type'	=> Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_3,
				],
				'selectors' => [
					' {{WRAPPER}} .title-icon-wrap #path0' => 'fill: {{VALUE}};',
					' {{WRAPPER}} .title-icon-wrap .title-icon' => 'color: {{VALUE}};',
				],
				'condition'		=> [
					'title_style' => 'title-to_take-icon',
				]
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' 		=> 'title_icon_typography',
				'scheme' 	=> Scheme_Typography::TYPOGRAPHY_3,
				'selector' 	=> '{{WRAPPER}} .title-icon ',
			]
		);
		$this->add_responsive_control(
			'title_icon_width',
			[
				'label' => __( 'Max Width', 'movertheme-elementor' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 190,
				],
				'selectors' => [
					'{{WRAPPER}} .title-icon' => 'max-width: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();

		# Title Content Section 2
		$this->start_controls_section(
			'section_title_content_style',
			[
				'label' 	=> __( 'Title Content', 'movertheme-elementor' ),
				'tab' 		=> Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'title_content_color',
			[
				'label'		=> __( 'Color', 'movertheme-elementor' ),
				'type'		=> Controls_Manager::COLOR,
				'scheme'	=> [
					'type'	=> Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_3,
				],

				'selectors' => [
					'{{WRAPPER}} .title-content' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' 		=> 'title_content_typography',
				'scheme' 	=> Scheme_Typography::TYPOGRAPHY_3,
				'selector' 	=> '{{WRAPPER}} .title-content',
			]
		);

		$this->add_responsive_control(
			'title_content_width',
			[
				'label' => __( 'Max Width', 'movertheme-elementor' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => '%',
					'size' => 100,
				],
				'selectors' => [
					'{{WRAPPER}} .title-content' => 'max-width: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'sub_text_padding',
			[
				'label' => __( 'Padding', 'movertheme-elementor' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .title-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_responsive_control(
			'sub_text_margin',
			[
				'label' 		=> __( 'Title Margin', 'movertheme-elementor' ),
				'type' 			=> Controls_Manager::DIMENSIONS,
				'size_units' 	=> [ 'px', 'em', '%' ],
				'selectors' 	=> [
					'{{WRAPPER}} .title-content' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' 	=> 'before',
			]
		);
		$this->end_controls_section();
		# Title Content part 2 end

		$this->_border_style();
	}
	protected function render( ) {
		$settings = $this->get_settings();
		//$color = new Scheme_Color_Picker();
		//$values = $color->get_scheme_value();
		//var_dump($values);

		$this->furnish_render();
    }

    public function furnish_render() {
		$settings = $this->get_settings();
		//$color = new Scheme_Color_Picker();
		//$values = $color->get_scheme_value();
		//var_dump($values);

		$lin = $settings['title_link'];
		$title_html_tag = $settings['title_html_tag'];

	    $document = \Elementor\Plugin::$instance->documents->get_doc_for_frontend( '361' );
		$this->add_render_attribute('title_content','class', 'title-content');
		$this->add_inline_editing_attributes( 'title_content', 'basic' );
		$banner_title = $settings['movertheme_title_txt'];
		$this->add_render_attribute('movertheme_title_txt','class', 'movertheme-heading-title');
		$this->add_inline_editing_attributes( 'movertheme_title_txt', 'basic' );
		$title_html = sprintf( '<%1$s %2$s>%3$s </%1$s>', $settings['title_html_tag'], $this->get_render_attribute_string( 'movertheme_title_txt' ), $banner_title);

		/*Title Content*/
		$title_content = $settings['title_content'];

		?>

		<div class="movertheme-title-content-wrapper">
			<?php if (! empty(  $lin )): ?>
				<a href="<?php echo $lin;?>"><?php echo $title_html; ?></a>
			<?php else: ?>
				<?php echo $title_html; ?>
			<?php endif; ?>
			<?php if (!empty($title_content) && ($settings['title_style'] === "dropout-background")): ?>
				<div class="title-content-wrap"><div <?php echo $this->get_render_attribute_string( 'title_content' ); ?>><?php echo $settings['title_content']; ?></div></div>
			<?php endif; ?>
			<?php if (!empty($banner_title) && ($settings['title_style'] === "title-to_take-icon")): ?>
				<div class="title-icon-wrap"><div class="title-icon"><?php Icons_Manager::render_icon( $settings['selected_title_icon'] ); ?></div></div>
			<?php endif; ?>
		</div>
    <?php }
}
