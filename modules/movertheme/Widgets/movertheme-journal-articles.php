<?php
namespace MoverthemeElementor\Modules\Movertheme\Widgets;

// You can add to or remove from this list - it's not conclusive! Chop & change to fit your needs.
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Background;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Repeater;
use Elementor\Scheme_Typography;
use Elementor\Widget_Base;
use WP_Query;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Movertheme_Journal_Articles extends Widget_Base {

	/* Uncomment the line below if you do not wish to use the function _content_template() - leave that section empty if this is uncommented! */
	//protected $_has_template_content = false; 
	
	public function get_name() {
		return 'movertheme-Journal-Article';
	}

	public function get_title() {
		return __( 'Journal Article', 'movertheme-elementor' );
	}

	public function get_icon() {
		return 'eicon-table';
	}

	public function get_categories() {
		return [ 'elementor-movertheme-widgets'];
	}

	/*Journal Papers Navigation Style*/
	protected function _journal_papers_navigation_style() {
		$css_scheme_journal_papers_navigation = apply_filters(
			'movertheme-elementor/movertheme-journal-papers/css-scheme/journal_papers_navigation',
			array(
				'journal_papers_navigation'         => ' {{WRAPPER}} .section-journal-papers .journal-papers-nav  a.link-prev_def',
				'journal_papers_navigation_hover'         => ' {{WRAPPER}} .section-journal-papers .journal-papers-nav  a.link-prev_def:hover',
			)
		);

		$this->start_controls_section(

			'journal_papers_navigation_style',
			[
				'label' 	=> __( 'Article Year Navigation', 'movertheme-elementor' ),
				'tab' 		=> Controls_Manager::TAB_STYLE,
			]
		);

		$this->start_controls_tabs( 'journal_papers_navigation_tab_style' );

		$this->start_controls_tab(
			'journal_papers_navigation_normal',
			[
				'label' => __( 'Normal', 'movertheme-elementor' ),
			]
		);

		$this->add_control(
			'journal_papers_navigation_color',
			[
				'label' => __( 'Color', 'movertheme-elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					$css_scheme_journal_papers_navigation['journal_papers_navigation'] => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'journal_papers_navigation_typography',
				'selector' => $css_scheme_journal_papers_navigation['journal_papers_navigation'],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'journal_papers_navigation_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => $css_scheme_journal_papers_navigation['journal_papers_navigation'],
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'journal_papers_navigation_background',
				'label' => __( 'Background', 'movertheme-elementor' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => $css_scheme_journal_papers_navigation['journal_papers_navigation'],
			]
		);
		$this->add_group_control(
			Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'journal_papers_navigation_text_shadow',
				'label' => __( 'Text Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_journal_papers_navigation['journal_papers_navigation'],
			]
		);
		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'journal_papers_navigation_shadow',
				'label' => __( 'Box Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_journal_papers_navigation['journal_papers_navigation'],
			]
		);
		$this->end_controls_tab();
		# End Normal Style Tab
		$this->start_controls_tab(
			'journal_papers_navigation_hover',
			[
				'label' => __( 'Hover', 'movertheme-elementor' ),
			]
		);

		$this->add_control(
			'journal_papers_navigation_hover_color',
			[
				'label' => __( 'Color', 'movertheme-elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					$css_scheme_journal_papers_navigation['journal_papers_navigation_hover'] => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'journal_papers_navigation_text_shadow_hover',
				'label' => __( 'Text Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_journal_papers_navigation['journal_papers_navigation_hover'],
			]
		);
		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'journal_papers_navigation_hover_shadow',
				'label' => __( 'Box Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_journal_papers_navigation['journal_papers_navigation_hover'],
			]
		);
		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'journal_papers_navigation_hover_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => $css_scheme_journal_papers_navigation['journal_papers_navigation_hover'],
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'journal_papers_navigation_hover_background',
				'label' => __( 'Background', 'movertheme-elementor' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => $css_scheme_journal_papers_navigation['journal_papers_navigation_hover'],
			]
		);
		$this->end_controls_tab();
		# End Hover Style Tab
		$this->end_controls_tabs();
		#End Tabs


		$this->add_responsive_control(
			'journal_papers_navigation_padding',
			[
				'label' => __('Padding', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					$css_scheme_journal_papers_navigation['journal_papers_navigation'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_responsive_control(
			'journal_papers_navigation_margin',
			[
				'label' => __('Margin', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					$css_scheme_journal_papers_navigation['journal_papers_navigation'] => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();
	}

	/*Style Elements Journal Paper Author*/
	protected function _journal_paper_authors_style() {
		$css_scheme_journal_paper_authors = apply_filters(
			'movertheme-elementor/movertheme-events/css-scheme/journal_paper_authors',
			array(
				'jpaper_author'         => ' .section-journal-papers  .journal-papers .jp-name ',
			)
		);
		$this->start_controls_section(
			'journal_paper_authors_style',
			[
				'label' 	=> __( 'Authors Name', 'movertheme-elementor' ),
				'tab' 		=> Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'elementor_movertheme_journal_paper_authors_color',
			[
				'label' => __( 'Color', 'movertheme-elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} '.$css_scheme_journal_paper_authors['jpaper_author'] => 'color: {{VALUE}};',
					'{{WRAPPER}} '.$css_scheme_journal_paper_authors['jpaper_author'].' a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'elementor_movertheme_journal_paper_authors_typography',
				'selector' => '{{WRAPPER}} '.$css_scheme_journal_paper_authors['jpaper_author'],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'elementor_movertheme_journal_paper_authors_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => '{{WRAPPER}} '.$css_scheme_journal_paper_authors['jpaper_author'],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'elementor_movertheme_journal_paper_authors_shadow',
				'label' => __( 'Box Shadow', 'movertheme-elementor' ),
				'selector' => '{{WRAPPER}} '.$css_scheme_journal_paper_authors['jpaper_author'],
			]
		);


		$this->add_responsive_control(
			'elementor_movertheme_journal_paper_authors_padding',
			[
				'label' => __('Spacing', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					'{{WRAPPER}} '.$css_scheme_journal_paper_authors['jpaper_author'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);
		$this->end_controls_section();
	}


	/*Style Elements Journal Paper Meta*/
	protected function _journal_paper_meta_style() {
		$css_scheme_journal_paper_meta = apply_filters(
			'movertheme-elementor/movertheme-journal_paper/css-scheme/journal_paper_meta',
			array(
				'jpaper_meta'         => ' .section-journal-papers  .journal-papers .journal-papers-meta p ',
			)
		);
		$this->start_controls_section(
			'journal_paper_meta_style',
			[
				'label' 	=> __( 'JPaper Meta', 'movertheme-elementor' ),
				'tab' 		=> Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'elementor_movertheme_journal_paper_meta_color',
			[
				'label' => __( 'Color', 'movertheme-elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} '.$css_scheme_journal_paper_meta['jpaper_meta'] => 'color: {{VALUE}};',
					'{{WRAPPER}} '.$css_scheme_journal_paper_meta['jpaper_meta'].' a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'elementor_movertheme_journal_paper_meta_typography',
				'selector' => '{{WRAPPER}} '.$css_scheme_journal_paper_meta['jpaper_meta'],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'elementor_movertheme_journal_paper_meta_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => '{{WRAPPER}} '.$css_scheme_journal_paper_meta['jpaper_meta'],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'elementor_movertheme_journal_paper_meta_shadow',
				'label' => __( 'Box Shadow', 'movertheme-elementor' ),
				'selector' => '{{WRAPPER}} '.$css_scheme_journal_paper_meta['jpaper_meta'],
			]
		);


		$this->add_responsive_control(
			'elementor_movertheme_journal_paper_meta_padding',
			[
				'label' => __('Spacing', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					'{{WRAPPER}} '.$css_scheme_journal_paper_meta['jpaper_meta'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);
		$this->end_controls_section();
	}


	/*Journal Papers Doi links Style*/
	protected function _journal_papers_doi_links_style() {
		$css_scheme_journal_papers_doi_links = apply_filters(
			'movertheme-elementor/movertheme-journal-papers/css-scheme/journal_papers_doi_links',
			array(
				'journal_papers_doi_links'         => ' {{WRAPPER}} .section-journal-papers .journal-papers-doi > a',
				'journal_papers_doi_links_hover'         => ' {{WRAPPER}} .section-journal-papers .journal-papers-doi > a:hover',
			)
		);

		$this->start_controls_section(

			'journal_papers_doi_links_style',
			[
				'label' 	=> __( 'Doi Links', 'movertheme-elementor' ),
				'tab' 		=> Controls_Manager::TAB_STYLE,
			]
		);

		$this->start_controls_tabs( 'journal_papers_doi_links_style' );

		$this->start_controls_tab(
			'journal_papers_doi_links_normal',
			[
				'label' => __( 'Normal', 'movertheme-elementor' ),
			]
		);

		$this->add_control(
			'journal_papers_doi_links_color',
			[
				'label' => __( 'Color', 'movertheme-elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					$css_scheme_journal_papers_doi_links['journal_papers_doi_links'] => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'journal_papers_doi_links_typography',
				'selector' => $css_scheme_journal_papers_doi_links['journal_papers_doi_links'],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'journal_papers_doi_links_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => $css_scheme_journal_papers_doi_links['journal_papers_doi_links'],
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'journal_papers_doi_links_background',
				'label' => __( 'Background', 'movertheme-elementor' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => $css_scheme_journal_papers_doi_links['journal_papers_doi_links'],
			]
		);
		$this->add_group_control(
			Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'journal_papers_doi_links_text_shadow',
				'label' => __( 'Text Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_journal_papers_doi_links['journal_papers_doi_links'],
			]
		);
		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'journal_papers_doi_links_shadow',
				'label' => __( 'Box Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_journal_papers_doi_links['journal_papers_doi_links'],
			]
		);
		$this->end_controls_tab();
		# End Normal Style Tab
		$this->start_controls_tab(
			'journal_papers_doi_links_hover',
			[
				'label' => __( 'Hover', 'movertheme-elementor' ),
			]
		);

		$this->add_control(
			'journal_papers_doi_links_hover_color',
			[
				'label' => __( 'Color', 'movertheme-elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					$css_scheme_journal_papers_doi_links['journal_papers_doi_links_hover'] => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'journal_papers_doi_links_text_shadow_hover',
				'label' => __( 'Text Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_journal_papers_doi_links['journal_papers_doi_links_hover'],
			]
		);
		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'journal_papers_doi_links_hover_shadow',
				'label' => __( 'Box Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_journal_papers_doi_links['journal_papers_doi_links_hover'],
			]
		);
		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'journal_papers_doi_links_hover_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => $css_scheme_journal_papers_doi_links['journal_papers_doi_links_hover'],
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'journal_papers_doi_links_hover_background',
				'label' => __( 'Background', 'movertheme-elementor' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => $css_scheme_journal_papers_doi_links['journal_papers_doi_links_hover'],
			]
		);
		$this->end_controls_tab();
		# End Hover Style Tab
		$this->end_controls_tabs();
		#End Tabs


		$this->add_responsive_control(
			'journal_papers_doi_links_padding',
			[
				'label' => __('Padding', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					$css_scheme_journal_papers_doi_links['journal_papers_doi_links'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_responsive_control(
			'journal_papers_doi_links_margin',
			[
				'label' => __('Margin', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					$css_scheme_journal_papers_doi_links['journal_papers_doi_links'] => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();
	}



	protected function _register_controls() {
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'movertheme-elementor' ),
			]
		);
		// Add your widget/element content controls here! Below is an example control


		$this->add_control(
			'post_order_by',
			[
				'label'     => __( 'Order', 'movertheme-elementor' ),
				'type'      => Controls_Manager::SELECT,
				'default'   => 'DESC',
				'options'   => [
					'DESC' 		=> __( 'Descending', 'movertheme-elementor' ),
					'ASC' 		=> __( 'Ascending', 'movertheme-elementor' ),
				],
			]
		);
		
		$this->add_control(
			'view',
			[
				'label' => __( 'View', 'movertheme-elementor' ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);
		
		$this->end_controls_section();
		$this->_journal_papers_navigation_style();
		$this->_journal_paper_authors_style();
		$this->_journal_paper_meta_style();
		$this->_journal_papers_doi_links_style();

	}
		
	protected function render() {
		$settings = $this->get_settings_for_display();


		$terms = get_terms( 'journal_article_cat', array(
			'hide_empty' => true,
			'parent' => 0,

		) );
		if( !empty( $terms ) && !is_wp_error( $terms ) ){
			$term_array = array();
			foreach ($terms as $term ) {
				$term_array[] = $term->name;
			}

			rsort($term_array);

			$year_array = array();

			foreach($term_array as $category){
				$year_array[] = $category;
			}
		}
		?>
        <div class="movertheme-content-area section-journal-papers">
            <nav class="journal-papers-nav">
                <ul class="journal-papers-nav-list list-inline" role="tablist">
                    <?php if( !empty( $terms ) && is_array( $terms ) ) :  $i = 0;
                    foreach ( $year_array as $year ) : ?>
	                    <?php
                            $i++;
                            $class_active = ( $i == 1 ) ? 'class=active' : '';
                        ?>

                        <li role="presentation" '.esc_attr( $class_active ).'>
                            <a href="#<?php echo esc_attr( $year ); ?>" class="link-prev_def" aria-controls="<?php echo esc_attr( $year ); ?>" role="tab" data-toggle="tab"><?php echo esc_html($year );?></a>
                        </li>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </nav>

            <div class="journal-papers-mound-wrap tab-content">
                <?php $i = 0; foreach ( $year_array as $year ) :  ?>

                    <?php

                        $i++;
                        $active = ( $i == 1 ) ? 'in active' : '';
                        $args = array(
                            'post_type'       => 'journal_article',
                            'posts_per_page'  => -1,
                            'journal_article_cat' => $year,
                            'post_status' => 'publish',
	                        'order'         => $settings['post_order_by'],
                        );
	                    $journal_papers_posts = new WP_Query( $args );
	                    $year_match = 1;
                    ?>

                    <div class="journal-papers-mound tab-pane fade <?php echo esc_attr( $active ); ?>" id="<?php echo esc_attr( $year ); ?>" role="tabpanel">
                        <?php if( $journal_papers_posts->have_posts() ) : while ( $journal_papers_posts->have_posts() ) : $journal_papers_posts->the_post(); ?><!--Posts Loop-->

                            <?php
                                if( get_post_meta( get_the_ID() , '_movertheme_journal_article_authors_name', true) ){
                                    $journal_article_authors_name = get_post_meta( get_the_ID() , '_movertheme_journal_article_authors_name', true);
                                } else{
                                    $journal_article_authors_name = '';
                                }

                                if( get_post_meta( get_the_ID() , '_movertheme_journal_article_research_topic', true) ){
                                    $journal_article_research_topic = get_post_meta( get_the_ID() , '_movertheme_journal_article_research_topic', true);
                                } else{
                                    $journal_article_research_topic = '';
                                }

                                if( get_post_meta( get_the_ID() , '_movertheme_journal_article_publication_identity', true) ){
                                    $journal_article_publication_identity = get_post_meta( get_the_ID() , '_movertheme_journal_article_publication_identity', true);
                                } else{
                                    $journal_article_publication_identity = '';
                                }

                                if( get_post_meta( get_the_ID() , '_movertheme_journal_article_doi', true) ){
                                    $journal_article_doi = get_post_meta( get_the_ID() , '_movertheme_journal_article_doi', true);
                                } else{
                                    $journal_article_doi = '';
                                }

                                if( get_post_meta( get_the_ID() , '_movertheme_journal_article_doi_link', true) ){
                                    $journal_article_doi_link = get_post_meta( get_the_ID() , '_movertheme_journal_article_doi_link', true);
                                } else{
                                    $journal_article_doi_link = '';
                                }


                                if( get_post_meta( get_the_ID() , '_movertheme_journal_article_pdf_title', true) ){
                                    $journal_article_pdf_title = get_post_meta( get_the_ID() , '_movertheme_journal_article_pdf_title', true);
                                } else{
                                    $journal_article_pdf_title = '';
                                }

                                if( get_post_meta( get_the_ID() , '_movertheme_journal_article_pdf_link', true) ){
                                    $journal_article_pdf_link = get_post_meta( get_the_ID() , '_movertheme_journal_article_pdf_link', true);
                                } else{
                                    $journal_article_pdf_link = '';
                                }
                            ?>

                            <nav class="journal-papers-mound-nav">
	                            <?php if( $year != $year_match ) : ?>
                                    <h3 class="nav-title"><?php echo wp_kses_post($year);?></h3>
		                        <?php $year_match = $year; endif; ?>
                            </nav>
                            <div class="journal-papers-list">
                                <div class="journal-papers">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <p class="jp-name"><?php echo wp_kses_post( $journal_article_authors_name ); ?></p>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="journal-papers-meta">
                                                <p>
	                                                <?php the_title(); ?>
                                                    <em> <?php echo ' '. wp_kses_post($journal_article_research_topic) . ' '; ?> </em>
	                                                <?php echo ' &nbsp '. wp_kses_post( $journal_article_publication_identity ) . ' '; ?>
                                                    <?php if( $journal_article_pdf_link ): ?>
                                                        <a href="<?php echo esc_url( $journal_article_pdf_link ); ?>" class="pdf-link" target="_blank"><?php echo wp_kses_post( $journal_article_pdf_title ); ?></a>
                                                    <?php endif; ?>
                                                </p>
                                            </div>
                                        </div><div class="col-sm-3">
                                            <div class="journal-papers-doi"><span><?php echo __('DOI: ','movertheme-elementor');?></span> <a href="<?php echo esc_url($journal_article_doi_link); ?>"> <?php echo wp_kses_post( $journal_article_doi ); ?></a></div>
                                        </div></div>
                                </div><!-- /.journal-papers -->
                            </div>

                        <?php endwhile; wp_reset_postdata(); endif;  ?><!--End Post loop-->
                    </div>
                <?php endforeach; ?>
            </div>

        </div>
        <?php
	}

	protected function _content_template() {
		/* If you have selected to uncomment the "protected $_has_template_content = false;" above then leave this section empty! */
	}
	
}
