<?php
namespace MoverthemeElementor\Modules\Movertheme\Widgets;

// You can add to or remove from this list - it's not conclusive! Chop & change to fit your needs.
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Background;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Repeater;
use Elementor\Scheme_Typography;
use Elementor\Widget_Base;
use DateTime;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Movertheme_Conference_Talks extends Widget_Base {

	/* Uncomment the line below if you do not wish to use the function _content_template() - leave that section empty if this is uncommented! */
	//protected $_has_template_content = false; 
	
	public function get_name() {
		return 'movertheme-conference-talks';
	}

	public function get_title() {
		return __( 'Conference & Talks', 'movertheme-elementor' );
	}

	public function get_icon() {
		return 'eicon-column';
	}

	public function get_categories() {
		return [ 'elementor-movertheme-widgets'];
	}
	
	protected function _register_controls() {
		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content', 'movertheme-elementor' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

		$repeater = new Repeater();

		$repeater->add_control(
			'conference_title', [
				'label' => __( 'Title', 'movertheme-elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Your conference title here' , 'movertheme-elementor' ),
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'conference_date',
			[
				'label' => __( 'Date:', 'movertheme-elementor' ),
				'type' => Controls_Manager::DATE_TIME,
				'default' => __( '2007-07-08' , 'movertheme-elementor' ),
				'picker_options' => array(
					'enableTime' => false,
				),
			]
		);

		$repeater->add_control(
			'conference_time',
			[
				'label' => __( 'Time:', 'movertheme-elementor' ),
				'type' => Controls_Manager::DATE_TIME,
				'default' => __( '14:57' , 'movertheme-elementor' ),
				'picker_options' => array(
					'enableTime' => true,
					'time_24hr'     => true,
					'noCalendar'     => true,
				)
			]
		);

		$repeater->add_control(
			'conference_location', [
				'label' => __( 'Location', 'movertheme-elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Gangni University, Gangni Meherpur' , 'movertheme-elementor' ),
				'label_block' => true,
                'separator'     => 'after',
			]
		);

		$repeater->add_control(
			'conference_content', [
				'label' => __( 'Content', 'movertheme-elementor' ),
				'description' => __( 'Your Content', 'movertheme-elementor' ),
				'type' => Controls_Manager::WYSIWYG,
				'default' => __( 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a ' , 'movertheme-elementor' ),
				'show_label' => true,
				'separator'     => 'before',
			]
		);

		$this->add_control(
			'conference_data',
			[
				'label' => __( 'Conference & Talks List', 'movertheme-elementor' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'conference_title' => __( 'Fireside chat with Suzanne Smith', 'movertheme-elementor' ),
						'conference_location' => __( 'Santi House, Gangni University', 'movertheme-elementor' ),
						'conference_content' => __( 'Fireside chat with Suzanne Smith, Center for Writing and Communicating Ideas, Harvard Graduate School of Arts and Sciences.', 'movertheme-elementor' ),
						'conference_date' => __( '2019-07-08', 'movertheme-elementor' ),
						'conference_time' => __( '5:50 pm', 'movertheme-elementor' ),
					],
					[
						'conference_title' => __( 'Fireside chat with Suzanne Smith', 'movertheme-elementor' ),
						'conference_location' => __( 'Santi House, Gangni University', 'movertheme-elementor' ),
						'conference_content' => __( 'Fireside chat with Suzanne Smith, Center for Writing and Communicating Ideas, Harvard Graduate School of Arts and Sciences.', 'movertheme-elementor' ),
						'conference_date' => __( '2019-07-08', 'movertheme-elementor' ),
						'conference_time' => __( '7:40 pm', 'movertheme-elementor' ),
					],
				],
				'title_field' => '{{{ conference_title }}}',
			]
		);

		$this->end_controls_section();
	}
		
	protected function render() {
		$settings = $this->get_settings_for_display();

		//var_dump($settings['list']);
		$conference_loop_data = $settings['conference_data']; ?>

        <section class="deff-timeline-section">
            <div class="auth-deff_timeline_timeline-segment">

                <ul class="auth-deff_timeline list-unstyled">
					<?php foreach ($conference_loop_data as $conferenceId => $conference ) : ?>
						<?php
						/*generate require Var*/
						$conference_title = $conference['conference_title'];
						$conference_location = $conference['conference_location'];
						$conference_content = $conference['conference_content'];

						/*Generate suitable time date*/
						$date = new DateTime( $conference['conference_date'] );
						$date = $date->format( 'Y jS F' );
						$date_array = explode( ' ', $date );
						$conference_year = $date_array[0];
						$conference_date_month = $date_array[1].' '.$date_array[2];
						$conference_time_12_hour_format  = date("g:i a", strtotime($conference['conference_time']));
						?>
                        <li>
                            <div class="time-span">
                                <div class="time-year"><?php echo wp_kses_post($conference_year); ?></div>
                                <div class="time-month"><?php echo wp_kses_post($conference_date_month); ?></div>
                            </div>
                            <div class="timeline-meta">
                                <h3 class="staff-title"><?php echo wp_kses_post($conference_title); ?></h3>
                                <div class="__time"><?php echo wp_kses_post($conference_time_12_hour_format); ?></div>
                                <div class="__loc"><span><?php echo esc_html( 'Location: ', 'movertheme-elementor');?> </span> <?php echo wp_kses_post($conference_location); ?></div>
                                <p class="meta-text"><?php echo wp_kses_post($conference_content); ?></p>
                            </div>
                        </li><!-- /timeline-item -->
					<?php endforeach; ?>
                </ul><!-- /.auth-deff_timeline -->
            </div>
        </section>
		<?php
	}

	protected function _content_template() {

	}
	
}
