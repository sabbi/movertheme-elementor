<?php
namespace MoverthemeElementor\Modules\Movertheme\Widgets;

// You can add to or remove from this list - it's not conclusive! Chop & change to fit your needs.
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Background;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Repeater;
use Elementor\Scheme_Typography;
//use Elementor\Scheme_Color_Picker;
use Elementor\Widget_Base;
use Elementor\Icons_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Movertheme_Common_Question extends Widget_Base {
    /**
    * Get widget name.
    *
    * Retrieve image carousel widget name.
    *
    * @since 1.0.0
    * @access public
    *
    * @return string Widget name.
    */
	public function get_name() {
		return 'movertheme-commonquestion';
	}

    /**
     * Get widget title.
     *
     * Retrieve image carousel widget title.
     *
     * @since 1.0.0
     * @access public
     *
     * @return string Widget title.
     */
	public function get_title() {
		return __( 'Movertheme Common Question', 'movertheme-elementor' );
	}

	public function get_icon() {
		return 'eicon-site-title';
	}
	public function get_categories() {
		return [ 'elementor-movertheme-widgets' ];
	}

    /**
     * Retrieve the list of scripts the image carousel widget depended on.
     *
     * Used to set scripts dependencies required to run the widget.
     *
     * @since 1.3.0
     * @access public
     *
     * @return array Widget scripts dependencies.
     */
	public function get_script_depends() {
		return [ 'jquery-accordion-scripts'];
	}
	/**
	 * Get widget keywords.
	 *
	 * Retrieve the list of keywords the widget belongs to.
	 *
	 * @since 2.1.0
	 * @access public
	 *
	 * @return array Widget keywords.
	 */
	public function get_keywords() {
		return [ 'accordion', 'tabs', 'toggle' ];
	}

	public function _add_content_control_question_answer() {
		$this->start_controls_section(
			'section_question_answer',
			[
				'label' => __( 'Question/Ans Tab', 'elementor' ),
			]
		);

		$repeater = new Repeater();

		$repeater->add_control(
			'question_answer_title',
			[
				'label' => __( 'Question', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Q. How can i.... ', 'elementor' ),
				'dynamic' => [
					'active' => true,
				],
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'question_answer_content',
			[
				'label' => __( 'Answer', 'elementor' ),
				'type' => Controls_Manager::TEXTAREA,
				'default' => __( 'Lorem ipsum dolor siamet, consectetur adipiscing elit. Aenean ac ornare odio_est.Lorem ipsum dolor siamet, consectetur adipiscing elit. Aenean ac ', 'elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'question_answer_tabs',
			[
				'label' => __( 'Question Items', 'elementor' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'tab_title' => __( 'Question #1', 'elementor' ),
						'tab_content' => __( 'Lorem Ipsum Dolor Siamet Answer...', 'elementor' ),
					],
					[
						'tab_title' => __( 'Question #2', 'elementor' ),
						'tab_content' => __( 'Lorem Ipsum Dolor Siamet Answer...', 'elementor' ),
					],
				],
				'title_field' => '{{{ tab_title }}}',
			]
		);

		$this->add_control(
			'view',
			[
				'label' => __( 'View', 'elementor' ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);



		$this->end_controls_section();
    }
	protected function _register_controls() {
	    $this->_add_content_control_question_answer();
	    $this->_movertheme_accordion_section_item_style();


		$this->start_controls_section(
			'section_title_style',
			[
				'label' 	=> __( 'Common Question', 'movertheme-elementor' ),
				'tab' 		=> Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'movertheme_accordion_section_title_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => '{{WRAPPER}} .movertheme-accordion .accordion-section .accordion-control-title',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' 		=> 'typography',
				'scheme' 	=> Scheme_Typography::TYPOGRAPHY_1,
				'selector' 	=> '{{WRAPPER}} .movertheme-accordion .accordion-section .accordion-control-title .accordion-control-text',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label'		=> __( 'Text Color', 'movertheme-elementor' ),
				'type'		=> Controls_Manager::COLOR,
				'scheme'	=> [
				    'type'	=> Scheme_Color::get_type(),
				    'value' => Scheme_Color::COLOR_3,
				],
				'selectors' => [
					' {{WRAPPER}} .movertheme-accordion .accordion-section .accordion-control-title .accordion-control-text' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'selected_icon',
			[
				'label' => __( 'Icon', 'elementor' ),
				'type' => Controls_Manager::ICONS,
				'separator' => 'before',
				'fa4compatibility' => 'icon',
				'default' => [
					'value' => 'fas fa-plus',
					'library' => 'fa-solid',
				],
			]
		);
		$this->add_control(
			'selected_active_icon',
			[
				'label' => __( 'Active Icon', 'elementor' ),
				'type' => Controls_Manager::ICONS,
				'fa4compatibility' => 'icon_active',
				'default' => [
					'value' => 'fas fa-minus',
					'library' => 'fa-solid',
				],
				'condition' => [
					'selected_icon[value]!' => '',
				],
			]
		);
		$this->add_responsive_control(
            'text_padding',
            [
                'label' 		=> __( 'Common Question Padding', 'movertheme-elementor' ),
                'type' 			=> Controls_Manager::DIMENSIONS,
                'size_units' 	=> [ 'px', 'em', '%' ],
                'selectors' 	=> [
                    '{{WRAPPER}} .movertheme-heading-title' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'separator' 	=> 'before',
            ]
        );
        $this->add_responsive_control(
            'text_margin',
            [
                'label' 		=> __( 'Common Question Margin', 'movertheme-elementor' ),
                'type' 			=> Controls_Manager::DIMENSIONS,
                'size_units' 	=> [ 'px', 'em', '%' ],
                'selectors' 	=> [
                    '{{WRAPPER}} .movertheme-accordion .accordion-section .accordion-control-title .accordion-control-text' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'separator' 	=> 'before',
            ]
        );


		$this->add_responsive_control(
			'accordion_min_height',
			[
				'label' => __( 'Min Height', 'movertheme-elementor' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'vh','px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1500,
					],
				],
				'devices' => [ 'desktop', 'tablet', 'mobile' ],
				'desktop_default' => [
					'size' => 50,
					'unit' => 'px',
				],
				'tablet_default' => [
					'size' => 20,
					'unit' => 'px',
				],
				'mobile_default' => [
					'size' => 385,
					'unit' => 'px',
				],
				'selectors' => [
					'{{WRAPPER}} .movertheme-accordion .accordion-section .accordion-control-title' => 'min-height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'accordion_box_height',
			[
				'label' => __( 'Max Height', 'movertheme-elementor' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'vh','px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1500,
					],
				],
				'devices' => [ 'desktop', 'tablet', 'mobile' ],
				'desktop_default' => [
					'size' => 185,
					'unit' => 'px',
				],
				'tablet_default' => [
					'size' => 155,
					'unit' => 'px',
				],
				'mobile_default' => [
					'size' => 155,
					'unit' => 'px',
				],
				'selectors' => [
					'{{WRAPPER}} .movertheme-accordion .accordion-section .accordion-control-title' => 'max-height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();
		# Common Question Section end 1


		# Common Question Content Section 2
		$this->start_controls_section(
			'section_title_content_style',
			[
				'label' 	=> __( 'Common Question Content', 'movertheme-elementor' ),
				'tab' 		=> Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'title_content_background',
				'label' => __( 'Background', 'plugin-domain' ),
				'types' => [ 'classic', 'gradient', 'video' ],
				'selector' => '{{WRAPPER}} .movertheme-accordion .accordion-section .accordion-expendables-content > article',
			]
		);
		$this->add_control(
			'title_content_color',
			[
				'label'		=> __( 'Color', 'movertheme-elementor' ),
				'type'		=> Controls_Manager::COLOR,
				'scheme'	=> [
				    'type'	=> Scheme_Color::get_type(),
				    'value' => Scheme_Color::COLOR_3,
				],

				'selectors' => [
					'{{WRAPPER}} .title-content' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' 		=> 'title_content_typography',
				'scheme' 	=> Scheme_Typography::TYPOGRAPHY_3,
				'selector' 	=> '{{WRAPPER}} .title-content',
			]
		);

		$this->add_responsive_control(
            'sub_text_padding',
            [
                'label' => __( 'Padding', 'movertheme-elementor' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .title-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'separator' => 'before',
            ]
        );

		$this->add_responsive_control(
		    'sub_text_margin',
		    [
		        'label' 		=> __( 'Common Question Margin', 'movertheme-elementor' ),
		        'type' 			=> Controls_Manager::DIMENSIONS,
		        'size_units' 	=> [ 'px', 'em', '%' ],
		        'selectors' 	=> [
		            '{{WRAPPER}} .title-content' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
		        ],
		        'separator' 	=> 'before',
		    ]
		);
		$this->end_controls_section();
		# Common Question Content part 2 end

		$this->_border_style();
	}
	protected function _border_style() {
		$this->start_controls_section(
			'section_border_title_style',
			[
				'label' => __( 'Border Style', 'movertheme-core' ),
				'tab' => Controls_Manager::TAB_STYLE,

				'condition' => [
					'title_style' => 'border-bottom',
				],
			]
		);

		$this->add_control(
			'border_title_color',
			[
				'label' => __( 'Color', 'movertheme-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .movertheme-heading-title::before' => 'background: {{VALUE}};',
				],

				'scheme'	=> [
					'type'	=> Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
			]
		);

		$this->add_responsive_control(
			'border_title_width',
			[
				'label' => __( 'Max Width', 'movertheme-elementor' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 40,
				],
				'selectors' => [
					'{{WRAPPER}} .movertheme-heading-title::before' => 'width: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'border_title_padding',
			[
				'label' => __('Padding', 'movertheme-core'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					'{{WRAPPER}} .movertheme-heading-title::before' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'border_title_margin',
			[
				'label' => __('Margin', 'movertheme-core'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					'{{WRAPPER}} .movertheme-heading-title::before' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();
	}

	protected function render( ) {
		$settings = $this->get_settings_for_display();
		//$values = $color->get_scheme_value();
		//var_dump($values);
		//$lin = $settings['title_link'];
		//$title_html_tag = $settings['title_html_tag'];

		$this->add_render_attribute('title_content','class', 'title-content');
		$this->add_inline_editing_attributes( 'title_content', 'basic' );
		//$banner_title = $settings['title_txt'];
		$this->add_render_attribute('title_txt','class', 'movertheme-heading-title');
		$this->add_inline_editing_attributes( 'title_txt', 'basic' );
		//$title_html = sprintf( '<%1$s %2$s>%3$s </%1$s>', $settings['title_html_tag'], $this->get_render_attribute_string( 'title_txt' ), $banner_title);

		/*Common Question Content*/
		//$title_content = $settings['title_content'];

		?>
        <?php echo  $this->furnish_render_murkup();
	}

    public function furnish_render_murkup() {
	    $settings = $this->get_settings_for_display();
	    $id_int = substr( $this->get_id_int(), 0, 4 );
	?>
        <div class="movertheme-accordion-wrap">
            <section class="movertheme-accordion" data-accordion-group  role="tablist">
                <?php
                //var_dump($settings['question_answer_tabs']);
                foreach ( $settings['question_answer_tabs'] as $index => $item ) :
                    $accordion_sum = $index + 1;

                    $question_answer_title_setting_key = $this->get_repeater_setting_key( '0', 'question_answer_tabs', $index );

                    $question_answer_content_setting_key = $this->get_repeater_setting_key( 'question_answer_content', 'question_answer_tabs', $index );

                    $this->add_render_attribute( $question_answer_title_setting_key, [
                        'id' => 'movertheme-accordion-control-title-' . $id_int . $accordion_sum,
                        'class' => [ 'accordion-control-title' ],
                        'data-tab' => $accordion_sum,
                        'role' => 'tab',
                        'aria-controls' => 'movertheme-accordion-control-content-' . $id_int . $accordion_sum,
                        'data-control' => '',
                    ] );

                    $this->add_render_attribute( $question_answer_content_setting_key, [
                        'id' => 'movertheme-accordion-control-content-' . $id_int . $accordion_sum,
                        'class' => [ 'accordion-expendables-content', 'elementor-clearfix' ],
                        'data-tab' => $accordion_sum,
                        'role' => 'tabpanel',
                        'aria-labelledby' => 'movertheme-accordion-control-title-' . $id_int . $accordion_sum,
                        'data-content'  => '',
                    ] );
                    ?>
                    <section class="accordion-section" data-accordion data-accordion-option="opt-active">
                        <button class="accordion-control-title"  <?php echo $this->get_render_attribute_string( $question_answer_title_setting_key ); ?>>
                            <span class="expendable-icon"><?php Icons_Manager::render_icon( $settings['selected_icon'] ); ?></span>
                            <h4><span class="accordion-control-text"><?php echo $item['question_answer_title']; ?></span></h4>
                            <span class="expendable-icon expendable-icon-unactive"> <?php Icons_Manager::render_icon( $settings['selected_active_icon'] ); ?> </span>
                        </button>
                        <div <?php echo $this->get_render_attribute_string( $question_answer_content_setting_key ); ?>>
                            <article>
                                <?php echo $item['question_answer_content']; ?>
                            </article>
                        </div>
                    </section>
                <?php endforeach; ?>
            </section>
        </div>
    <?php
    }
	public function _movertheme_accordion_section_item_style() {
		$css_scheme_movertheme_accordion_section_item = apply_filters(
			'movertheme-elementor/movertheme-movertheme_accordion/css-scheme/movertheme_accordion_section_item',
			array(
				'movertheme_accordion_section_item'         => ' {{WRAPPER}} .movertheme-accordion .accordion-section',
			)
		);
		$this->start_controls_section(

			'movertheme_accordion_section_item_style',
			[
				'label' 	=> __( 'Accordion Section Item', 'movertheme-elementor' ),
				'tab' 		=> Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'movertheme_accordion_section_item_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => $css_scheme_movertheme_accordion_section_item['movertheme_accordion_section_item'],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'movertheme_accordion_section_item_shadow',
				'label' => __( 'Box Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_movertheme_accordion_section_item['movertheme_accordion_section_item'],
			]
		);

		$this->add_responsive_control(
			'movertheme_accordion_section_item_btn_padding',
			[
				'label' => __('Padding', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					$css_scheme_movertheme_accordion_section_item['movertheme_accordion_section_item'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_responsive_control(
			'movertheme_accordion_section_item_btn_margin',
			[
				'label' => __('Margin', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					$css_scheme_movertheme_accordion_section_item['movertheme_accordion_section_item'] => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();
	}
}

