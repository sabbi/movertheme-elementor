<?php
namespace MoverthemeElementor\Modules\Movertheme\Widgets;

// You can add to or remove from this list - it's not conclusive! Chop & change to fit your needs.
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Background;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Repeater;
use Elementor\Scheme_Typography;
//use Elementor\Scheme_Color_Picker;
use Elementor\Widget_Base;
use Elementor\Icons_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Movertheme_WorkingProcess extends Widget_Base {
	public function get_name() {
		return 'movertheme-workingprocess';
	}

	public function get_title() {
		return __( 'Movertheme Workingprocess', 'movertheme-elementor' );
	}

	public function get_icon() {
		return 'eicon-site-title';
	}
	public function get_categories() {
		return [ 'elementor-movertheme-widgets' ];
	}

	protected function _border_style() {
		$this->start_controls_section(
			'section_border_workingprocess_style',
			[
				'label' => __( 'Border Style', 'movertheme-core' ),
				'tab' => Controls_Manager::TAB_STYLE,

				'condition' => [
					'workingprocess_style' => 'border-bottom',
				],
			]
		);

		$this->add_control(
			'border_workingprocess_color',
			[
				'label' => __( 'Color', 'movertheme-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .movertheme-heading-workingprocess::before' => 'background: {{VALUE}};',
				],

				'scheme'	=> [
					'type'	=> Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
			]
		);

		$this->add_responsive_control(
			'border_workingprocess_width',
			[
				'label' => __( 'Max Width', 'movertheme-elementor' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 40,
				],
				'selectors' => [
					'{{WRAPPER}} .movertheme-heading-workingprocess::before' => 'width: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'border_workingprocess_padding',
			[
				'label' => __('Padding', 'movertheme-core'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					'{{WRAPPER}} .movertheme-heading-workingprocess::before' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'border_workingprocess_margin',
			[
				'label' => __('Margin', 'movertheme-core'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					'{{WRAPPER}} .movertheme-heading-workingprocess::before' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();
	}
	protected function _register_controls() {
		$this->start_controls_section(
			'section_workingprocess',
			[
				'label' => __( 'Title Element', 'movertheme-elementor' )
			]
		);
		$this->add_control(
			'workingprocess_style',
			[
				'label' => __( 'Title Style', 'movertheme-elementor' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'with-background',
				'options' => [
					'with-background'  => __( 'Default Style', 'movertheme-elementor' ),
					'dropout-background' => __( 'Title With Description', 'movertheme-elementor' ),
					'workingprocess-to_take-icon' => __( 'Icon Title', 'movertheme-elementor' ),
					'border-bottom' => __( 'Style Border Hr', 'movertheme-elementor' ),
				],
				'prefix_class' 	=> 'movertheme-heading-workingprocess-',
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label' 	=> __( 'Alignment', 'movertheme-elementor' ),
				'type' 		=> Controls_Manager::CHOOSE,
				'options' 	=> [
					'left' 		=> [
						'workingprocess' => __( 'Left', 'movertheme-elementor' ),
						'icon' 	=> 'fa fa-align-left',
					],
					'center' 	=> [
						'workingprocess' => __( 'Center', 'movertheme-elementor' ),
						'icon' 	=> 'fa fa-align-center',
					],
					'right' 	=> [
						'workingprocess' => __( 'Right', 'movertheme-elementor' ),
						'icon' 	=> 'fa fa-align-right',
					],
					'justify' 	=> [
						'workingprocess' => __( 'Justified', 'movertheme-elementor' ),
						'icon' 	=> 'fa fa-align-justify',
					],
				],
				'default' 	=> '',
				'selectors' => [
					'{{WRAPPER}}' => 'text-align: {{VALUE}};',
				],
				'separator' => 'before',
			]
		);



		$this->end_controls_section();






		$this->_border_style();
	}
	protected function render( ) {
		$settings = $this->get_settings();
		//$color = new Scheme_Color_Picker();
		//$values = $color->get_scheme_value();
		//var_dump($values);

		$this->furnish_render();
    }

    public function furnish_render() {
		$settings = $this->get_settings();
		//$color = new Scheme_Color_Picker();
		//$values = $color->get_scheme_value();
		//var_dump($values);

		//$lin = $settings['workingprocess_link'];
		//$workingprocess_html_tag = $settings['workingprocess_html_tag'];

	    //$document = \Elementor\Plugin::$instance->documents->get_doc_for_frontend( '361' );
		$this->add_render_attribute('workingprocess_content','class', 'workingprocess-content');
		$this->add_inline_editing_attributes( 'workingprocess_content', 'basic' );
		$banner_workingprocess = $settings['movertheme_workingprocess_txt'];
		$this->add_render_attribute('movertheme_workingprocess_txt','class', 'movertheme-heading-workingprocess');
		$this->add_inline_editing_attributes( 'movertheme_workingprocess_txt', 'basic' );
		//$workingprocess_html = sprintf( '<%1$s %2$s>%3$s </%1$s>', $settings['workingprocess_html_tag'], $this->get_render_attribute_string( 'movertheme_workingprocess_txt' ), $banner_workingprocess);

		/*Title Content*/
		//$workingprocess_content = $settings['workingprocess_content'];

		?>

		<div class="movertheme-workingprocess-custodian">

            <ul class="progress-list-rosary list-unstyled">
                <li class="progress-list-item --badge-pointer --separator --separator--line">
                    <div class="progress-list-item-content">
                        <div class="pli-icon-holder --round __pointer">
                            <div class="pli-icon-img">
                                <img src="<?php echo esc_url(ELEMENTOR_MOVERTHEME_ASSETS_URL . 'img/progress-icon.png'); ?>" alt="Some UnAwesome Image" class="img-fluid">
                            </div>
                            <div class="pli-progress-pointer">
                                <div class="pli-number">01</div>
                            </div>
                        </div>
                        <div class="pli-affinity-holder">
                            <div class="pli-affinity-reciter">Contact</div>
                        </div>
                    </div>
                    <div class="rosary-list-sep"></div>
                </li>
                <li class="progress-list-item --badge-pointer --separator --separator--line">
                    <div class="progress-list-item-content">
                        <div class="pli-icon-holder --round __pointer">
                            <div class="pli-icon-img">
                                <img src="<?php echo esc_url(ELEMENTOR_MOVERTHEME_ASSETS_URL . 'img/progress-icon.png'); ?>" alt="Some UnAwesome Image" class="img-fluid">
                            </div>
                            <div class="pli-progress-pointer">
                                <div class="pli-number">01</div>
                            </div>
                        </div>
                        <div class="pli-affinity-holder">
                            <div class="pli-affinity-reciter">Contact</div>
                        </div>
                    </div>
                    <div class="rosary-list-sep"></div>
                </li>
                <li class="progress-list-item --badge-pointer --separator --separator--line --last-child">
                    <div class="progress-list-item-content">
                        <div class="pli-icon-holder --round __pointer">
                            <div class="pli-icon-img">
                                <img src="<?php echo esc_url(ELEMENTOR_MOVERTHEME_ASSETS_URL . 'img/progress-icon.png'); ?>" alt="Some UnAwesome Image" class="img-fluid">
                            </div>
                            <div class="pli-progress-pointer">
                                <div class="pli-number">01</div>
                            </div>
                        </div>
                        <div class="pli-affinity-holder">
                            <div class="pli-affinity-reciter">Contact</div>
                        </div>
                    </div>
                </li>
            </ul>
		</div>
    <?php }
}
