<?php
namespace MoverthemeElementor\Modules\Movertheme\Widgets;

// You can add to or remove from this list - it's not conclusive! Chop & change to fit your needs.
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Background;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Repeater;
use Elementor\Scheme_Typography;
use Elementor\Widget_Base;
use MoverthemeElementor\Classes\Utils;
use WP_Query;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Movertheme_TeamMember extends Widget_Base {

	/* Uncomment the line below if you do not wish to use the function _content_template() - leave that section empty if this is uncommented! */
	//protected $_has_template_content = false;

	public function get_name() {
		return 'movertheme-teammember';
	}

	public function get_title() {
		return __( 'Team Member', 'movertheme-elementor' );
	}

	public function get_icon() {
		return 'eicon-person';
	}

	public function get_categories() {
		return [ 'elementor-movertheme-widgets'];
	}
	protected function controls_content_settings() {
        $this->start_controls_section(
            'section_content',
            [
                'label' => __( 'Content', 'movertheme-elementor' ),
            ]
        );
        // Add your widget/element content controls here! Below is an example control

        $this->add_control(
            'select_category',
            [
                'label' => __( 'Category:', 'movertheme-elementor' ),
                'type' => Controls_Manager::SELECT,
                'multiple' => true,
                'options' => Utils::movertheme_get_terms_by_posttype__taxonomies('team','team_cat'),
                'default' => [ 'NULL'],
            ]
        );
        $this->add_control(
            'post_number',
            [
                'label'         => __( 'Number of Member', 'movertheme-elementor' ),
                'type'          => Controls_Manager::NUMBER,
                'default'       => __( '6', 'movertheme-elementor' ),
                'content_classes' => 'elementor-panel-alert elementor-panel-alert-warning',
            ]
        );
		$this->add_control(
			'post_order_by',
			[
				'label'     => __( 'Order', 'movertheme-elementor' ),
				'type'      => Controls_Manager::SELECT,
				'default'   => 'DESC',
				'options'   => [
					'DESC' 		=> __( 'Descending', 'movertheme-elementor' ),
					'ASC' 		=> __( 'Ascending', 'movertheme-elementor' ),
				],
			]
		);
		$this->add_control(
			'box_columns',
			[
				'label'     => __( 'Columns:', 'movertheme-elementor' ),
				'type'      => Controls_Manager::SELECT,
				'default'   => 'DESC',
				'options'   => [
					'3' 		=> __( 'Four Column', 'movertheme-elementor' ),
					'4' 		=> __( 'Three Column', 'movertheme-elementor' ),
					'6' 		=> __( 'Two Column', 'movertheme-elementor' ),
					'12' 		=> __( 'One Column', 'movertheme-elementor' ),
				],
			]
		);


        $this->add_control(
            'show_member_name',
            [
                'label' => __( 'Show Name', 'movertheme-elementor' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __( 'Show', 'movertheme-elementor' ),
                'label_off' => __( 'Hide', 'movertheme-elementor' ),
                'return_value' => 'yes',
                'default' => 'yes',
                'separator' => 'before',
            ]
        );
        $this->add_control(
            'show_member_designation',
            [
                'label' => __( 'Show Designation', 'movertheme-elementor' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __( 'Show', 'movertheme-elementor' ),
                'label_off' => __( 'Hide', 'movertheme-elementor' ),
                'return_value' => 'yes',
                'default' => 'yes',
                'separator' => 'before',
            ]
        );
        $this->add_control(
            'show_member_number',
            [
                'label' => __( 'Show NUMBER', 'movertheme-elementor' ),
                'type' => Controls_Manager::HIDDEN,
                'label_on' => __( 'Show', 'movertheme-elementor' ),
                'label_off' => __( 'Hide', 'movertheme-elementor' ),
                'return_value' => 'yes',
                'default' => 'no',
                'separator' => 'before',
            ]
        );
        $this->add_control(
            'show_member_email',
            [
                'label' => __( 'Show Email', 'movertheme-elementor' ),
                'type' => Controls_Manager::HIDDEN,
                'label_on' => __( 'Show', 'movertheme-elementor' ),
                'label_off' => __( 'Hide', 'movertheme-elementor' ),
                'return_value' => 'yes',
                'default' => 'no',
                'separator' => 'before',
            ]
        );
        $this->add_control(
            'show_member_social',
            [
                'label' => __( 'Show Social', 'movertheme-elementor' ),
                'type' => Controls_Manager::HIDDEN,
                'label_on' => __( 'Show', 'movertheme-elementor' ),
                'label_off' => __( 'Hide', 'movertheme-elementor' ),
                'return_value' => 'yes',
                'default' => 'no',
                'separator' => 'before',
            ]
        );
        $this->add_control(
            'show_view_all_button',
            [
                'label' => __( 'Show Button', 'movertheme-elementor' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __( 'Show', 'movertheme-elementor' ),
                'label_off' => __( 'Hide', 'movertheme-elementor' ),
                'return_value' => 'yes',
                'default' => 'no',
                'separator' => 'before',
            ]
        );
        $this->add_control(
            'show_image_border',
            [
                'label' => __( 'Show Curved Border', 'movertheme-elementor' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __( 'Show', 'movertheme-elementor' ),
                'label_off' => __( 'Hide', 'movertheme-elementor' ),
                'return_value' => 'active',
                'default' => 'active',
                'separator' => 'before',
                'prefix_class' 	=> 'image-border-',
            ]
        );
        $this->add_control(
            'view',
            [
                'label' => __( 'View', 'movertheme-elementor' ),
                'type' => Controls_Manager::HIDDEN,
                'default' => 'traditional',
            ]
        );

        $this->end_controls_section();
    }
    protected function controls_post_button_element () {
        $this->start_controls_section(
            'controls_post_button_element',
            [
                'label' => __( 'View More Button Element', 'movertheme-elementor' ),

                'condition' => [
                    'show_view_all_button' => 'yes',
                ],
            ]
        );
        $this->add_control(
            'view_all_button_text',
            [
                'label'         => __( ' Button Text', 'movertheme-elementor' ),
                'type'          => Controls_Manager::TEXT,
                'label_block'   => false,
                'default'       => __( 'View All', 'movertheme-elementor' ),
                'separator'     => 'before',
                'condition' => [
                    'show_view_all_button' => 'yes',
                ],
            ]
        );

        $this->add_control(
            'read_more_button_url',
            [
                'label' => __( 'Button Url', 'movertheme-elementor' ),
                'type' => Controls_Manager::URL,
                'placeholder' => __( 'https://your-link.com', 'movertheme-elementor' ),
                'show_external' => true,
                'label_block'   => false,
                'default' => [
                    'url' => '',
                    'is_external' => true,
                    'nofollow' => true,
                ],
                'condition' => [
                    'show_view_all_button' => 'yes',
                ],
            ]
        );

        $this->add_responsive_control(
            'view_all_button_align',
            [
                'label' 	=> __( 'Alignment', 'movertheme-elementor' ),
                'type' 		=> Controls_Manager::CHOOSE,
                'options' 	=> [
                    'left' 		=> [
                        'title' => __( 'Left', 'movertheme-elementor' ),
                        'icon' 	=> 'fa fa-align-left',
                    ],
                    'center' 	=> [
                        'title' => __( 'Center', 'movertheme-elementor' ),
                        'icon' 	=> 'fa fa-align-center',
                    ],
                    'right' 	=> [
                        'title' => __( 'Right', 'movertheme-elementor' ),
                        'icon' 	=> 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .auth-callto-btn-wraper' => 'text-align: {{VALUE}} ;',
                ],
                'default' => 'left',
                'condition' => [
                    'show_view_all_button' => 'yes',
                ],
            ]
        );
	    $this->add_responsive_control(
		    'elementor_movertheme_viewall_button_padding',
		    [
			    'label' => __('Spacing', 'movertheme-elementor'),
			    'type' => Controls_Manager::DIMENSIONS,
			    'size_units' => [ 'px', '%', 'em' ],
			    'default' => [],
			    'selectors' => [
				    '{{WRAPPER}} .auth-callto-btn-wraper' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
			    ],
			    'separator' => 'before',
		    ]
	    );
        $this->end_controls_section();
    }

	/*Style Elements Member Box*/
	protected function _teammember_box_style() {
		$css_scheme_teammember_box = apply_filters(
			'movertheme-elementor/movertheme-teammember/css-scheme/teammember_box',
			array(
				'wrapper_box'         => '.section-meet_the_team .profile-card ',
			)
		);
		$this->start_controls_section(
			'teammember_box_style',
			[ 'label' 	=> __( 'Team Member Box Style', 'movertheme-elementor' ), 'tab' 		=> Controls_Manager::TAB_STYLE,]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'elementor_movertheme_teammember_background',
				'label' => __( 'Background', 'movertheme-elementor' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} '.$css_scheme_teammember_box['wrapper_box'],
			]
		);

		$this->add_control(
			'elementor_movertheme_teammember_color',
			[
				'label' => __( 'Color', 'movertheme-elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} '.$css_scheme_teammember_box['wrapper_box'] => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'elementor_movertheme_teammember_shadow',
				'label' => __( 'Box Shadow', 'movertheme-elementor' ),
				'selector' => '{{WRAPPER}} '.$css_scheme_teammember_box['wrapper_box'],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'elementor_movertheme_teammember_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => '{{WRAPPER}} '.$css_scheme_teammember_box['wrapper_box'],
			]
		);

		$this->add_responsive_control(
			'elementor_movertheme_teammember_padding',
			[
				'label' => __('Padding', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					'{{WRAPPER}} '.$css_scheme_teammember_box['wrapper_box'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_responsive_control(
			'elementor_movertheme_teammember_margin',
			[
				'label' => __('Margin', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					'{{WRAPPER}} '.$css_scheme_teammember_box['wrapper_box'] => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();
	}

	/*Style Elements Title*/
	protected function _teammember_title_style() {
		$css_scheme_teammember_title = apply_filters(
			'movertheme-elementor/movertheme-teammember/css-scheme/teammember_title',
			array(
				'title'         => ' .profile-card .fig-title',
			)
		);
		$this->start_controls_section(
			'teammember_title_style',
			[ 'label' 	=> __( 'Member Name', 'movertheme-elementor' ), 'tab' 		=> Controls_Manager::TAB_STYLE,]
		);
		$this->add_control(
			'elementor_movertheme_title_color',
			[
				'label' => __( 'Color', 'movertheme-elementor' ),
				'type' => Controls_Manager::COLOR,

				'scheme'	=> [
					'type'	=> Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_3,
				],

				'selectors' => [
					'{{WRAPPER}} '.$css_scheme_teammember_title['title'] => 'color: {{VALUE}};',
					'{{WRAPPER}} '.$css_scheme_teammember_title['title'].' a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'elementor_movertheme_title_typography',
				'selector' => '{{WRAPPER}} '.$css_scheme_teammember_title['title'],
			]
		);
		$this->add_group_control(
			Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'text_shadow',
				'label' => __( 'Text Shadow', 'movertheme-elementor' ),
				'selector' => '{{WRAPPER}} '.$css_scheme_teammember_title['title'],
			]
		);
		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'elementor_movertheme_title_shadow',
				'label' => __( 'Box Shadow', 'movertheme-elementor' ),
				'selector' => '{{WRAPPER}} '.$css_scheme_teammember_title['title'],
			]
		);
		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'elementor_movertheme_title_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => '{{WRAPPER}} '.$css_scheme_teammember_title['title'],
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'elementor_movertheme_title_background',
				'label' => __( 'Background', 'movertheme-elementor' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} '.$css_scheme_teammember_title['title'],
			]
		);

		$this->add_responsive_control(
			'elementor_movertheme_title_padding',
			[
				'label' => __('Spacing', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					'{{WRAPPER}} '.$css_scheme_teammember_title['title'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);
		$this->end_controls_section();
	}

	/*Style Elements Designation*/
	protected function _teammember_designation_style() {
		$css_scheme_teammember_designation = apply_filters(
			'movertheme-elementor/movertheme-teammember/css-scheme/teammember_designation',
			array(
				'designation'         => '{{WRAPPER}}  .profile-card .fig-title-des',
			)
		);
		$this->start_controls_section(
			'teammember_designation_style',
			[ 'label' 	=> __( 'Member Designation', 'movertheme-elementor' ), 'tab' 		=> Controls_Manager::TAB_STYLE,]
		);
		$this->add_control(
			'elementor_movertheme_designation_color',
			[
				'label' => __( 'Color', 'movertheme-elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					$css_scheme_teammember_designation['designation'] => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'elementor_movertheme_designation_typography',
				'selector' => $css_scheme_teammember_designation['designation'],
			]
		);
		$this->add_group_control(
			Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'text_shadow',
				'label' => __( 'Text Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_teammember_designation['designation'],
			]
		);
		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'elementor_movertheme_designation_shadow',
				'label' => __( 'Box Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_teammember_designation['designation'],
			]
		);
		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'elementor_movertheme_designation_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => $css_scheme_teammember_designation['designation'],
			]
		);

		$this->add_responsive_control(
			'elementor_movertheme_designation_padding',
			[
				'label' => __('Spacing', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					$css_scheme_teammember_designation['designation'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);
		$this->end_controls_section();
	}

	/*Style Elements Meta*/
	protected function _teammember_meta_style() {
		$css_scheme_teammember_meta = apply_filters(
			'movertheme-elementor/movertheme-teammember/css-scheme/teammember_meta',
			array(
				'meta'         => '{{WRAPPER}}  .profile-card .fig-meta',
			)
		);
		$this->start_controls_section(
			'teammember_meta_style',
			[ 'label' 	=> __( 'Profile Meta', 'movertheme-elementor' ), 'tab' 		=> Controls_Manager::TAB_STYLE,]
		);
		$this->add_control(
			'elementor_movertheme_meta_color',
			[
				'label' => __( 'Color', 'movertheme-elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					$css_scheme_teammember_meta['meta'] => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'elementor_movertheme_meta_typography',
				'selector' => $css_scheme_teammember_meta['meta'],
			]
		);
		$this->add_group_control(
			Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'text_shadow',
				'label' => __( 'Text Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_teammember_meta['meta'],
			]
		);
		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'elementor_movertheme_meta_shadow',
				'label' => __( 'Box Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_teammember_meta['meta'],
			]
		);
		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'elementor_movertheme_meta_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => $css_scheme_teammember_meta['meta'],
			]
		);

		$this->add_responsive_control(
			'elementor_movertheme_meta_padding',
			[
				'label' => __('Spacing', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					$css_scheme_teammember_meta['meta'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);
		$this->end_controls_section();
	}

	/*Style Elements Social*/
	protected function _teammember_social_style() {
		$css_scheme_teammember_social = apply_filters(
			'movertheme-elementor/movertheme-teammember/css-scheme/teammember_social',
			array(
				'social'         => '{{WRAPPER}} .profile-card .pfofile-social img',
			)
		);
		$this->start_controls_section(
			'teammember_social_style', [

				'label' 	=> __( 'Icon Settings', 'movertheme-elementor' ),
				'tab' 		=> Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_member_social' => 'yes',
				],
			]
		);


		$this->add_control(
			'social_icon_width',
			[
				'label' => __( 'Icon Size', 'movertheme-elementor' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 1,
					],
					'%' => [
						'min' => 0,
						'max' => 1000,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 50,
				],
				'selectors' => [
					$css_scheme_teammember_social['social'] => 'width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'elementor_movertheme_social_shadow',
				'label' => __( 'Box Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_teammember_social['social'],
			]
		);
		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'elementor_movertheme_social_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => $css_scheme_teammember_social['social'],
			]
		);
		$this->add_control(
			'social_icon_broder_radius',
			[
				'label' => __( 'Border Radius', 'movertheme-elementor' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 200,
						'step' => 1,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => '%',
					'size' => 0,
				],
				'selectors' => [
					$css_scheme_teammember_social['social'] => 'border-radius: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'elementor_movertheme_social_padding',
			[
				'label' => __('Spacing', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					$css_scheme_teammember_social['social'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);
		$this->end_controls_section();
	}

	protected function _register_controls() {
		$this->controls_content_settings();
		$this->controls_post_button_element();

		/*Style Element*/
		$this->_teammember_box_style();
		$this->_teammember_title_style();
		$this->_teammember_designation_style();
		$this->_teammember_meta_style();
		$this->_teammember_social_style();
	}

	protected function render() {
		$settings = $this->get_settings_for_display();

        $this->add_render_attribute( 'view_all_button_text', [
            'class' => 'btn btn-unsolemn btn-action '
        ] );

        $target = $settings['read_more_button_url']['is_external'] ? ' target="_blank"' : '';
        $nofollow = $settings['read_more_button_url']['nofollow'] ? ' rel="nofollow"' : '';

        ob_start();
        ?>
        <div class="section-meet_the_team">

            <div class="row">

                <?php

                $post_number = '-1';
                if ($settings['post_number']) {
	                $post_number = $settings['post_number'];
                }

                if (!($settings['select_category'] == 'NULL')) {
                    $tax_qury = array(
                        'relation' => 'AND',
                        array(
                            'taxonomy' => 'team_cat',
                            'field'    => 'term_id',
                            'terms'    => $settings['select_category'],
                        )
                    );
                }
                $args = array(
	                'posts_per_page'	=> $post_number,
                    'post_type' => 'team',
                    'order' => $settings['post_order_by'],
                    //'tax_query' => $tax_qury,
                );

                $query = new WP_Query($args);
                //var_dump($query);
                if ( $query->have_posts() ) :
                    while ( $query->have_posts() ) :
                        $query->the_post();
                        /*if( wp_get_attachment_image( get_post_meta( get_the_ID(), '_movertheme_team_profile_picture_id', true ), 'movertheme-team-thumbnail', false, array( 'class' => 'img-responsive', 'data-alt' => 'byu byu' ) ) ){
                            $profile_image = wp_get_attachment_image( get_post_meta( get_the_ID(),'_movertheme_team_profile_picture_id', true ), 'movertheme-team-thumbnail', false, array( 'class' => 'img-responsive' ) );
                        } else{
                            $profile_image = '';
                        }*/
	                    //$document = \Elementor\Plugin::$instance->documents->get_doc_for_frontend( '361' );


	                    /* grab the url for the full size featured image */
	                    $featured_img_url = get_the_post_thumbnail_url('full');
	                    $featured_img_url =  wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'single-post-thumbnail' );

                        /*Get Card all(Team member) Meta*/
                        $member_designation         = get_post_meta( get_the_ID(), '_movertheme_member_position', true );
                        $member_phone_number        = get_post_meta( get_the_ID(), '_movertheme_phone_number', true );
                        $member_email               = get_post_meta( get_the_ID(), '_movertheme_email', true );
                        $member_team_social         = get_post_meta( get_the_ID(), '_movertheme_team_social_option', true );
                        ?>

                        <div class="col-sm-4 col-md-<?php echo esc_attr($settings['box_columns']) ?> ">
                            <div class="profile-card profile-card-meta_center">
                                <figure>
                                    <div class="moverthemeprofile-pic-wrap">
                                        <a href="<?php the_permalink(); ?>" rel="lightbox"><img src ="<?php echo $featured_img_url [0] ?>"</img></a>
                                    </div>
                                    <figcaption>


                                        <?php if ($settings['show_member_name'] == 'yes'): ?>
                                            <h4 class="fig-title">
                                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                            </h4>
                                        <?php endif; ?>


                                        <?php if ($settings['show_member_designation'] == 'yes'): ?>
                                            <div class="fig-title-des"><?php echo $member_designation; ?></div>
                                        <?php endif; ?>

                                        <?php if (($settings['show_member_number'] == 'yes') || ($settings['show_member_email'] == 'yes')): ?>
                                            <div class="fig-meta">
                                                <?php if ($settings['show_member_number'] == 'yes'): ?>
                                                    <p class="fig-cal"><strong><?php echo __('Call:', 'movertheme-elementor'); ?></strong> <span><?php echo $member_phone_number;?></span></p>
                                                <?php endif; ?>

                                                <?php if ($settings['show_member_email'] == 'yes'): ?>
                                                    <p class="fig-mail"><strong><?php echo __('Email:', 'movertheme-elementor'); ?></strong> <span><?php echo $member_email; ?></span></p>
                                                <?php endif; ?>
                                            </div>
                                        <?php endif; ?>
                                    </figcaption>
                                </figure>
                                <?php if (($settings['show_member_social'] == 'yes') && !empty($member_team_social)): ?>
                                    <div class="profile-card-meta">
                                        <ul class="pfofile-social list-unstyled list-inline">
                                            <?php foreach ($member_team_social as $social) :  ?>

                                                <li>
                                                    <a href="<?php echo esc_url($social['_movertheme_socail_link']);?>">
                                                        <i alt="moverthemesocial" class="fa <?php echo $social['_movertheme_social_icon'];?>"></i>
                                                    </a>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
	        <?php if ($settings['show_view_all_button'] == 'yes'): ?>
                <div class="auth-callto-btn-wraper">
                    <a href="<?php echo $settings['read_more_button_url']['url']; ?>" <?php echo $target . $nofollow; ?>>
                        <div <?php echo $this->get_render_attribute_string( 'view_all_button_text' ); ?>> <?php echo $settings['view_all_button_text'];?></div>
                    </a>
                </div>
	        <?php endif; ?>
        </div>
        <?php $buffer = ob_get_flush();
	}
}
