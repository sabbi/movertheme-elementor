<?php
namespace MoverthemeElementor\Modules\Movertheme\Widgets;

// You can add to or remove from this list - it's not conclusive! Chop & change to fit your needs.
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Background;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Repeater;
use Elementor\Scheme_Typography;
use Elementor\Widget_Base;
use WP_Query;
use DateTime;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Movertheme_Events_Lists extends Widget_Base {

	/* Uncomment the line below if you do not wish to use the function _content_template() - leave that section empty if this is uncommented! */
	//protected $_has_template_content = false; 
	
	public function get_name() {
		return 'movertheme-movertheme';
	}

	public function get_title() {
		return __( 'Events Lists', 'movertheme-elementor' );
	}

	public function get_icon() {
		return 'eicon-posts-grid';
	}

	public function get_categories() {
		return [ 'elementor-movertheme-widgets'];
	}

	protected function controls_content_settings() {
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Post Content', 'movertheme-elementor' ),
			]
		);
		// Add your widget/element content controls here! Below is an example control

		$this->add_control(
			'post_number',
			[
				'label'         => __( 'Number of Event', 'movertheme-elementor' ),
				'type'          => Controls_Manager::NUMBER,
				'default'       => __( '6', 'movertheme-elementor' ),
				'content_classes' => 'elementor-panel-alert elementor-panel-alert-warning balbal bal',
			]
		);

		$this->add_control(
			'post_order_by',
			[
				'label'     => __( 'Order', 'movertheme-elementor' ),
				'type'      => Controls_Manager::SELECT,
				'default'   => 'DESC',
				'options'   => [
					'DESC' 		=> __( 'Descending', 'movertheme-elementor' ),
					'ASC' 		=> __( 'Ascending', 'movertheme-elementor' ),
				],
			]
		);


		$this->add_control(
			'date_format',
			[
				'label'         => __( 'Date Format:', 'movertheme-elementor' ),
				'type'          => Controls_Manager::TEXT,
				'label_block'   => false,
				'default'       => __( 'jS F Y', 'movertheme-elementor' ),
			]
		);

		$this->add_control(
			'show_pagination',
			[
				'label' => __( 'Show Pagination', 'movertheme-elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'movertheme-elementor' ),
				'label_off' => __( 'Hide', 'movertheme-elementor' ),
				'return_value' => 'yes',
				'default' => 'yes',
				'separator' => 'before',
			]
		);

		$this->add_control(
			'view',
			[
				'label' => __( 'View', 'movertheme-elementor' ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

		$this->end_controls_section();
	}

	/*Style Elements Title*/
	/*Style Elements Title*/
	protected function _events_title_style() {
		$css_scheme_events_title = apply_filters(
			'movertheme-elementor/movertheme-events/css-scheme/events_title',
			array(
				'title'         => ' .stage_events_post .moverthemeevents-item .moverthemeevents-title ',
			)
		);
		$this->start_controls_section(
			'events_title_style',
			[ 'label' 	=> __( 'Member Name', 'movertheme-elementor' ), 'tab' 		=> Controls_Manager::TAB_STYLE,]
		);
		$this->add_control(
			'elementor_movertheme_events_title_color',
			[
				'label' => __( 'Color', 'movertheme-elementor' ),
				'type' => Controls_Manager::COLOR,

				'scheme'	=> [
					'type'	=> Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_3,
				],

				'selectors' => [
					'{{WRAPPER}} '.$css_scheme_events_title['title'] => 'color: {{VALUE}};',
					'{{WRAPPER}} '.$css_scheme_events_title['title'].' a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'elementor_movertheme_events_title_typography',
				'selector' => '{{WRAPPER}} '.$css_scheme_events_title['title'],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'elementor_movertheme_events_title_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => '{{WRAPPER}} '.$css_scheme_events_title['title'],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'elementor_movertheme_events_title_shadow',
				'label' => __( 'Box Shadow', 'movertheme-elementor' ),
				'selector' => '{{WRAPPER}} '.$css_scheme_events_title['title'],
			]
		);


		$this->add_responsive_control(
			'elementor_movertheme_events_title_padding',
			[
				'label' => __('Spacing', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					'{{WRAPPER}} '.$css_scheme_events_title['title'].' a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);
		$this->end_controls_section();
	}
	/*Style Elements Item Meta*/
	protected function _events_item_meta_style() {
		$css_scheme_events_item_meta = apply_filters(
			'movertheme-elementor/movertheme-events/css-scheme/events_item_meta',
			array(
				'item_meta'         => ' .stage_events_post .moverthemeevents-item .events-item-meta ',
			)
		);
		$this->start_controls_section(
			'events_item_meta_style',
			[ 'label' 	=> __( 'Item Meta', 'movertheme-elementor' ), 'tab' 		=> Controls_Manager::TAB_STYLE,]
		);
		$this->add_control(
			'elementor_movertheme_events_item_meta_color',
			[
				'label' => __( 'Color', 'movertheme-elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} '.$css_scheme_events_item_meta['item_meta'] => 'color: {{VALUE}};',
					'{{WRAPPER}} '.$css_scheme_events_item_meta['item_meta'].' a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'elementor_movertheme_events_item_meta_typography',
				'selector' => '{{WRAPPER}} '.$css_scheme_events_item_meta['item_meta'],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'elementor_movertheme_events_item_meta_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => '{{WRAPPER}} '.$css_scheme_events_item_meta['item_meta'],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'elementor_movertheme_events_item_meta_shadow',
				'label' => __( 'Box Shadow', 'movertheme-elementor' ),
				'selector' => '{{WRAPPER}} '.$css_scheme_events_item_meta['item_meta'],
			]
		);


		$this->add_responsive_control(
			'elementor_movertheme_events_item_meta_padding',
			[
				'label' => __('Spacing', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					'{{WRAPPER}} '.$css_scheme_events_item_meta['item_meta'].' a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);
		$this->end_controls_section();
	}

	protected function _register_controls() {

		$this->controls_content_settings();
		$this->_events_title_style();
		$this->_events_item_meta_style();

	}
		
	protected function render() {
		$settings = $this->get_settings();

		/*Get needed post var*/
		$count          = $settings['post_number'];
		$order          = $settings['post_order_by'];
		$order          = $settings['post_order_by'];
		$paged          = '';
		$date_format    = $settings['date_format'];
		$pagination_visibility  = $settings['show_pagination'];

		if ( get_query_var('paged') ) {
		    $paged = get_query_var('paged');
		} elseif ( get_query_var('page') ) {
		    $paged = get_query_var('page'); } else { $paged = 1;
		}
		$args = array(
			'post_type'       => 'post',
			'posts_per_page'  => $count,
			'order' => $order,
			'orderby' => 'ID',
			'post_status' => 'publish',
			'paged' => $paged
		);

		$event_post_holder = new WP_Query( $args );

		if( $event_post_holder->have_posts() ){

			?>

            <div class="movertheme-content-area page-events stage_events_post">
                <div class="events_post-sagment-wrap">
                    <div class="events_post-sagment container-sagment">

                        <div class="row">
							<?php
							while ( $event_post_holder->have_posts() ) : $event_post_holder->the_post();

								$idd = get_the_ID();
								$prefix = '_movertheme_';

								$event_image_id = get_post_meta( $idd, $prefix . 'event_image_id', 1 );
								if( wp_get_attachment_image( $event_image_id , 'movertheme-event-thumbnail', false, array( 'class' => 'moverthemeevents-img img-responsive' ) ) ){
									$event_image = wp_get_attachment_image( $event_image_id, 'movertheme-event-thumbnail', false, array( 'class' => 'moverthemeevents-img img-responsive' ) );
								} else{
									$event_image = '';
								}


								if( get_post_meta( $idd , $prefix . 'event_location', true) ){
									$event_location = get_post_meta( $idd , $prefix . 'event_location', true);
								} else{
									$event_location = '';
								}

								if( get_post_meta( $idd , $prefix . 'event_date', true) ){
									$event_date = get_post_meta( $idd , $prefix . 'event_date', true);
								} else{
									$event_date = '';
								}

								$date = new DateTime( $event_date );

								$date = $date->format( $date_format );

								?>
                                <div class="col-sm-6">
                                    <article class="moverthemeevents-item">
                                        <a href="<?php esc_url( the_permalink() ); ?>" class="moverthemeevents-link">
                                            <figure>
												<?php echo wp_kses_post( $event_image ); ?>
                                                <figcaption>
                                                    <h2 class="moverthemeevents-title font-md__x"><?php the_title(); ?></h2>
                                                </figcaption>
                                            </figure>
                                        </a>
                                        <div class="events-item-meta">
											<?php if( !empty( $event_location ) ) { ?>
                                                <div class="events-loc"><i class="ion-location"></i><span class="text"><?php echo wp_kses_post($event_location); ?></span></div>
											<?php } ?>
											<?php if( !empty( $event_date ) ) { ?>
                                                <div class="events-date">
                                                    <i class="ion-calendar"></i>
                                                    <span class="text">
                                                        <?php echo wp_kses_post($date); ?>
                                                    </span>
                                                </div>
											<?php } ?>
                                        </div>
                                    </article><!-- /.moverthemeevents-item -->
                                </div>
							<?php endwhile; ?>
                        </div>

                    </div><!-- /.events_post-sagment -->


					<?php if( $pagination_visibility == 'yes' ) { ?>
                        <nav aria-label="Page navigation" class="pagination_wraper">
							<?php

							$numpages = $event_post_holder->max_num_pages;

							if ($numpages == '') {
								global $wp_query;
								$numpages = $wp_query->max_num_pages;
								if(!$numpages) {
									$numpages = 1;
								}
							}

							$big = 999999999; // need an unlikely integer
							echo paginate_links( array(
								'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
								'format'       => '',
								'add_args'     => '',
								'current'      => max( 1, get_query_var( 'page' ) ),
								'total'        => $numpages,
								'prev_text'    => '<i class="ion-arrow-left-c"></i>' .esc_html__( 'Prev', 'movertheme-elementor' ),
								'next_text'    => esc_html__( 'Next', 'movertheme-elementor' ) . '<i class="ion-arrow-right-c"></i>',
								'type'         => 'list',
								'end_size'     => 2,
								'mid_size'     => 2
							) );
							?>
                        </nav>
					<?php } ?>

                </div>
            </div><!-- #primary -->
		<?php }
		wp_reset_postdata();
	}
}
