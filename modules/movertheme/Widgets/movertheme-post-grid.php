<?php
namespace MoverthemeElementor\Modules\Movertheme\Widgets;

// You can add to or remove from this list - it's not conclusive! Chop & change to fit your needs.
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Background;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Repeater;
use Elementor\Scheme_Typography;
use Elementor\Widget_Base;
use MoverthemeElementor\Classes\Utils;
use \WP_Query;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Movertheme_Post_Grid extends Widget_Base {

	/* Uncomment the line below if you do not wish to use the function _content_template() - leave that section empty if this is uncommented! */
	//protected $_has_template_content = false; 

    public function get_name() {
        return 'movertheme-posts-grid';
    }

    public function get_title() {
        return __( 'Movertheme Some Posts Grid', 'movertheme-core' );
    }

    public function get_icon() {
        return 'eicon-gallery-grid';
    }

    public function get_categories() {
        return [ 'elementor-movertheme-widgets' ];
    }
    protected function get_post_category() {
        $categories = get_terms( 'category', 'orderby=count&hide_empty=0' );
        $new_cat_array = array('allpost' => 'All Category');
        foreach ($categories as $category) {
            $new_cat_array[$category->slug]=$category->name;
        }
        return $new_cat_array;
    }
    protected function ControlsPostElement () {

        $this->add_control(
            'post_number',
            [
                'label'         => __( 'Number of Posts', 'movertheme-core' ),
                'type'          => Controls_Manager::NUMBER,
                'label_block'   => true,
                'default'       => __( '6', 'movertheme-core' ),

            ]
        );
        $this->add_control(
            'post_column',
            [
                'label'     => __( 'Number of Column', 'movertheme-core' ),
                'type'      => Controls_Manager::SELECT,
                'default'   => 4,
                'options'   => [
                    '12' 	=> __( 'One Column', 'movertheme-core' ),
                    '6' 	=> __( 'Two Column', 'movertheme-core' ),
                    '4' 	=> __( 'Three Column', 'movertheme-core' ),
                    '3' 	=> __( 'Four Column', 'movertheme-core' ),
                ]
            ]
        );
        $this->add_control(
            'post_cat',
            [
                'label'    => __( 'Category', 'movertheme-core' ),
                'type'     => Controls_Manager::SELECT,
                'options'  => $this->get_post_category(),
                'multiple' => true,
                'default'  => 'allpost'
            ]
        );
        $this->add_control(
            'post_order_by',
            [
                'label'     => __( 'Order', 'movertheme-core' ),
                'type'      => Controls_Manager::SELECT,
                'default'   => 'DESC',
                'options'   => [
                    'DESC' 		=> __( 'Descending', 'movertheme-core' ),
                    'ASC' 		=> __( 'Ascending', 'movertheme-core' ),
                ],
            ]
        );
        $this->add_control(
            'text_align',
            [
                'label' => __( 'Alignment', 'plugin-domain' ),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __( 'Left', 'plugin-domain' ),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __( 'Center', 'plugin-domain' ),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __( 'Right', 'plugin-domain' ),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'default' => 'center',
                'toggle' => true,
                'selectors' => [
                    '{{WRAPPER}}' => 'text-align: {{VALUE}};',
                ]
            ]
        );

        $this->add_control(
            'read_more_button_text',
            [
                'label'         => __( 'View More Text', 'movertheme-core' ),
                'type'          => Controls_Manager::TEXT,
                'label_block'   => false,
                'default'       => __( 'Read All', 'movertheme-core' ),
                'separator'     => 'before'
            ]
        );
        $this->add_control(
            'read_more_button_url',
            [
                'label'         => __( 'View More Url', 'movertheme-core' ),
                'type'          => Controls_Manager::URL,
                'label_block'   => false,
                'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
                'show_external' => true,
                'default' => [
                    'url' => '',
                    'is_external' => false,
                    'nofollow' => false,
                ]

            ]
        );


    }
    protected function TabStyleEntryTitle () {
        $this->add_control(
            'title_color',
            [
                'label' 	=> __( 'Title Colors', 'movertheme-core' ),
                'type' 		=> Controls_Manager::COLOR,
                'scheme' 	=> [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .movertheme-blog-post-grid .entry-title a' => 'color: {{VALUE}};',
                ],
            ]
        );
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' 		=> 'typography',
                'scheme' 	=> Scheme_Typography::TYPOGRAPHY_1,
                'selector' 	=> '{{WRAPPER}} .movertheme-blog-post-grid .entry-title a',
            ]
        );
    }
    protected function TabStyleEntryContent() {
        $this->add_control(
            'content_color',
            [
                'label' 	=> __( 'Content Color', 'movertheme-core' ),
                'type' 		=> Controls_Manager::COLOR,
                'scheme' 	=> [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors' => [
                    '{{WRAPPER}} .movertheme-blog-post-grid .entry-content,{{WRAPPER}} .movertheme-blog-post-grid .byline a ' => 'color: {{VALUE}};',
                ],
                'default' => ''
            ]
        );
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' 		=> 'entry_content_typography',
                'scheme' 	=> Scheme_Typography::TYPOGRAPHY_3,
                'selector' 	=> '{{WRAPPER}} .movertheme-blog-post-grid .movertheme-blog-post-item .entry-content,{{WRAPPER}} .movertheme-blog-post-grid .post-btn-wrap',
            ]
        );
    }
    protected function _register_controls() {

        $this->start_controls_section(
            'movertheme_post_grid',
            [
                'label' 	=> __( 'Post Element', 'movertheme-core' )
            ]
        );
        $this->ControlsPostElement ();
        $this->end_controls_section();
        #TabStyle Title Section
        $this->start_controls_section(
            'section_title_style',
            [
                'label' 	=> __( 'Title', 'movertheme-core' ),
                'tab' 		=> Controls_Manager::TAB_STYLE,
            ]
        );
        $this->TabStyleEntryTitle ();
        $this->end_controls_section();
        # Title Section End
        #TabStyle Entry Content Section
        $this->start_controls_section(
            'section_price_style',
            [
                'label' 	=> __( 'Entry Content', 'movertheme-core' ),
                'tab' 		=> Controls_Manager::TAB_STYLE,
            ]
        );
        $this->TabStyleEntryContent();
        $this->end_controls_section();
        #End TabStyle Entry Content Section


        #TabStyle Read More Section
        $this->start_controls_section(
            'section_readmore_style',
            [
                'label' 	=> __( 'View More', 'movertheme-core' ),
                'tab' 		=> Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' 		=> 'readmore_typography',
                'scheme' 	=> Scheme_Typography::TYPOGRAPHY_2,
                'selector' 	=> '{{WRAPPER}} .movertheme-blog-post-grid .view_more-btn-wrap a',
            ]
        );
        $this->add_control(
            'button_text_align',
            [
                'label' => __( 'Alignment', 'plugin-domain' ),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __( 'Left', 'plugin-domain' ),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __( 'Center', 'plugin-domain' ),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __( 'Right', 'plugin-domain' ),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'default' => 'left',
                'toggle' => true,
                'selectors' => [
                    '{{WRAPPER}} .view_more-btn-wrap' => 'text-align: {{VALUE}};',
                ]
            ]
        );

        $this->end_controls_section();
        # End TabStyle Read More Section
    } # function _register_controls end

    protected function render( ) {

        $settings 			= $this->get_settings();
        $post_number 		= $settings['post_number'];
        $post_column 		= $settings['post_column'];
        $post_cat 			= $settings['post_cat'];
        $post_order_by 		= $settings['post_order_by'];
        $target = $settings['read_more_button_url']['is_external'] ? ' target="_blank"' : '';
        $nofollow = $settings['read_more_button_url']['nofollow'] ? ' rel="nofollow"' : '';
        //var_dump($post_number);
        # Query Build.
        $arg = array(
            'post_type'   =>  'post',
            'post_status' => 'publish',
            //'numberposts' => '2',
        );
        if( $post_order_by ){
            $arg['order'] = $post_order_by;
        }
        $arg['posts_per_page'] = 1;
        if( $post_cat ){
            if( $post_cat != 'allpost' ){
                $cat_data = array();
                $cat_data['relation'] = 'AND';
                $cat_data[] = array(
                    'taxonomy' 	=> 'category',
                    'field' 	=> 'slug',
                    'terms' 	=> $post_cat
                );
                $arg['tax_query'] = $cat_data;
            }
        }

        $postblock = new WP_Query( $arg );  ?>

        <article class="movertheme-blog-post-grid">
            <div class="row">
                <?php if ( $postblock->have_posts() ) : $i = 0;?>
                    <?php while ( $postblock->have_posts() ) : $postblock->the_post();

                        $permalink 	= get_permalink();
                        $title 		= get_the_title();
                        $media_url 	= wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), "movertheme-large" );
                        $media_url  = ( isset( $media_url[0] ) ) ? $media_url[0] : '';
                        $excerpt_limit = '125';
                        ?>
                        <div class="movertheme-blog-post-item-col col-md-6 col-lg-<?php echo $post_column; ?>">
                            <div class="movertheme-blog-post-item">
                                <header class="article-header thumb_exist">

                                </header>
                                <section class="entry-content thumb_exist">
                                    <h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title();?></a></h2>
                                    <div class="movertheme-byline">
                                        <?php
                                            Utils::movertheme_core_posted_on(true);
                                            echo '<span class="post-grid-sep d-none">'.esc_attr('|','movertheme-core').'</span>';
                                            Utils::movertheme_core_post_comments();
                                        ?>
                                    </div>

                                    <div class="post-excerpt">
                                        <p><?php echo Utils::get_excerpt(100); ?></p>
                                    </div>
                                </section>
                                <section class="blogpost-image-section">
                                    <div class="blogpost-image-wrap">
                                        <?php the_post_thumbnail('movertheme-post-grid'); ?>
                                    </div>
                                </section>
                                <footer>
                                    <div class="post-btn-wrap">
                                        <a href="#" class="more-link"><span class="more-button"><?php echo __('Continue Reding', 'movertheme-elementor'); ?></span><span class="more-line"> <i class="fas fa-arrow-right"></i></span> </a>
                                    </div>
                                </footer>
                            </div>
                        </div>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); wp_reset_query();

                    ?>
                <?php endif; ?>
            </div>

            <div class="view_more-btn-wrap text-center">
                <a class="view_more-btn" href="<?php echo $settings['read_more_button_url']['url']; ?>" <?php  $target . $nofollow; ?>> <?php echo $settings['read_more_button_text']; ?> <i class="fas fa-arrow-alt-circle-right"></i></a>
            </div>
        </article>
    <?php }
}
