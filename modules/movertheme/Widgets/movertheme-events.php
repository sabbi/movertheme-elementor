<?php
namespace MoverthemeElementor\Modules\Movertheme\Widgets;

// You can add to or remove from this list - it's not conclusive! Chop & change to fit your needs.
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Background;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Repeater;
use Elementor\Scheme_Typography;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Movertheme_Events extends Widget_Base {

	/* Uncomment the line below if you do not wish to use the function _content_template() - leave that section empty if this is uncommented! */
	//protected $_has_template_content = false; 
	
	public function get_name() {
		return 'events-extracto';
	}

	public function get_title() {
		return __( 'Latest Events', 'movertheme-elementor' );
	}

	public function get_icon() {
		return 'eicon-post-list';
	}

	public function get_categories() {
		return [ 'elementor-movertheme-widgets'];
	}

    protected function get_post_category() {
        $categories = get_terms( 'category', 'orderby=count&hide_empty=0' );
        $new_cat_array = array('allpost' => 'All Category');
        foreach ($categories as $category) {
            $new_cat_array[$category->slug]=$category->name;
        }
        return $new_cat_array;
    }
    protected function movertheme_get_posttype() {

        $post_types = get_post_types(array('public' => true), 'objects');

        $options = ['' => ''];

        foreach ($post_types as $post_type) {
            $exclude = array( 'My Templates','Media','Pages','Menu Items');

            if( TRUE === in_array( $post_type->label, $exclude ) ) {

            }else {
                $options[$post_type->name] = $post_type->label;
            }

        }

        return apply_filters('lae_post_type_options', $options);
    }
    protected function ControlsWrapperElement () {
        $this->add_control(
            'wrapper_title',
            [
                'label' => __( 'Title', 'movertheme-elementor' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __( 'Latest Events', 'movertheme-elementor' ),
                'placeholder' => __( 'Latest Events', 'movertheme-elementor' ),
            ]
        );
    }
    protected function ControlsPostElement () {
        $this->add_control(
            'post_types',
            [
                'label' => __('Post Types', 'livemesh-el-addons'),
                'type' => Controls_Manager::SELECT,
                'default' => 'post',
                'options' => $this->movertheme_get_posttype(),
            ]
        );


        $this->add_control(
            'post_number',
            [
                'label'         => __( 'Number of Posts', 'movertheme-elementor' ),
                'type'          => Controls_Manager::NUMBER,
                'default'       => __( '6', 'movertheme-elementor' ),

            ]
        );

        $this->add_control(
            'post_order_by',
            [
                'label'     => __( 'Order', 'movertheme-elementor' ),
                'type'      => Controls_Manager::SELECT,
                'default'   => 'DESC',
                'options'   => [
                    'DESC' 		=> __( 'Descending', 'movertheme-elementor' ),
                    'ASC' 		=> __( 'Ascending', 'movertheme-elementor' ),
                ],
            ]
        );
        $this->add_control(
            'text_align',
            [
                'label' => __( 'Alignment', 'movertheme-elementor' ),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __( 'Left', 'movertheme-elementor' ),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __( 'Center', 'movertheme-elementor' ),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __( 'Right', 'movertheme-elementor' ),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'default' => 'left',
                'toggle' => true,
                'selectors' => [
                    '{{WRAPPER}}' => 'text-align: {{VALUE}};',
                ]
            ]
        );
    }
    protected function ControlsPostButtonElement () {
        $this->add_control(
            'view_all_button_text',
            [
                'label'         => __( 'View All Button Text', 'movertheme-elementor' ),
                'type'          => Controls_Manager::TEXT,
                'label_block'   => false,
                'default'       => __( 'View All', 'movertheme-elementor' ),
                'separator'     => 'before'
            ]
        );
        $this->add_control(
            'show_link_manually',
            [
                'label' => __( 'Add archive link Manually', 'movertheme-elementor' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __( 'Yes', 'movertheme-elementor' ),
                'label_off' => __( 'No', 'movertheme-elementor' ),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );
        $this->add_control(
            'read_more_button_url',
            [
                'label'         => __( 'All Post Url', 'movertheme-elementor' ),
                'type'          => Controls_Manager::URL,
                'label_block'   => false,
                'placeholder' => __( 'https://your-link.com', 'movertheme-elementor' ),
                'show_external' => false,
                'default' => [
                    'url' => '#',
                    'is_external' => false,
                    'nofollow' => false,
                ],
                'condition' => [
                    'show_link_manually' => 'yes',
                ],

            ]
        );

        $this->add_control(
            'hide_view_all_button',
            [
                'label' => __( 'Hide This Button', 'movertheme-elementor' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __( 'Yes', 'movertheme-elementor' ),
                'label_off' => __( 'No', 'movertheme-elementor' ),
                'return_value' => 'yes',
                'default' => 'no',
            ]
        );
        $this->add_responsive_control(
            'view_all_button_align',
            [
                'label' 	=> __( 'Alignment', 'movertheme-elementor' ),
                'type' 		=> Controls_Manager::CHOOSE,
                'options' 	=> [
                    'left' 		=> [
                        'title' => __( 'Left', 'movertheme-elementor' ),
                        'icon' 	=> 'fa fa-align-left',
                    ],
                    'center' 	=> [
                        'title' => __( 'Center', 'movertheme-elementor' ),
                        'icon' 	=> 'fa fa-align-center',
                    ],
                    'right' 	=> [
                        'title' => __( 'Right', 'movertheme-elementor' ),
                        'icon' 	=> 'fa fa-align-right',
                    ],
                    'justify' 	=> [
                        'title' => __( 'Justified', 'movertheme-elementor' ),
                        'icon' 	=> 'fa fa-align-justify',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .view_more-btn-wrap' => 'text-align: {{VALUE}} ;',
                ],
                'default' => 'left',
            ]
        );
    }
    protected function TabStyleWrapperTitle () {
        $this->add_control(
            'wrapper_title_color',
            [
                'label' 		=> __( 'Color', 'movertheme-elementor' ),
                'type' 			=> Controls_Manager::COLOR,
                'scheme' 		=> [
                    'type' 	=> Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_3,
                ],
                'selectors' 	=> [
                    '{{WRAPPER}} .events-post-wrapper .stage-title' => 'color: {{VALUE}};',
                ],
            ]
        );
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' 		=> 'wrapper_title_typography',
                'scheme' 	=> Scheme_Typography::TYPOGRAPHY_1,
                'selector' 	=> '{{WRAPPER}} .events-post-wrapper .stage-title',
            ]
        );

        $this->add_responsive_control(
            'wrapper_title_padding',
            [
                'label' 		=> __('Padding', 'movertheme-elementor'),
                'type'			=> Controls_Manager::DIMENSIONS,
                'size_units' 	=> [ 'px', '%', 'em' ],
                'default' 		=> [],
                'selectors' 	=> [
                    '{{WRAPPER}} .events-post-wrapper .stage-title' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'wrapper_title_margin',
            [
                'label' 		=> __('Margin', 'movertheme-elementor'),
                'type' 			=> Controls_Manager::DIMENSIONS,
                'size_units' 	=> [ 'px', '%', 'em' ],
                'default' 		=> [],
                'selectors' 	=> [
                    '{{WRAPPER}} .events-post-wrapper .stage-title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );
    }
    protected function TabStyleEntryTitle () {
        $this->add_control(
            'title_color',
            [
                'label' 		=> __( 'Color', 'movertheme-elementor' ),
                'type' 			=> Controls_Manager::COLOR,
                'scheme' 		=> [
                    'type' 	=> Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_3,
                ],
                'selectors' 	=> [
                    '{{WRAPPER}} .events-post-wrapper .lst_news_item .title a' => 'color: {{VALUE}};',
                ],
            ]
        );
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' 		=> 'title_typography',
                'scheme' 	=> Scheme_Typography::TYPOGRAPHY_1,
                'selector' 	=> '{{WRAPPER}} .events-post-wrapper .lst_news_item .title',
            ]
        );

        $this->add_responsive_control(
            'title_padding',
            [
                'label' 		=> __('Padding', 'movertheme-elementor'),
                'type'			=> Controls_Manager::DIMENSIONS,
                'size_units' 	=> [ 'px', '%', 'em' ],
                'default' 		=> [],
                'selectors' 	=> [
                    '{{WRAPPER}} .events-post-wrapper .lst_news_item .title' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'title_margin',
            [
                'label' 		=> __('Margin', 'movertheme-elementor'),
                'type' 			=> Controls_Manager::DIMENSIONS,
                'size_units' 	=> [ 'px', '%', 'em' ],
                'default' 		=> [],
                'selectors' 	=> [
                    '{{WRAPPER}} .events-post-wrapper .lst_news_item .title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );
    }
    protected function TabStyleEntryContent() {
        $this->add_control(
            'content_meta_color',
            [
                'label' 	=> __( 'Color', 'movertheme-elementor' ),
                'type' 		=> Controls_Manager::COLOR,
                'scheme' 	=> [
                    'type' 	=> Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_3,
                ],
                'selectors' => [
                    '{{WRAPPER}} .event-meta span' => 'color: {{VALUE}};',
                ],
                'default' 	=> ''
            ]
        );
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' 		=> 'entry_content_meta_typography',
                'scheme' 	=> Scheme_Typography::TYPOGRAPHY_3,
                'selector' 	=> '{{WRAPPER}} .event-meta span',
            ]
        );
        $this->add_responsive_control(
            'entry_content_meta_padding',
            [
                'label' 		=> __('Padding', 'movertheme-elementor'),
                'type' 			=> Controls_Manager::DIMENSIONS,
                'size_units' 	=> [ 'px', '%', 'em' ],
                'default' 		=> [],
                'selectors' 	=> [
                    '{{WRAPPER}} .event-meta span' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'separator' 	=> 'before',
            ]
        );

        $this->add_responsive_control(
            'entry_content_meta_margin',
            [
                'label' 		=> __('Margin', 'movertheme-elementor'),
                'type' 			=> Controls_Manager::DIMENSIONS,
                'size_units'	=> [ 'px', '%', 'em' ],
                'default'		=> [],
                'selectors'		=> [
                    '{{WRAPPER}}  .event-meta' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'separator'	=> 'before',
            ]
        );
    }
    protected function _register_controls() {

        $this->start_controls_section(
            'movertheme_wrapper_grid',
            [
                'label' 	=> __( 'Wrapper Content', 'movertheme-elementor' ),
                'condition' => [
                    'show_link_manually' => 'yes',
                ],
            ]
        );
        $this->ControlsWrapperElement ();
        $this->end_controls_section();
        #End Posts element

        $this->start_controls_section(
            'movertheme_post_grid',
            [
                'label' 	=> __( 'Post Element', 'movertheme-elementor' )
            ]
        );
        $this->ControlsPostElement ();
        $this->end_controls_section();
        #End Posts element

        $this->start_controls_section(
            'movertheme_filterable_post_grid_buttton',
            [
                'label' 	=> __( 'Button Element', 'movertheme-elementor' )
            ]
        );
        $this->ControlsPostButtonElement();
        $this->end_controls_section();
        #End Button element

        ##TabStyle TabStyleWrapperTitle
        $this->start_controls_section(
            'movertheme_wrappertitle_style',
            [
                'label' 	=> __( 'Wrapper Title', 'movertheme-elementor' ),
                'tab' 		=> Controls_Manager::TAB_STYLE,
            ]
        );
        $this->TabStyleWrapperTitle();
        $this->end_controls_section();
        #TabStyle Title Section
        $this->start_controls_section(
            'section_title_style',
            [
                'label' 	=> __( 'Title', 'movertheme-elementor' ),
                'tab' 		=> Controls_Manager::TAB_STYLE,
            ]
        );
        $this->TabStyleEntryTitle ();
        $this->end_controls_section();
        # Title Section End
        #TabStyle Entry Meta Section
        $this->start_controls_section(
            'section_price_style',
            [
                'label' 	=> __( 'Entry Meta', 'movertheme-elementor' ),
                'tab' 		=> Controls_Manager::TAB_STYLE,
            ]
        );
        $this->TabStyleEntryContent();
        $this->end_controls_section();
        #End TabStyle Entry Meta Section


        #TabStyle Read More Section
        $this->start_controls_section('section_readmore_style', [ 'label' 	=> __( 'View More Button', 'movertheme-elementor' ), 'tab' 		=> Controls_Manager::TAB_STYLE,]);
        $this->_viewmore_btn_style();
        $this->end_controls_section();
        # End TabStyle Read More Section
    } # function _register_controls end
    protected function _viewmore_btn_style() {

        $this->start_controls_tabs( 'viewmore_btn_style' );

        $this->start_controls_tab(
            'viewmore_btn_normal',
            [
                'label' => __( 'Normal', 'movertheme-elementor' ),
            ]
        );

        $this->add_control(
            'cfpg_viewmore_btn_color',
            [
                'label' => __( 'Color', 'movertheme-elementor' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .view_more-btn-wrap a' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'cfpg_viewmore_btn_typography',
                'selector' => '{{WRAPPER}} .view_more-btn-wrap a',
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'cfpg_viewmore_btn_shadow',
                'label' => __( 'Box Shadow', 'movertheme-elementor' ),
                'selector' => '{{WRAPPER}} .view_more-btn-wrap a',
            ]
        );
        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'cfpg_viewmore_btn_border',
                'label' => __( 'Border', 'movertheme-elementor' ),
                'selector' => '{{WRAPPER}} .view_more-btn-wrap a',
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'cfpg_viewmore_btn_background',
                'label' => __( 'Background', 'movertheme-elementor' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .view_more-btn-wrap a',
            ]
        );
        $this->add_responsive_control(
            'cfpg_viewmore_btn_padding',
            [
                'label' => __('Padding', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} .view_more-btn-wrap a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'cfpg_viewmore_btn_margin',
            [
                'label' => __('Margin', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} .view_more-btn-wrap a' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_tab();
        # End Normal Style Tab
        $this->start_controls_tab(
            'viewmore_btn_hover',
            [
                'label' => __( 'Hover', 'movertheme-elementor' ),
            ]
        );

        $this->add_control(
            'cfpg_viewmore_btn_hover_color',
            [
                'label' => __( 'Color', 'movertheme-elementor' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .view_more-btn-wrap a:hover' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'cfpg_viewmore_btn_hover_shadow',
                'label' => __( 'Box Shadow', 'movertheme-elementor' ),
                'selector' => '{{WRAPPER}} .view_more-btn-wrap a:hover',
            ]
        );
        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'cfpg_viewmore_btn_hover_border',
                'label' => __( 'Border', 'movertheme-elementor' ),
                'selector' => '{{WRAPPER}} .view_more-btn-wrap a:hover',
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'cfpg_viewmore_btn_hover_background',
                'label' => __( 'Background', 'movertheme-elementor' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .view_more-btn-wrap a:hover',
            ]
        );
        $this->add_responsive_control(
            'cfpg_viewmore_btn_hover_padding',
            [
                'label' => __('Padding', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} .view_more-btn-wrap a:hover' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'cfpg_viewmore_btn_hover_margin',
            [
                'label' => __('Margin', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} .view_more-btn-wrap a:hover' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_tab();
        # End Hover Style Tab
        $this->end_controls_tabs();
        #End Tabs

        $this->end_controls_section();
    }
    protected function render( ) {

        $settings 			= $this->get_settings_for_display();
        $post_type 			= $settings['post_types'];
        $post_texanomy_slug	= $settings['post_texanomy_slug'];
        $post_number 		= $settings['post_number'];
        $post_order_by 		= $settings['post_order_by'];
        $target = $settings['read_more_button_url']['is_external'] ? ' target="_blank"' : '';
        $nofollow = $settings['read_more_button_url']['nofollow'] ? ' rel="nofollow"' : '';

        /*View all links*/
        $view_all_button_text	= $settings['view_all_button_text'];
        $show_link_manually 	= $settings['show_link_manually'];
        $view_all_link			= empty($show_link_manually) ? get_post_type_archive_link( $post_type  ) : $settings['read_more_button_url']['url'] ;
        $hide_view_all_button	= $settings['hide_view_all_button'];
        # Query Build.
        $arg = array(
            'post_type'   =>  $post_type,
            'post_status' => 'publish',
            'posts_per_page' => '6'
        );
        if( $post_order_by ){
            $arg['order'] = $post_order_by;
        }
        if( $post_number ){
            $arg['posts_per_page'] = $post_number;
        }

        $postblock = new \WP_Query( $arg );  ?>
        <article class="events-post-wrapper news-card moverthemethumlinepost-card solitude-bg__x">
            <h2 class="stage-title"><?php echo $settings['wrapper_title']; ?></h2>

            <ul class="list-unstyled lst_news_list" tabindex="0">
                <?php if ( $postblock->have_posts() ) : $i = 0;?>
                    <?php while ( $postblock->have_posts() ) : $postblock->the_post();
                        $permalink 	= get_permalink();
                        $title 		= get_the_title();
                        $media_url 	= wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), "movertheme-large" );
                        $media_url  = ( isset( $media_url[0] ) ) ? $media_url[0] : '';
                        $excerpt_limit = '125';

                        /*MetaBox Option*/
                        $prefix = "_movertheme_";

                        $event_date = get_post_meta( get_the_ID(), '_movertheme_event_date', true );


                        /*filtered term*/
                        $post_terms = get_the_terms( get_the_ID(), $post_texanomy_slug );

                        if( $post_terms && !is_wp_error( $post_terms ) ):

                            $portfolio_cat_slugs = array();

                            foreach ( $post_terms as $term ):
                                $portfolio_cat_slugs[] = $term->slug;
                            endforeach;

                            $portfolio_cat_slug = join( ' ', $portfolio_cat_slugs );

                        endif;
                        ?>
                        <li class="lst_news_item">
                            <article>
                                <h3 class="title mg_0" title="<?php the_title(); ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

                                <?php if (!empty($event_date)): ?>
                                    <div class="event-meta">
                                        <span class="date text"><?php echo $event_date; ?></span>
                                    </div>
                                <?php endif ?>
                            </article>
                        </li>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); wp_reset_query(); ?>
                <?php endif; ?>
            </ul>

            <?php if (!empty($view_all_button_text) && $hide_view_all_button != 'yes'): ?>
                <div class="view_more-btn-wrap">
                    <a class="btn btn-unsolemn btn-action read-more" href="<?php echo $view_all_link; ?>"><?php echo $settings['view_all_button_text']; ?></a>
                </div>
            <?php endif ?>
        </article>
    <?php }
	
}
