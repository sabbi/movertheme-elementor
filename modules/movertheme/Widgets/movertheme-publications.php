<?php
namespace MoverthemeElementor\Modules\Movertheme\Widgets;

// You can add to or remove from this list - it's not conclusive! Chop & change to fit your needs.
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Background;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Repeater;
use Elementor\Scheme_Typography;
use Elementor\Widget_Base;
use WP_Query;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Movertheme_Publications extends Widget_Base {

	/* Uncomment the line below if you do not wish to use the function _content_template() - leave that section empty if this is uncommented! */
	//protected $_has_template_content = false;
	
	public function get_name() {
		return 'movertheme-publications';
	}

	public function get_title() {
		return __( 'Movertheme Publications', 'movertheme-elementor' );
	}

	public function get_icon() {
		return 'eicon-price-list';
	}

	public function get_categories() {
		return [ 'elementor-movertheme-widgets'];
	}

	//protected static $css_scheme = '';
    protected function ControlsBoxElement () {
        $css_scheme_box_wrapper = apply_filters(
            'movertheme-elementor/movertheme-publication/css-scheme/box_wrapper',
            array(
                'box_wrapper'         => ' .paper_cut ',
                'box_wrapper_list_icon'         => '{{WRAPPER}} .paper_cut .pub-item.with-icon .elem-wrapper i',
            )
        );
        $this->start_controls_section(
            'movertheme_publication_box',
            [
                'label' 	=> __( 'Box Setting', 'movertheme-elementor' )
            ]
        );
        $this->add_control(
            'list_icon',
            [
                'label' => __( 'Select Icon', 'movertheme-elementor' ),
                'type' => Controls_Manager::ICON,
                'default' => 'ion-ios-paper-outline',
            ]
        );

        $this->add_control(
            'publication_icon_size',
            [
                'label' => __( 'Icon Size', 'movertheme-elementor' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%' ],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 1000,
                        'step' => 5,
                    ],
                    '%' => [
                        'min' => 0,
                        'max' => 100,
                    ],
                ],
                'default' => [
                    'unit' => 'px',
                    'size' => 30,
                ],
                'selectors' => [
                    $css_scheme_box_wrapper['box_wrapper_list_icon'] => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );
        $this->add_control(
            'publication_icon_box_size',
            [
                'label' => __( 'Icon Box Size', 'movertheme-elementor' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%' ],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 1000,
                        'step' => 5,
                    ],
                    '%' => [
                        'min' => 0,
                        'max' => 100,
                    ],
                ],
                'default' => [
                    'unit' => 'px',
                    'size' => 55,
                ],
                'selectors' => [
                    $css_scheme_box_wrapper['box_wrapper_list_icon'] => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};line-height:{{SIZE}}{{UNIT}}',
                ],
            ]
        );

        $this->add_control(
            'text_align',
            [
                'label' => __( 'Alignment', 'movertheme-elementor' ),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __( 'Left', 'movertheme-elementor' ),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __( 'Center', 'movertheme-elementor' ),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __( 'Right', 'movertheme-elementor' ),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'default' => 'left',
                'toggle' => true,
                'selectors' => [
                    '{{WRAPPER}}' . $css_scheme_box_wrapper['box_wrapper'] => 'text-align: {{VALUE}};',
                ],
                'separator' => 'before',
            ]
        );

	    $this->add_control(
		    'show_view_all_button',
		    [
			    'label' => __( 'Show Archive Button', 'movertheme-elementor' ),
			    'type' => Controls_Manager::SWITCHER,
			    'label_on' => __( 'Show', 'movertheme-elementor' ),
			    'label_off' => __( 'Hide', 'movertheme-elementor' ),
			    'return_value' => 'yes',
			    'default' => 'yes',
			    'separator' => 'before',
		    ]
	    );
        $this->end_controls_section();
    }
    protected function ControlsPostElement () {
        $css_scheme = apply_filters(
            'movertheme-elementor/movertheme-publication/css-scheme',
            array(

            )
        );
        $this->add_control(
            'post_number',
            [
                'label'         => __( 'Number of Publications', 'movertheme-elementor' ),
                'type'          => Controls_Manager::NUMBER,
                'default'       => __( '6', 'movertheme-elementor' ),

            ]
        );
        $this->add_control(
            'post_order_by',
            [
                'label'     => __( 'Order', 'movertheme-elementor' ),
                'type'      => Controls_Manager::SELECT,
                'default'   => 'DESC',
                'options'   => [
                    'DESC' 		=> __( 'Descending', 'movertheme-elementor' ),
                    'ASC' 		=> __( 'Ascending', 'movertheme-elementor' ),
                ],
            ]
        );
        $this->add_control(
            'show_selected_publication',
            [
                'label' => __( 'Selected Publication', 'movertheme-elementor' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __( 'Show', 'movertheme-elementor' ),
                'label_off' => __( 'Hide', 'movertheme-elementor' ),
                'return_value' => 'yes',
                'default' => 'yes',
                'separator' => 'before',
            ]
        );
    }



    /*Style Elements Title*/
    protected function _publication_box_style() {
	    $css_scheme_publication_box = apply_filters(
            'movertheme-elementor/movertheme-publication/css-scheme/publication_box',
            array(
                'wrapper_box'         => ' .paper_cut ',
            )
        );
        $this->start_controls_section(
            'publication_box_style',
            [ 'label' 	=> __( 'Publication Box Style', 'movertheme-elementor' ), 'tab' 		=> Controls_Manager::TAB_STYLE,]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'elementor_movertheme_publication_box_background',
                'label' => __( 'Background', 'movertheme-elementor' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} '.$css_scheme_publication_box['wrapper_box'] . '.paper_cut-flow-canvas',
            ]
        );

        $this->add_control(
            'elementor_movertheme_publication_box_color',
            [
                'label' => __( 'Color', 'movertheme-elementor' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} '.$css_scheme_publication_box['wrapper_box'] => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'elementor_movertheme_publication_box_shadow',
                'label' => __( 'Box Shadow', 'movertheme-elementor' ),
                'selector' => '{{WRAPPER}} '.$css_scheme_publication_box['wrapper_box'],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'elementor_movertheme_publication_box_border',
                'label' => __( 'Border', 'movertheme-elementor' ),
                'selector' => '{{WRAPPER}} '.$css_scheme_publication_box['wrapper_box'],
            ]
        );

        $this->add_responsive_control(
            'elementor_movertheme_publication_box_padding',
            [
                'label' => __('Padding', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} '.$css_scheme_publication_box['wrapper_box'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'separator' => 'before',
            ]
        );

        $this->add_responsive_control(
            'elementor_movertheme_publication_box_margin',
            [
                'label' => __('Margin', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} '.$css_scheme_publication_box['wrapper_box'] => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );
        $this->end_controls_section();
    }

    /*Style Elements Title*/
    protected function _publication_title_style() {
        $css_scheme_publication_title = apply_filters(
            'movertheme-elementor/movertheme-publication/css-scheme/publication_title',
            array(
                'title'         => ' .paper_cut .pub-item .content-wrapper >.title',
            )
        );
        $this->start_controls_section(
            'publication_title_style',
            [ 'label' 	=> __( 'Publication Title', 'movertheme-elementor' ), 'tab' 		=> Controls_Manager::TAB_STYLE,]
        );
        $this->add_control(
            'elementor_movertheme_title_color',
            [
                'label' => __( 'Color', 'movertheme-elementor' ),
                'type' => Controls_Manager::COLOR,

                'scheme'	=> [
	                'type'	=> Scheme_Color::get_type(),
	                'value' => Scheme_Color::COLOR_3,
                ],

                'selectors' => [
                    '{{WRAPPER}} '.$css_scheme_publication_title['title'] => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'elementor_movertheme_title_typography',
                'selector' => '{{WRAPPER}} '.$css_scheme_publication_title['title'],
            ]
        );
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'name' => 'text_shadow',
                'label' => __( 'Text Shadow', 'movertheme-elementor' ),
                'selector' => '{{WRAPPER}} '.$css_scheme_publication_title['title'],
            ]
        );
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'elementor_movertheme_title_shadow',
                'label' => __( 'Box Shadow', 'movertheme-elementor' ),
                'selector' => '{{WRAPPER}} '.$css_scheme_publication_title['title'],
            ]
        );
        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'elementor_movertheme_title_border',
                'label' => __( 'Border', 'movertheme-elementor' ),
                'selector' => '{{WRAPPER}} '.$css_scheme_publication_title['title'],
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'elementor_movertheme_title_background',
                'label' => __( 'Background', 'movertheme-elementor' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} '.$css_scheme_publication_title['title'],
            ]
        );

        $this->add_responsive_control(
            'elementor_movertheme_title_padding',
            [
                'label' => __('Padding', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} '.$css_scheme_publication_title['title'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'separator' => 'before',
            ]
        );

        $this->add_responsive_control(
            'elementor_movertheme_title_margin',
            [
                'label' => __('Margin', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} '.$css_scheme_publication_title['title'] => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );
        $this->end_controls_section();
    }

    /*Style Elements Author*/
    protected function _publication_authors_style() {
        $css_scheme_publication_authors = apply_filters(
            'movertheme-elementor/movertheme-publication/css-scheme/publication_authors',
            array(
                'authors'         => ' .paper_cut .pub-item .content-wrapper .slc_des .authr',
            )
        );
        $this->start_controls_section(
            'style_publication_author',
            [ 'label' 	=> __( 'Publication Author', 'movertheme-elementor' ), 'tab' 		=> Controls_Manager::TAB_STYLE,]
        );
        $this->add_control(
            'auth_style_publication_author_color',
            [
                'label' => __( 'Color', 'movertheme-elementor' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} '.$css_scheme_publication_authors['authors'] => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'auth_style_publication_author_typography',
                'selector' => '{{WRAPPER}} '.$css_scheme_publication_authors['authors'],
            ]
        );
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'name' => 'auth_style_publication_author_text_shadow',
                'label' => __( 'Text Shadow', 'movertheme-elementor' ),
                'selector' => '{{WRAPPER}} '.$css_scheme_publication_authors['authors'],
            ]
        );
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'auth_style_publication_author_shadow',
                'label' => __( 'Box Shadow', 'movertheme-elementor' ),
                'selector' => '{{WRAPPER}} '.$css_scheme_publication_authors['authors'],
            ]
        );
        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'auth_style_publication_author_border',
                'label' => __( 'Border', 'movertheme-elementor' ),
                'selector' => '{{WRAPPER}} '.$css_scheme_publication_authors['authors'],
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'auth_style_publication_author_background',
                'label' => __( 'Background', 'movertheme-elementor' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} '.$css_scheme_publication_authors['authors'],
            ]
        );

        $this->add_responsive_control(
            'auth_style_publication_author_padding',
            [
                'label' => __('Padding', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} '.$css_scheme_publication_authors['authors'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'separator' => 'before',
            ]
        );

        $this->add_responsive_control(
            'auth_style_publication_author_margin',
            [
                'label' => __('Margin', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} '.$css_scheme_publication_authors['authors'] => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );
        $this->end_controls_section();
    }

    /*Button Elements & Style*/
	protected function controls_post_button_element () {
		$this->start_controls_section(
			'controls_post_button_element',
			[
				'label' => __( 'Archive Button Element', 'movertheme-elementor' ),

				'condition' => [
					'show_view_all_button' => 'yes',
				],
			]
		);
		$this->add_control(
			'view_all_button_text',
			[
				'label'         => __( ' Button Text', 'movertheme-elementor' ),
				'type'          => Controls_Manager::TEXT,
				'label_block'   => false,
				'default'       => __( 'View All', 'movertheme-elementor' ),
				'separator'     => 'before',
				'condition' => [
					'show_view_all_button' => 'yes',
				],
			]
		);

		$this->add_control(
			'read_more_button_url',
			[
				'label' => __( 'Button Url', 'movertheme-elementor' ),
				'type' => Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'movertheme-elementor' ),
				'show_external' => true,
				'label_block'   => false,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
				'condition' => [
					'show_view_all_button' => 'yes',
				],
			]
		);

		$this->add_responsive_control(
			'view_all_button_align',
			[
				'label' 	=> __( 'Alignment', 'movertheme-elementor' ),
				'type' 		=> Controls_Manager::CHOOSE,
				'options' 	=> [
					'left' 		=> [
						'title' => __( 'Left', 'movertheme-elementor' ),
						'icon' 	=> 'fa fa-align-left',
					],
					'center' 	=> [
						'title' => __( 'Center', 'movertheme-elementor' ),
						'icon' 	=> 'fa fa-align-center',
					],
					'right' 	=> [
						'title' => __( 'Right', 'movertheme-elementor' ),
						'icon' 	=> 'fa fa-align-right',
					],
				],
				'selectors' => [
					'{{WRAPPER}} .auth-callto-btn-wraper' => 'text-align: {{VALUE}} ;',
				],
				'default' => 'left',
				'condition' => [
					'show_view_all_button' => 'yes',
				],
			]
		);
		$this->add_responsive_control(
			'elementor_movertheme_social_padding',
			[
				'label' => __('Spacing', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					'{{WRAPPER}} .auth-callto-btn-wraper' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);
		$this->end_controls_section();
	}

	protected function _archive_button_style() {
		$css_scheme_publication_archive_button = apply_filters(
			'movertheme-elementor/movertheme-publication/css-scheme/publication_archive_button',
			array(
				'archive_button'         => '{{WRAPPER}} .paper_cut .auth-callto-btn-wraper a',
				'archive_button_hover'         => '{{WRAPPER}} .paper_cut .auth-callto-btn-wraper a:hover',
			)
		);
		$this->start_controls_section(
			'publication_archive_button_style',

			[
				'label' 	=> __( 'Archive Button Element', 'movertheme-elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_view_all_button' => 'yes',
				],
			]
		);
		$this->start_controls_tabs( 'archive_button_style' );

		$this->start_controls_tab(
			'publication_archive_button_normal',
			[
				'label' => __( 'Normal', 'movertheme-elementor' ),
			]
		);

		$this->add_control(
			'publication_archive_button_color',
			[
				'label' => __( 'Color', 'movertheme-elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					$css_scheme_publication_archive_button['archive_button'] => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'publication_archive_button_typography',
				'selector' => $css_scheme_publication_archive_button['archive_button'],
			]
		);
		$this->add_group_control(
			Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'publication_archive_button_text_shadow',
				'label' => __( 'Text Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_publication_archive_button['archive_button'],
			]
		);
		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'publication_archive_button_shadow',
				'label' => __( 'Box Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_publication_archive_button['archive_button'],
			]
		);
		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'publication_archive_button_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => $css_scheme_publication_archive_button['archive_button'],
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'publication_archive_button_background',
				'label' => __( 'Background', 'movertheme-elementor' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => $css_scheme_publication_archive_button['archive_button'],
			]
		);

		$this->end_controls_tab();
		# End Normal Style Tab
		$this->start_controls_tab(
			'publication_archive_button_hover',
			[
				'label' => __( 'Hover', 'movertheme-elementor' ),
			]
		);

		$this->add_control(
			'publication_archive_button_hover_color',
			[
				'label' => __( 'Color', 'movertheme-elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					$css_scheme_publication_archive_button['archive_button_hover'] => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'publication_archive_button_text_shadow_hover',
				'label' => __( 'Text Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_publication_archive_button['archive_button_hover'],
			]
		);
		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'publication_archive_button_hover_shadow',
				'label' => __( 'Box Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_publication_archive_button['archive_button_hover'],
			]
		);
		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'publication_archive_button_hover_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => $css_scheme_publication_archive_button['archive_button_hover'],
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'publication_archive_button_hover_background',
				'label' => __( 'Background', 'movertheme-elementor' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => $css_scheme_publication_archive_button['archive_button_hover'],
			]
		);
		$this->end_controls_tab();
		# End Hover Style Tab
		$this->end_controls_tabs();
		#End Tabs


		$this->add_responsive_control(
			'publication_archive_button_padding',
			[
				'label' => __('Padding', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					$css_scheme_publication_archive_button['archive_button'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_responsive_control(
			'publication_archive_button_margin',
			[
				'label' => __('Margin', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					$css_scheme_publication_archive_button['archive_button'] => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();
	}

    /*Button like Post link*/
    protected function _website_links_style() {
        $css_scheme_publication_viewmore = apply_filters(
            'movertheme-elementor/movertheme-publication/css-scheme/website_links',
            array(
                'website_links'         => ' .paper_cut .pub-item .content-wrapper .description .link-with-icon',
                'website_links_hover'         => ' .paper_cut .pub-item .content-wrapper .description .link-with-icon:hover',
            )
        );
        $this->start_controls_section(
            'publication_readmore_style',
            [ 'label' 	=> __( 'Publisher\'s website Links', 'movertheme-elementor' ), 'tab' 		=> Controls_Manager::TAB_STYLE,]
        );
        $this->start_controls_tabs( 'website_links_style' );

        $this->start_controls_tab(
            'publication_readmore_btn_normal',
            [
                'label' => __( 'Normal', 'movertheme-elementor' ),
            ]
        );

        $this->add_control(
            'publication_readmore_btn_color',
            [
                'label' => __( 'Color', 'movertheme-elementor' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} '.$css_scheme_publication_viewmore['website_links'] => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'publication_readmore_btn_typography',
                'selector' => '{{WRAPPER}} '.$css_scheme_publication_viewmore['website_links'],
            ]
        );
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'name' => 'publication_readmore_btn_text_shadow',
                'label' => __( 'Text Shadow', 'movertheme-elementor' ),
                'selector' => '{{WRAPPER}} '.$css_scheme_publication_viewmore['website_links'],
            ]
        );
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'publication_readmore_btn_shadow',
                'label' => __( 'Box Shadow', 'movertheme-elementor' ),
                'selector' => '{{WRAPPER}} '.$css_scheme_publication_viewmore['website_links'],
            ]
        );
        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'publication_readmore_btn_border',
                'label' => __( 'Border', 'movertheme-elementor' ),
                'selector' => '{{WRAPPER}} '.$css_scheme_publication_viewmore['website_links'],
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'publication_readmore_btn_background',
                'label' => __( 'Background', 'movertheme-elementor' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} '.$css_scheme_publication_viewmore['website_links'],
            ]
        );

        $this->end_controls_tab();
        # End Normal Style Tab
        $this->start_controls_tab(
            'publication_readmore_btn_hover',
            [
                'label' => __( 'Hover', 'movertheme-elementor' ),
            ]
        );

        $this->add_control(
            'publication_readmore_btn_hover_color',
            [
                'label' => __( 'Color', 'movertheme-elementor' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} '.$css_scheme_publication_viewmore['website_links_hover'] => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'name' => 'publication_readmore_btn_text_shadow_hover',
                'label' => __( 'Text Shadow', 'movertheme-elementor' ),
                'selector' => '{{WRAPPER}} '.$css_scheme_publication_viewmore['website_links_hover'],
            ]
        );
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'publication_readmore_btn_hover_shadow',
                'label' => __( 'Box Shadow', 'movertheme-elementor' ),
                'selector' => '{{WRAPPER}} '.$css_scheme_publication_viewmore['website_links_hover'],
            ]
        );
        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'publication_readmore_btn_hover_border',
                'label' => __( 'Border', 'movertheme-elementor' ),
                'selector' => '{{WRAPPER}} '.$css_scheme_publication_viewmore['website_links_hover'],
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'publication_readmore_btn_hover_background',
                'label' => __( 'Background', 'movertheme-elementor' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} '.$css_scheme_publication_viewmore['website_links_hover'],
            ]
        );
        $this->end_controls_tab();
        # End Hover Style Tab
        $this->end_controls_tabs();
        #End Tabs


        $this->add_responsive_control(
            'publication_readmore_btn_padding',
            [
                'label' => __('Padding', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} '.$css_scheme_publication_viewmore['website_links'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'separator' => 'before',
            ]
        );

        $this->add_responsive_control(
            'publication_readmore_btn_margin',
            [
                'label' => __('Margin', 'movertheme-elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'default' => [],
                'selectors' => [
                    '{{WRAPPER}} '.$css_scheme_publication_viewmore['website_links'] => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();
    }


	
	protected function _register_controls() {

        $this->ControlsBoxElement ();

        $this->start_controls_section(
            'movertheme_post_grid',
            [
                'label' 	=> __( 'Post Element', 'movertheme-elementor' )
            ]
        );
        $this->ControlsPostElement ();
        $this->end_controls_section();
        #End Posts element

        #Button Element
		$this->controls_post_button_element();


        #Style Elements
        $this->_publication_box_style();

        $this->_publication_title_style();

        $this->_publication_authors_style();

        $this->_website_links_style();
		$this->_archive_button_style();
	}


	protected function render() {
		$settings = $this->get_settings_for_display();


        $my_acf_checkbox_field_arr = '_movertheme_selected_publications';


		/*View all links*/

        // Build the meta query based on the selected options
        $meta_query = array();
        if ($settings['show_selected_publication']) {
            $meta_query[] = array(
                'key'     => $my_acf_checkbox_field_arr,
            );
        }

        $post_number = '-1';
        if ($settings['post_number']) {
            $post_number = $settings['post_number'];
        }

        // args
        $args = array(
            'posts_per_page'	=> $post_number,
            'post_type'		=> 'journal_article',
            'meta_query' => $meta_query,
            'order' => $settings['post_order_by'],
        );
        // query
        $the_query = new WP_Query( $args );

        if( $the_query->have_posts() ): ?>
            <div class="paper_cut">
                <div class="paper_cut-flow-canvas"></div>
                <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <div class="pub-item with-icon">
                        <div class="elem-wrapper">
                            <i class="<?php echo esc_attr($settings['list_icon']); ?>"></i>
                        </div>
                        <div class="content-wrapper">
                            <h3 class="title mb_0"><?php echo the_title(); ?></h3>
                            <div class="slc_des">
                                <div class="authr">
                                    <?php
                                        $whois_authr = get_post_meta( get_the_ID(), '_movertheme_journal_article_authors_name', true );
                                        echo wp_kses_post($whois_authr);
                                    ?>
                                </div>
                            </div>
                            <div class="description">
                                <?php
                                    $selected_publication_button_text = get_post_meta( get_the_ID(), '_movertheme_journal_article_selecte_publication_btn_text', true );
                                    $selected_publication_button_url = get_post_meta( get_the_ID(), '_movertheme_journal_article_selecte_publication_btn_link', true );
                                ?>
                                <?php if (!empty($selected_publication_button_text) || !empty($selected_publication_button_url)): ?>
                                    <a class="link-with-icon" href="<?php echo esc_attr($selected_publication_button_url); ?>" target="black">
                                        <i class="ion-ios-browsers-outline"></i>
                                        <?php echo wp_kses_post($selected_publication_button_text); ?>
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>

	            <?php if ($settings['show_view_all_button'] == 'yes'):  ?>
                    <div class="auth-callto-btn-wraper position-relative">
                        <a href="<?php echo $settings['read_more_button_url']['url']; ?>" <?php echo $target . $nofollow; ?>>
                            <div <?php echo $this->get_render_attribute_string( 'view_all_button_text' ); ?>> <?php echo $settings['view_all_button_text'];?></div>
                        </a>
                    </div>
	            <?php endif; ?>
            </div><!-- /.paper_cut -->
        <?php endif;  wp_reset_query();
	}
}
