<?php
namespace MoverthemeElementor\Modules\Movertheme\Widgets;

// You can add to or remove from this list - it's not conclusive! Chop & change to fit your needs.
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Background;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Repeater;
use Elementor\Scheme_Typography;
use Elementor\Widget_Base;
use MoverthemeElementor\Classes\Utils;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Movertheme_Info_Card extends Widget_Base {

	/* Uncomment the line below if you do not wish to use the function _content_template() - leave that section empty if this is uncommented! */
	//protected $_has_template_content = false; 
	
	public function get_name() {
		return 'movertheme-info-card';
	}

	public function get_title() {
		return __( 'Movertheme Info Cards', 'movertheme-elementor' );
	}

	public function get_icon() {
		return 'eicon-image-box';
	}

	public function get_categories() {
		return [ 'elementor-movertheme-widgets'];
	}

	protected function _info_card_view_more_button_element () {
		$this->start_controls_section(
			'card_info_button_element',
			[
				'label' => __( 'View More Button Element', 'movertheme-elementor' ),

				'condition' => [
					'info_card_hide_v_a_button!' => 'yes',
				],
			]
		);

		$this->add_control(
			'card_info_button_text',
			[
				'label'         => __( 'Button Text:', 'movertheme-elementor' ),
				'type'          => Controls_Manager::TEXT,
				'label_block'   => false,
				'default'       => __( 'read more', 'movertheme-elementor' ),
				'separator'     => 'before',
			]
		);

		$this->add_control(
			'card_info_button_url',
			[
				'label' => __( 'Button Url:', 'movertheme-elementor' ),
				'type' => Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'movertheme-elementor' ),
				'show_external' => false,
				'label_block'   => false,
				'default' => [
					'url' => '',
					'is_external' => false,
					'nofollow' => false,
				],
			]
		);

		$this->add_responsive_control(
			'card_info_button_align',
			[
				'label' 	=> __( 'Alignment:', 'movertheme-elementor' ),
				'type' 		=> Controls_Manager::CHOOSE,
				'options' 	=> [
					'left'    => [
						'title' => __( 'Left', 'elementor' ),
						'icon' => 'eicon-h-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'elementor' ),
						'icon' => 'eicon-h-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'elementor' ),
						'icon' => 'eicon-h-align-right',
					],
				],
				'selectors' => [
					'{{WRAPPER}} .read-more-wrap ' => 'text-align: {{VALUE}} ;',
				],
				'default' => 'left',
			]
		);

		$this->end_controls_section();
	}

	/*View More Button Style*/
	protected function _card_info_button_style() {
		$css_scheme_card_info_button = apply_filters(
			'movertheme-elementor/movertheme-card_info_button/css-scheme/card_info_button',
			array(
				'card_info_button'         => ' {{WRAPPER}} .moverthemethumlinepost-card-wrap .read-more-wrap .read-more',
				'card_info_button_hover'         => ' {{WRAPPER}} .moverthemethumlinepost-card-wrap .read-more-wrap .read-more:hover',
			)
		);
		$this->start_controls_section(

			'card_info_button_style_main',
			[
				'label' 	=> __( 'Read More', 'movertheme-elementor' ),
				'tab' 		=> Controls_Manager::TAB_STYLE,
				'condition' => [
					'info_card_hide_v_a_button!' => 'yes',
				],
			]
		);

		$this->start_controls_tabs( 'card_info_button_tab_style' );

		$this->start_controls_tab(
			'card_info_button_normal',
			[
				'label' => __( 'Normal', 'movertheme-elementor' ),
			]
		);

		$this->add_control(
			'card_info_button_color',
			[
				'label' => __( 'Color', 'movertheme-elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					$css_scheme_card_info_button['card_info_button'] => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'card_info_button_typography',
				'selector' => $css_scheme_card_info_button['card_info_button'],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'card_info_button_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => $css_scheme_card_info_button['card_info_button'],
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'card_info_button_background',
				'label' => __( 'Background', 'movertheme-elementor' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => $css_scheme_card_info_button['card_info_button'],
			]
		);
		$this->add_group_control(
			Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'card_info_button_text_shadow',
				'label' => __( 'Text Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_card_info_button['card_info_button'],
			]
		);
		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'card_info_button_shadow',
				'label' => __( 'Box Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_card_info_button['card_info_button'],
			]
		);
		$this->end_controls_tab();
		# End Normal Style Tab
		$this->start_controls_tab(
			'card_info_button_hover',
			[
				'label' => __( 'Hover', 'movertheme-elementor' ),
			]
		);

		$this->add_control(
			'card_info_button_hover_color',
			[
				'label' => __( 'Color', 'movertheme-elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					$css_scheme_card_info_button['card_info_button_hover'] => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'card_info_button_text_shadow_hover',
				'label' => __( 'Text Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_card_info_button['card_info_button_hover'],
			]
		);
		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'card_info_button_hover_shadow',
				'label' => __( 'Box Shadow', 'movertheme-elementor' ),
				'selector' => $css_scheme_card_info_button['card_info_button_hover'],
			]
		);
		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'card_info_button_hover_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => $css_scheme_card_info_button['card_info_button_hover'],
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'card_info_button_hover_background',
				'label' => __( 'Background', 'movertheme-elementor' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => $css_scheme_card_info_button['card_info_button_hover'],
			]
		);
		$this->end_controls_tab();
		# End Hover Style Tab
		$this->end_controls_tabs();
		#End Tabs


		$this->add_responsive_control(
			'card_info_button_padding',
			[
				'label' => __('Padding', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					$css_scheme_card_info_button['card_info_button'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_responsive_control(
			'card_info_button_margin',
			[
				'label' => __('Margin', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					$css_scheme_card_info_button['card_info_button'] => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();
	}


	/*Style Elements Info Cards*/
	protected function _info_card_title_style() {
		$css_scheme_info_card_title = apply_filters(
			'movertheme-elementor/movertheme-info_card/css-scheme/info_card_title',
			array(
				'info_card_style'         => ' .moverthemethumlinepost-card-wrap .moverthemethumlinepost-card-meta .meta-title ',
			)
		);
		$this->start_controls_section(
			'info_card_title_style',
			[
				'label' 	=> __( 'Title', 'movertheme-elementor' ),
				'tab' 		=> Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'elementor_movertheme_info_card_title_color',
			[
				'label' => __( 'Color', 'movertheme-elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} '.$css_scheme_info_card_title['info_card_style'] => 'color: {{VALUE}};',
					'{{WRAPPER}} '.$css_scheme_info_card_title['info_card_style'].' a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'elementor_movertheme_info_card_title_typography',
				'selector' => '{{WRAPPER}} '.$css_scheme_info_card_title['info_card_style'],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'elementor_movertheme_info_card_title_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => '{{WRAPPER}} '.$css_scheme_info_card_title['info_card_style'],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'elementor_movertheme_info_card_title_shadow',
				'label' => __( 'Box Shadow', 'movertheme-elementor' ),
				'selector' => '{{WRAPPER}} '.$css_scheme_info_card_title['info_card_style'],
			]
		);


		$this->add_responsive_control(
			'elementor_movertheme_info_card_title_padding',
			[
				'label' => __('Spacing', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					'{{WRAPPER}} '.$css_scheme_info_card_title['info_card_style'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);
		$this->end_controls_section();
	}


	/*Style Elements Info Cards Description*/
	protected function _info_card_meta_text_style() {
		$css_scheme_info_card_meta_text = apply_filters(
			'movertheme-elementor/movertheme-info_card/css-scheme/info_card_meta_text',
			array(
				'info_card_meta_style'         => ' .moverthemethumlinepost-card-wrap .moverthemethumlinepost-card-meta .meta-text ',
			)
		);
		$this->start_controls_section(
			'info_card_meta_text_style',
			[
				'label' 	=> __( 'Description', 'movertheme-elementor' ),
				'tab' 		=> Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'elementor_movertheme_info_card_meta_text_color',
			[
				'label' => __( 'Color', 'movertheme-elementor' ),
				'type' => Controls_Manager::COLOR,

				'scheme'	=> [
					'type'	=> Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_3,
				],

				'selectors' => [
					'{{WRAPPER}} '.$css_scheme_info_card_meta_text['info_card_meta_style'] => 'color: {{VALUE}};',
					'{{WRAPPER}} '.$css_scheme_info_card_meta_text['info_card_meta_style'].' a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'elementor_movertheme_info_card_meta_text_typography',
				'selector' => '{{WRAPPER}} '.$css_scheme_info_card_meta_text['info_card_meta_style'],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'elementor_movertheme_info_card_meta_text_border',
				'label' => __( 'Border', 'movertheme-elementor' ),
				'selector' => '{{WRAPPER}} '.$css_scheme_info_card_meta_text['info_card_meta_style'],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'elementor_movertheme_info_card_meta_text_shadow',
				'label' => __( 'Box Shadow', 'movertheme-elementor' ),
				'selector' => '{{WRAPPER}} '.$css_scheme_info_card_meta_text['info_card_meta_style'],
			]
		);


		$this->add_responsive_control(
			'elementor_movertheme_info_card_meta_text_padding',
			[
				'label' => __('Spacing', 'movertheme-elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [],
				'selectors' => [
					'{{WRAPPER}} '.$css_scheme_info_card_meta_text['info_card_meta_style'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);
		$this->end_controls_section();
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Card Content', 'movertheme-elementor' ),
			]
		);
		// Add your widget/element content controls here! Below is an example control

		$this->add_control(
			'movertheme_info_card_title',
			[
				'label' => __( 'Card Title', 'movertheme-elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'DNA: Definition, Structure and Discovery', 'movertheme-elementor' ),
				'separator' => 'after',
				'label_block' => true,
			]
		);

		$this->add_control(
			'movertheme_info_card_image',
			[
				'label' => __( 'Choose Card Image', 'movertheme-elementor' ),
				'type' => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],

			]
		);

		$this->add_control(
			'movertheme_info_card_description',
			[
				'label' => __( 'Description', 'movertheme-elementor' ),
				'type' => Controls_Manager::TEXTAREA,
				'rows' => 4,
				'default' => __( 'Default description', 'movertheme-elementor' ),
				'placeholder' => __( 'Type your description here', 'movertheme-elementor' ),
				'separator' => 'before',
			]
		);


		$this->add_responsive_control(
			'box-align',
			[
				'label' => __( 'Alignment', 'elementor' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' 		=> [
						'title' => __( 'Left', 'movertheme-elementor' ),
						'icon' 	=> 'fa fa-align-left',
					],
					'center' 	=> [
						'title' => __( 'Center', 'movertheme-elementor' ),
						'icon' 	=> 'fa fa-align-center',
					],
					'right' 	=> [
						'title' => __( 'Right', 'movertheme-elementor' ),
						'icon' 	=> 'fa fa-align-right',
					],
				],
				'default' => 'center',
				'selectors' => [
					'{{WRAPPER}} .moverthemethumlinepost-card-wrap,{{WRAPPER}} .read-more-wrap ' => 'text-align: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'info_card_hide_v_a_button',
			[
				'label' => __( 'Hide View More Button', 'movertheme-elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => __( 'Yes', 'movertheme-elementor' ),
				'label_off' => __( 'No', 'movertheme-elementor' ),
				'return_value' => 'yes',
				'default' => 'no',
				'separator' => 'before',
			]
		);


		$this->add_control(
			'view',
			[
				'label' => __( 'View', 'movertheme-elementor' ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);
		
		$this->end_controls_section();

		$this->_info_card_view_more_button_element();
		$this->_info_card_title_style();
		$this->_info_card_meta_text_style();
		$this->_card_info_button_style();
	}

		
	protected function render() {
		$settings = $this->get_settings();

		?>
            <div class="moverthemethumlinepost-card-wrap">
                <article class="moverthemethumlinepost-card solitude-bg__x">
                    <figure class="moverthemethumlinepost-card-figure">
                        <img src="<?php echo esc_url($settings['movertheme_info_card_image']['url']); ?>" alt="" class="img-responsive img-thumpost">
                    </figure>
                    <div class="moverthemethumlinepost-card-meta">
                        <h2 class="meta-title ht-5"><?php echo wp_kses_post($settings['movertheme_info_card_title']); ?></h2>
                        <div class="meta-text"><?php echo wp_kses_post($settings['movertheme_info_card_description']); ?></div>
                        <?php if ($settings['info_card_hide_v_a_button'] !== 'yes'): ?>

                            <div class="read-more-wrap">
                                <a href="<?php echo esc_url($settings['card_info_button_url']['url']); ?>" class="btn btn-unsolemn btn-action read-more"><?php echo wp_kses_post($settings['card_info_button_text']); ?></a>
                            </div>
                        <?php endif; ?>
                    </div>
                </article>
            </div>

		<?php
	}
}
