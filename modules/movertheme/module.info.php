<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

return [
	'title' => __( 'Movertheme', 'movertheme-elementor' ),
	'required' => true,
	'default_activation' => true,
];
