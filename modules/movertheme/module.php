<?php
namespace MoverthemeElementor\Modules\Movertheme;

use MoverthemeElementor\Base\Module_Base;
use Elementor;
class Module extends Module_Base {

	public function __construct() {
		parent::__construct();
		// This is here for extensibility purposes - go to town and make things happen!
	}
	
	public function get_name() {
		return 'movertheme-elementor';
	}

	public function get_widgets() {
		return [

			'movertheme_title',
			'movertheme_workingprocess',
			'movertheme_sb_slider',
            'movertheme_info_card',
            'movertheme_post_grid',
			'movertheme_events',
            //'movertheme_events_lists',
            //'movertheme_publications',
            'movertheme_teammember',
            'movertheme_common_question',
            'movertheme_common_questiontwo',
			'movertheme_testimonials',
			//'movertheme_conference_talks',
			//'movertheme_book',
			'movertheme_fluentformwidget',
			//'movertheme_journal_articles',
            //'movertheme_postsgrid'
		];
	}
}

