<?php
namespace ElementorControls\Controls;
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
class Elementor_Custom extends \Elementor\Base_Control {
    public function get_type() {
        return 'custom';
    }
    public function content_template() {
        echo '<input type="text" placeholder="custom" data-setting="post" />';
    }
}
