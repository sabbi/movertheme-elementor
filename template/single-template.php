<?php
/**
 * The template for displaying all single posts for elementor library template
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package movertheme
 */

get_header();
?>
<div class="single-elementor_library-section">
	<?php the_content(); ?>
</div>
<?php
get_footer();
