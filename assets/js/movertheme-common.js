
jQuery( window ).on('elementor/frontend/init', () => {
    class moveraccodi extends elementorModules.frontend.handlers.Base {
        getDefaultSettings() {
            return {
                selectors: {
                    moverthemeAccordionHolder: '.movertheme-accordion-wrap',
                    moverthemeAccordion: '.movertheme-accordion',
                    moverthemeAccordionItem: '.accordion-section',
                },
            };
        }

        getDefaultElements() {

            const selectors = this.getSettings( 'selectors' );

            const elements = {
                $moverthemeAccordion: this.$element.find( selectors.moverthemeAccordion ),
                $moverthemeAccordionSection: this.$element.find( selectors.moverthemeAccordionItem ),
            };

            /*return {
             $accordionHolder: this.$element.find( selectors.firstSelector ),
             $moverthemeAccordion: this.$element.find( selectors.moverthemeAccordion ),
             };*/

            return elements;
        }

        bindEvents() {
            this.elements.$moverthemeAccordion.on( 'click', this.onFirstSelectorClick.bind( this ) );
        }

        onFirstSelectorClick( event ) {
            event.preventDefault();
            //console.dir(elementor.getPanelView().getCurrentPageView());
            //console.dirxml(this.elements)
            //this.elements.$moverthemeAccordion.show();
            //console.log(this.getElementSettings());
        }

        onInit( ...args ) {
            super.onInit( ...args );

            if ( ! this.elements.$moverthemeAccordionSection.length  ) {
                return;
            }

            this.themeAccordion = this.elements.$moverthemeAccordionSection.accordion({
                transitionSpeed: 500,
                transitionEasing: 'cubic-bezier(.75,-0.5,0,1.75)',
                controlElement: '[data-control]',
                contentElement: '[data-content]',
                groupElement: '[data-accordion-group]',
                singleOpen: true
            });
            jQuery(this.themeAccordion[0]).find('.accordion-control-title').click();
        }
    }

    const addHandler = ( $scope ) => {
        elementorFrontend.elementsHandler.addHandler( moveraccodi, {
            $element: $scope,
            toggleSelf: false,
        } );
    };

    elementorFrontend.hooks.addAction( 'frontend/element_ready/movertheme-commonquestion.default', addHandler );
} );



/*movertestimoni*/
jQuery( window ).on('elementor/frontend/init', () => {
    class movertestimoni extends elementorModules.frontend.handlers.Base {
        getDefaultSettings() {
            return {
                selectors: {
                    carouselwrap: '.movertheme-testimonials-wrap',
                    carousel: '.movertheme-testimonials-carousel',
                    slideContent: '.movertheme-testimonials-item',
                },
            }
        }

        getDefaultElements() {

            const selectors = this.getSettings( 'selectors' );

            const elements = {
                $carousel:      this.$element.find( selectors.carousel ),
                $buttonPrev:    this.$element.find('.swiper-navigation .swiper-button-prev'),
                $buttonNext:    this.$element.find('.swiper-navigation .swiper-button-next'),
            };

            elements.$testimonialSlides = elements.$carousel.find( selectors.slideContent );

            return elements;
        }

        getSlidesCount() {
            return this.elements.$testimonialSlides.length;
        }

        getSwiperSettings() {
            var elementSettings = this.getElementSettings();
                elementSettings.slides_to_show = 2;

            var slidesToShow = +elementSettings.slides_to_show || 3,
                isSingleSlide = 1 === slidesToShow,
                defaultLGDevicesSlidesCount = isSingleSlide ? 1 : 2,
                elementorBreakpoints = elementorFrontend.config.breakpoints;


            // edit with feroas
            var slidesToShow_feroas = 1;
            const mThemeTestimonialOptions = {
                slidesPerView: slidesToShow,
                loop: 'yes' === elementSettings.infinite,
                speed: elementSettings.speed,
            };

            mThemeTestimonialOptions.breakpoints = {};

            mThemeTestimonialOptions.breakpoints[ elementorBreakpoints.md ] = {
                slidesPerView: +elementSettings.slides_to_show_mobile || 1,
                slidesPerGroup: +elementSettings.slides_to_scroll_mobile || 1,
            };

            mThemeTestimonialOptions.breakpoints[ elementorBreakpoints.lg ] = {
                slidesPerView: +elementSettings.slides_to_show_tablet || defaultLGDevicesSlidesCount,
                slidesPerGroup: +elementSettings.slides_to_scroll_tablet || 1,
            };

            if ( ! this.isEdit && 'yes' === elementSettings.autoplay ) {
                mThemeTestimonialOptions.autoplay = {
                    delay: elementSettings.autoplay_speed,
                    disableOnInteraction: !! elementSettings.pause_on_hover,
                };
            }

            if ( true === mThemeTestimonialOptions.loop ) {
                mThemeTestimonialOptions.loopedSlides = this.getSlidesCount();
            }

            if ( isSingleSlide ) {
                mThemeTestimonialOptions.effect = elementSettings.effect;

                if ( 'fade' === elementSettings.effect ) {
                    mThemeTestimonialOptions.fadeEffect = { crossFade: true };
                }
            } else {
                mThemeTestimonialOptions.slidesPerGroup = +elementSettings.slides_to_scroll || 1;
            }

            if ( elementSettings.image_spacing_custom ) {
                mThemeTestimonialOptions.spaceBetween = elementSettings.image_spacing_custom.size;
            }

            const showArrows = 'arrows' === elementSettings.navigation || 'both' === elementSettings.navigation,
                showDots = 'dots' === elementSettings.navigation || 'both' === elementSettings.navigation;
            if ( showArrows ) {
                mThemeTestimonialOptions.navigation = {
                    prevEl: '.testimonial--button-prev',
                    nextEl: '.testimonial--button-next',
                };
            }

            if ( showDots ) {
                mThemeTestimonialOptions.pagination = {
                    el: '.swiper-pagination',
                    type: 'bullets',
                    clickable: true,
                };
            }
            return mThemeTestimonialOptions;
        }

        updateSpaceBetween() {
            this.swiper.params.spaceBetween = this.getElementSettings( 'image_spacing_custom' ).size || 0;

            this.swiper.update();
        }

        onInit( ...args ) {
            super.onInit( ...args );

            if ( ! this.elements.$carousel.length || 2 > this.elements.$testimonialSlides.length ) {
                return;
            }

            //this.swiper = new Swiper( this.elements.$carousel, this.getSwiperSettings() );
            this.swiper = new Swiper( this.elements.$carousel, {
                slidesPerView: 2,
                spaceBetween: 30,
                loop: false,
                // init: false,
                /*pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },*/
                /*
                breakpoints: {
                    640: {
                        slidesPerView: 2,
                        spaceBetween: 20,
                    },
                    768: {
                        slidesPerView: 4,
                        spaceBetween: 40,
                    },
                    1024: {
                        slidesPerView: 1,
                        spaceBetween: 0,
                    },
                },*/

                navigation: {
                    nextEl: this.elements.$buttonNext,
                    prevEl: this.elements.$buttonPrev,
                },
                controller: {
                    inverse: true,
                    by: 'slide'
                },
                autoplay: {
                    delay: 5000,
                    disableOnInteraction: true
                },
            });
            this.swiper.autoplay.stop()
            //console.log(this.getSwiperSettings());

        }

        onElementChange( propertyName ) {

            //console.log('elements',propertyName);
            if ( 0 === propertyName.indexOf( 'image_spacing_custom' ) ) {
                this.updateSpaceBetween();
            } else if ( 'arrows_position' === propertyName ) {
                this.swiper.update();
            }
        }

        /*onEditSettingsChange( propertyName ) {
            if ( this.isEdit ) {
                //attributes = elementorFrontend.config.elements.editSettings[ this.getModelCID() ].attributes;
            }
            console.log(elementorFrontend.config.elements.editSettings[ this.getModelCID() ]);

            if ( 'activeItemIndex' === propertyName ) {
                this.swiper.slideTo( this.getEditSettings( 'activeItemIndex' ) - 1 );
            }
        }*/
    }

    const moverTestimoniHandler = ( $scope ) => {
        elementorFrontend.elementsHandler.addHandler( movertestimoni, {
            $element: $scope,
            toggleSelf: false,
        } );
    };

    elementorFrontend.hooks.addAction( 'frontend/element_ready/movertheme-testimonials.default', moverTestimoniHandler );
} );



/*moverSlider*/
jQuery( window ).on('elementor/frontend/init', () => {
    class moverSlider extends elementorModules.frontend.handlers.Base {
        getDefaultSettings() {
            return {
                selectors: {
                    carouselwrap: '.movertheme-sliders-wrap',
                    carousel: '.movertheme-sliders-container',
                    slideContent: '.movertheme-sliders-item',
                },
            }
        }

        getDefaultElements() {

            const selectors = this.getSettings( 'selectors' );

            const elements = {
                $carousel:      this.$element.find( selectors.carousel ),
                $buttonPrev:    this.$element.find('.swiper-navigation .swiper-button-prev'),
                $buttonNext:    this.$element.find('.swiper-navigation .swiper-button-next'),
            };


            elements.$site_sliderSlides = elements.$carousel.find( selectors.slideContent );
            return elements;
        }

        getSlidesCount() {
            return this.elements.$site_sliderSlides.length;
        }

        getSwiperSettings() {
            var elementSettings = this.getElementSettings();
            elementSettings.slides_to_show = 2;

            var slidesToShow = +elementSettings.slides_to_show || 3,
                isSingleSlide = 1 === slidesToShow,
                defaultLGDevicesSlidesCount = isSingleSlide ? 1 : 2,
                elementorBreakpoints = elementorFrontend.config.breakpoints;


            // edit with feroas
            var slidesToShow_feroas = 1;
            const mThemeTestimonialOptions = {
                slidesPerView: slidesToShow,
                loop: 'yes' === elementSettings.infinite,
                speed: elementSettings.speed,
            };

            mThemeTestimonialOptions.breakpoints = {};

            mThemeTestimonialOptions.breakpoints[ elementorBreakpoints.md ] = {
                slidesPerView: +elementSettings.slides_to_show_mobile || 1,
                slidesPerGroup: +elementSettings.slides_to_scroll_mobile || 1,
            };

            mThemeTestimonialOptions.breakpoints[ elementorBreakpoints.lg ] = {
                slidesPerView: +elementSettings.slides_to_show_tablet || defaultLGDevicesSlidesCount,
                slidesPerGroup: +elementSettings.slides_to_scroll_tablet || 1,
            };

            if ( ! this.isEdit && 'yes' === elementSettings.autoplay ) {
                mThemeTestimonialOptions.autoplay = {
                    delay: elementSettings.autoplay_speed,
                    disableOnInteraction: !! elementSettings.pause_on_hover,
                };
            }

            if ( true === mThemeTestimonialOptions.loop ) {
                mThemeTestimonialOptions.loopedSlides = this.getSlidesCount();
            }

            if ( isSingleSlide ) {
                mThemeTestimonialOptions.effect = elementSettings.effect;

                if ( 'fade' === elementSettings.effect ) {
                    mThemeTestimonialOptions.fadeEffect = { crossFade: true };
                }
            } else {
                mThemeTestimonialOptions.slidesPerGroup = +elementSettings.slides_to_scroll || 1;
            }

            if ( elementSettings.image_spacing_custom ) {
                mThemeTestimonialOptions.spaceBetween = elementSettings.image_spacing_custom.size;
            }

            const showArrows = 'arrows' === elementSettings.navigation || 'both' === elementSettings.navigation,
                showDots = 'dots' === elementSettings.navigation || 'both' === elementSettings.navigation;
            if ( showArrows ) {
                mThemeTestimonialOptions.navigation = {
                    prevEl: '.slider--button-prev',
                    nextEl: '.slider--button-next',
                };
            }

            if ( showDots ) {
                mThemeTestimonialOptions.pagination = {
                    el: '.swiper-pagination',
                    type: 'bullets',
                    clickable: true,
                };
            }
            return mThemeTestimonialOptions;
        }

        updateSpaceBetween() {
            this.swiper.params.spaceBetween = this.getElementSettings( 'image_spacing_custom' ).size || 0;

            this.swiper.update();
        }

        onInit( ...args ) {
            super.onInit( ...args );

            if ( ! this.elements.$carousel.length || 2 > this.elements.$site_sliderSlides.length ) {
                return;
            }

            //this.swiper = new Swiper( this.elements.$carousel, this.getSwiperSettings() );
            this.swiper = new Swiper( this.elements.$carousel, {
                slidesPerView: 1,
                spaceBetween: 30,
                loop: true,

                parallax: true,
                effect: "fade",

                fadeEffect: {
                    crossFade: false
                },
                // init: false,
                /*pagination: {
                 el: '.swiper-pagination',
                 clickable: true,
                 },*/
                /*
                 breakpoints: {
                 640: {
                 slidesPerView: 2,
                 spaceBetween: 20,
                 },
                 768: {
                 slidesPerView: 4,
                 spaceBetween: 40,
                 },
                 1024: {
                 slidesPerView: 1,
                 spaceBetween: 0,
                 },
                 },*/

                navigation: {
                    nextEl: this.elements.$buttonNext,
                    prevEl: this.elements.$buttonPrev,
                },
                controller: {
                    inverse: true,
                    by: 'slide'
                },
                autoplay: {
                    delay: 2200,
                    disableOnInteraction: true
                },
            });
            //this.swiper.autoplay.stop();
            //console.log(this.getSwiperSettings());

        }

        onElementChange( propertyName ) {

            //console.log('elements ee',propertyName);
            if ( 0 === propertyName.indexOf( 'image_spacing_custom' ) ) {
                this.updateSpaceBetween();
            } else if ( 'arrows_position' === propertyName ) {
                this.swiper.update();
            }
        }

        /*onEditSettingsChange( propertyName ) {
         if ( this.isEdit ) {
         //attributes = elementorFrontend.config.elements.editSettings[ this.getModelCID() ].attributes;
         }
         console.log(elementorFrontend.config.elements.editSettings[ this.getModelCID() ]);

         if ( 'activeItemIndex' === propertyName ) {
         this.swiper.slideTo( this.getEditSettings( 'activeItemIndex' ) - 1 );
         }
         }*/
    }

    const moverSiteSliderHandler = ( $scope ) => {
        elementorFrontend.elementsHandler.addHandler( moverSlider, {
            $element: $scope,
            toggleSelf: false,
        } );
    };

    elementorFrontend.hooks.addAction( 'frontend/element_ready/movertheme-slider.default', moverSiteSliderHandler );
} );
