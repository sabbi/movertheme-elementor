const mix = require('laravel-mix');

//mix.sass('src/scss/frontend.scss', 'src/assets/dist');
mix.sass('src/scss/frontend.scss', 'assets/css').options({
    processCssUrls: false,
    autoprefixer: {
        options: {
            browsers: [
                'last 6 versions',
            ]
        }
    },
    clearConsole: false,
    cssNano: {
        discardComments: {
            removeAll: false
        },
    },
    outputStyle: 'nested',
    /*publicPath: 'dist/',
    // set uglify to false in order to prevent production minification
    // it prevents mix from pushing UglifyJSPlugin into the webpack config
    uglify: false*/
}).minify('assets/css/frontend.css').sourceMaps();

   //.setPublicPath('dist');

/*
let mix = require('laravel-mix');
let minifier = require('minifier');

mix.sass('src/sass/app.scss', 'public/css/', {
    outputStyle: 'nested'
});

mix.then(() => {
    minifier.minify('public/css/app.css')
});*/
mix.sass('src/scss/common/movertheme-elementor.scss', 'assets/css/common');
