<?php
namespace MoverthemeElementor;

use Elementor\Utils;
use Elementor\Controls_Manager;
use MoverthemeElementor\Classes;

if ( ! defined( 'ABSPATH' ) ) {	exit; } // Exit if accessed directly

/**
 * Main class plugin
 */
class MoverthemeElementorPlugin {

	/**
	 * @var Plugin
	 */
	private static $_instance;

	/**
	 * @var Manager
	 */
	private $_modules_manager;

	/**
	 * @var array
	 */
	private $_localize_settings = [];

	/**
	 * @return string
	 */
	public function get_version() {
		return ELEMENTOR_MOVERTHEME_VERSION;
	}

	/**
	 * Throw error on object clone
	 *
	 * The whole idea of the singleton design pattern is that there is a single
	 * object therefore, we don't want the object to be cloned.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function __clone() {
		// Cloning instances of the class is forbidden
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'movertheme-elementor' ), '1.0.0' );
	}

	/**
	 * Disable unserializing of the class
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function __wakeup() {
		// Unserializing instances of the class is forbidden
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'movertheme-elementor' ), '1.0.0' );
	}

	/**
	 * @return Plugin
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	private function _includes() {
        require ELEMENTOR_MOVERTHEME_PATH . 'classes/utils.php';
        require ELEMENTOR_MOVERTHEME_PATH . 'classes/ControlTypeicon.php';
        require ELEMENTOR_MOVERTHEME_PATH . 'includes/modules-manager.php';
	}

	public function autoload( $class ) {
		if ( 0 !== strpos( $class, __NAMESPACE__ ) ) {
			return;
		}

		$filename = strtolower(
			preg_replace(
				[ '/^' . __NAMESPACE__ . '\\\/', '/([a-z])([A-Z])/', '/_/', '/\\\/' ],
				[ '', '$1-$2', '-', DIRECTORY_SEPARATOR ],
				$class
			)
		);
		$filename = ELEMENTOR_MOVERTHEME_PATH . $filename . '.php';

		if ( is_readable( $filename ) ) {
			include( $filename );
		}
	}

	public function get_localize_settings() {
		return $this->_localize_settings;
	}

	public function add_localize_settings( $setting_key, $setting_value = null ) {
		if ( is_array( $setting_key ) ) {
			$this->_localize_settings = array_replace_recursive( $this->_localize_settings, $setting_key );

			return;
		}

		if ( ! is_array( $setting_value ) || ! isset( $this->_localize_settings[ $setting_key ] ) || ! is_array( $this->_localize_settings[ $setting_key ] ) ) {
			$this->_localize_settings[ $setting_key ] = $setting_value;

			return;
		}

		$this->_localize_settings[ $setting_key ] = array_replace_recursive( $this->_localize_settings[ $setting_key ], $setting_value );
	}

	public function enqueue_styles() {
		$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

		$direction_suffix = is_rtl() ? '-rtl' : '';

		wp_enqueue_style(
			'movertheme-elementor',
			ELEMENTOR_MOVERTHEME_URL . 'assets/css/frontend' . $direction_suffix . $suffix . '.css',
			[],
			MoverthemeElementorPlugin::instance()->get_version()
		);
	}
    public function enqueue_styles_globally() {

        wp_enqueue_style(
            'elementor-movertheme-global',
            ELEMENTOR_MOVERTHEME_URL . 'assets/css/common/movertheme-elementor.css',
            [],
            MoverthemeElementorPlugin::instance()->get_version()
        );
    }
	public function enqueue_scripts() {
		$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

		wp_enqueue_script(
			'elementor-movertheme-js',
			ELEMENTOR_MOVERTHEME_URL . 'assets/js/frontend' . $suffix . '.js',
			[
				'jquery',
			],
			MoverthemeElementorPlugin::instance()->get_version(),
			true
		);
		
		wp_localize_script(
			'elementor-movertheme-js',
			'MoverthemeElementorFrontendConfig', // This is used in the js file to group all of your scripts together
			[
				'ajaxurl' => admin_url( 'admin-ajax.php' ),
				'nonce' => wp_create_nonce( 'elementor-movertheme-js' ),
			]
		);
	}
	public function enqueue_script_globally() {

		wp_enqueue_script(
			'elementor-movertheme-common-js',
			ELEMENTOR_MOVERTHEME_URL . 'assets/js/movertheme-common.js',
			[
				'jquery',
			],
			MoverthemeElementorPlugin::instance()->get_version(),
			true
		);
	}
	public function enqueue_panel_scripts() {
		wp_enqueue_script(
			'elementor-movertheme-editor-js',
			ELEMENTOR_MOVERTHEME_URL . 'assets/js/movertheme-elementor-editor.js',
			[
				'jquery',
			],
			MoverthemeElementorPlugin::instance()->get_version(),
			true
		);
		wp_enqueue_script(
			'elementor-movertheme-common-js',
			ELEMENTOR_MOVERTHEME_URL . 'assets/js/movertheme-common.js',
			[
				'jquery',
			],
			MoverthemeElementorPlugin::instance()->get_version(),
			true
		);

		//wp_enqueue_script( 'wp-api' );
	}

	public function enqueue_panel_styles() {
		$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
	}

	public function elementor_init() {

		/*Change elementor global scheme*/
		add_filter( 'pre_update_option_elementor_scheme_typography', function( $new_typo, $old_value ) {
		    $new_typo = array (
		        1 =>
		            array (
		                'font_family' => 'Montserrat',
		                'font_weight' => '700',
		            ),
		        2 =>
		            array (
		                'font_family' => 'Montserrat',
		                'font_weight' => '600',
		            ),
		        3 =>
		            array (
		                'font_family' => 'Open Sans',
		                'font_weight' => '400',
		            ),
		        4 =>
		            array (
		                'font_family' => 'Open Sans',
		                'font_weight' => '400',
		            ),
		    );
		    return $new_typo;
		}, 10, 2);
		add_filter( 'pre_update_option_elementor_scheme_color', function( $new_color, $old_value ) {
		    $new_color = array (
		        1 => '#183c55',
		        2 => '#edf2f6',
		        3 => '#183c55',
		        4 => '#35a6df',
		    );
		    return $new_color;
		}, 10, 2);

		$this->_modules_manager = new Manager();

		// Add element category in panel
		\Elementor\Plugin::instance()->elements_manager->add_category(
			'elementor-movertheme-widgets', // This is the name of your addon's category and will be used to group your widgets/elements in the Edit sidebar pane!
			[
				'title' => __( 'movertheme Widgets', 'movertheme-elementor' ), // The title of your modules category - keep it simple and short!
				'icon' => 'font',
			],
			1
		);


	}

	protected function add_actions() {
		add_action( 'elementor/init', [ $this, 'elementor_init' ] );

		$this->add_builder_common_script();
		/*MoverthemeElementor Common Style*/
		add_action( 'elementor/editor/before_enqueue_styles', [ $this, 'enqueue_styles_globally' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_styles_globally' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_styles_globally' ], 9 );

		/*MoverthemeElementor Common Scripts*/
		add_action( 'elementor/frontend/after_register_scripts', [ $this, 'enqueue_script_globally' ], 998 );

		add_action( 'elementor/frontend/before_enqueue_scripts', [ $this, 'enqueue_scripts' ], 998 );
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_styles' ], 998 );

		add_action( 'elementor/editor/after_enqueue_scripts', [ $this, 'enqueue_panel_scripts' ], 997 );

		/*add_action( 'elementor/element/movertheme-slider/content_section/before_section_end', function( $element, $section_id ) {
			$element->add_control(
				'image_advanced_caption',
				[
					'label'        => 'Didascalia avanzata XXgry',
					'type'         => Controls_Manager::WYSIWYG
				]
			);
		}, 10, 2 );*/
		/*add_filter( 'elementor/global/settings/additional_controls', function( $controls ) {
			$controls[ Controls_Manager::TAB_STYLE ] = [
				'custom_section' => [ //Section ID
					'label' => 'Movertheme Elementor', //Section Label
					'controls' => [
						'custom_text_control' => [ //Control ID
							'label' => 'Text',
							'type' => Controls_Manager::TEXT,
						],
					],
				],
			];
			return $controls;
		} );

		add_action( 'elementor/element/movertheme-teammember/teammember_title_style/before_section_end', function( $element, $args ) {
			$element->start_injection( [
				'at' => 'after',
				'of' => 'elementor_movertheme_title_color',
			] );
			// add a control
			$element->add_control(
				'team_member_name_hover_color',
				[
					'label' => 'Hover Color',
					'type' => \Elementor\Controls_Manager::COLOR,
					'scheme' => [
						'type' => \Elementor\Scheme_Color::get_type(),
						'value' => \Elementor\Scheme_Color::COLOR_4,
					],
					'selectors' => [
						'{{WRAPPER}} .profile-card .fig-title a:hover' => 'color: {{VALUE}}',
					],
				]
			);

			$element->end_injection();
		}, 10, 2 );

		

		add_action('elementor/element/icon-list/section_text_style/before_section_end', function ($instance) {
			$controls = $instance->get_controls();
			$controls['text_color']["default"] = "#183c55";
			$instance->update_control('text_color', $controls['text_color']);

			$controls['icon_color']["default"] = "#36a4de";
			$instance->update_control('icon_color', $controls['icon_color']);

			$controls['icon_typography_font_family']["default"] = "Slabo 27px";
			$instance->update_control('icon_typography_font_family', $controls['icon_typography_font_family']);
		});*/
	}

	public function add_builder_common_script() {
		add_action( 'elementor/frontend/after_register_scripts', [ $this, 'enqueue_builder_common_script' ], 998 );
		add_action( 'elementor/editor/after_enqueue_scripts', [ $this, 'enqueue_builder_common_script' ], 997 );
	}

	public function enqueue_builder_common_script() {
		wp_enqueue_script( 'jquery-accordion-scripts', ELEMENTOR_MOVERTHEME_ASSETS_URL_JS . 'vendor/jquery.accordion.js'  , '', '', true );
	}
	/**
	 * Plugin constructor.
	 */
	private function __construct() {
		spl_autoload_register( [ $this, 'autoload' ] );

		$this->_includes();
		$this->add_actions();
	}
	
}

if ( ! defined( 'ELEMENTOR_MOVERTHEME_TESTS' ) ) {
	// In tests we run the instance manually.
	MoverthemeElementorPlugin::instance();
}


