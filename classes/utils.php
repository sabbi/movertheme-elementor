<?php
namespace MoverthemeElementor\Classes;
use WP_Query;

if ( ! defined( 'ABSPATH' ) )  exit; // Exit if accessed directly
class Utils {

	public static function movertheme_get_all_posttype() {
		$post_types = get_post_types( array( 'public' => true ), 'objects' );

		$options = [ '' => '' ];

		foreach ( $post_types as $post_type ) {
			$exclude = array( 'My Templates', 'Media', 'Pages', 'Menu Items' );

			if ( true === in_array( $post_type->label, $exclude ) ) {

			} else {
				$options[ $post_type->name ] = $post_type->label;
			}
		}

		return apply_filters( 'elementor_movertheme_get_all_posttype', $options );
	}

	public static function movertheme_get_terms_by_posttype__taxonomies( $args = array(), $taxonomies ) {
		//Parse $args in case its a query string.
		$args = wp_parse_args( $args );

		if ( ! empty( $args['post_types'] ) ) {
			$args['post_types'] = (array) $args['post_types'];
		}
		$taxonomi_terms = get_terms( $taxonomies, $args );

		$options = [ 'NULL' => '-- Select Category --' ];
		if ( is_array( $taxonomi_terms ) ) {
			foreach ( $taxonomi_terms as $taxonomi_term ) {
				$options[ $taxonomi_term->term_id ] = $taxonomi_term->name;
			}
		}

		return apply_filters( 'elementor_movertheme_get_terms_by_posttype__taxonomies', $options );
	}

	public static function movertheme_get_postId_list_by_team($post_type = 'team') {

		$post_args = array(
			'post_type'      => $post_type,
			'post_status'    => 'publish',
			'posts_per_page' => '-1',
		);

		$teamMember = array();
		$loop       = new WP_Query( $post_args );
		while ( $loop->have_posts() ) : $loop->the_post();
			$teamMember[ get_the_ID() ] = get_the_title();
		endwhile; wp_reset_postdata();

		return $teamMember;
	}

	public static function movertheme_get_team_member() {

	}

	public static function movertheme_get_team_option_fields() {
		$fields_option = array(
			'select_none' => 'Select a profile section',
			'basic_and_bio' => 'Basic Info And Biography',
			'education_timeline' => 'Education',
			'pro_experience' => 'Professional Experience',
			'awards_prizes' => 'Awards Prizes',
		);

		return apply_filters( 'elementor_movertheme_get_team_option_fields', $fields_option );
	}


	public static function get_placeholder_image_src($size = null) {
		$placeholder_image = ELEMENTOR_MOVERTHEME_ASSETS_URL . 'img/kaloHeem__place-holder-image.jpg';
		return apply_filters( 'elementor_movertheme_placeholder_image', $placeholder_image );
	}


	public static function get_excerpt($limit, $source = null){

		$excerpt = $source == "content" ? get_the_content() : get_the_excerpt();
		$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
		$excerpt = strip_shortcodes($excerpt);
		$excerpt = strip_tags($excerpt);
		$excerpt = substr($excerpt, 0, $limit);
		$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
		$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
		return $excerpt;
	}
	/**
	 * Prints HTML with meta information for the current post-date/time.
	 */
	public static function movertheme_core_posted_on($icon = false) {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( DATE_W3C ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( DATE_W3C ) ),
			esc_html( get_the_modified_date() )
		);
		$posted_on = sprintf(
		/* translators: %s: post date. */
			esc_html_x( '%s', 'post date', 'medica-core' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);
		if ($icon === TRUE) {
			echo '<i class="far fa-calendar"></i> <span class="posted-on  byline-item">' . $posted_on . '</span>'; // WPCS: XSS OK.
		}else {
			echo '<span class="posted-on  byline-item">' . $posted_on . '</span>'; // WPCS: XSS OK.
		}
	}

	/**
	 * Prints HTML with meta information for the current post-date/time.
	*/
	public static function movertheme_core_post_comments($all_numeric = false) { ?>
		<span class="byline-comment byline-item">
            <span class="comment_meta">
                <i class="far fa-comments"></i>
                <?php
				if ($all_numeric === TRUE) {
					comments_popup_link(
						__( '0','medica-core' ), // No comments exist, you would probably want to display a link here in order for people to add the first comment
						__( '1', 'medica-core' ), // 1 comment, usually phrased differently
						__( '%', 'medica-core' ) // > 1 comment
					);
				}else {
					comments_popup_link(
						__( 'No Comments','medica-core' ), // No comments exist, you would probably want to display a link here in order for people to add the first comment
						__( '1 Comment', 'medica-core' ), // 1 comment, usually phrased differently
						__( '% Comments', 'medica-core' ) // > 1 comment
					);
				}
				?>
            </span>
        </span>
	<?php }
}
?>
