<?php
namespace MoverthemeElementor;
use Elementor;
/**
 * Class ControlTypeicon
 *
 * @package \\${NAMESPACE}
 */
/**
 * A Font Icon select box.CASE27_Elementor
 */
class Elementor_Movertheme_Control_Icon extends Elementor\Base_Control {
    public function get_type() {
        return 'icon';
    }
    public static function get_icons() {
        $icons = [];
        // Get arrays of icons.
        $font_awesome_icons = require ELEMENTOR_MOVERTHEME_PATH . 'includes/custom-icon/font-awesome.php';
        $material_icons = require ELEMENTOR_MOVERTHEME_PATH . 'includes/custom-icon/ionicon-icons.php';
        //$custom_icons = require '/path/to/custom-icons.php';
        foreach ($font_awesome_icons as $icon) {
            $icons["fa {$icon}"] = str_replace('fa-', '', $icon);
        }
        foreach ($material_icons as $icon) {
            $icons["movertheme-icons {$icon}"] = $icon;
        }
        /*foreach ($custom_icons as $icon) {
            $icons[$icon] = str_replace('icon-', '', $icon);
        }*/

        return $icons;
    }
    protected function get_default_settings() {
        return [
            'icons' => self::get_icons(),
        ];
    }
    public function content_template() {
        ?>
        <div class="elementor-control-field">
            <label class="elementor-control-title">{{{ data.label }}}</label>
            <div class="elementor-control-input-wrapper">
                <select class="elementor-control-icon" data-setting="{{ data.name }}" data-placeholder="<?php _e( 'Select Icon', 'movertheme-elementor' ); ?>">
                    <option value=""><?php _e( 'Select Icon', 'movertheme-elementor' ); ?></option>
                    <# _.each( data.icons, function( option_title, option_value ) { #>
                        <option value="{{ option_value }}">{{{ option_title }}}</option>
                        <# } ); #>
                </select>
            </div>
        </div>
        <# if ( data.description ) { #>
            <div class="elementor-control-field-description">{{ data.description }}</div>
            <# } #>
        <?php
    }
}
add_action('elementor/controls/controls_registered', function($el) {
    $el->register_control('icon', new Elementor_Movertheme_Control_Icon);
});

/**
 * Add Font Group
 */
/*add_filter( 'elementor/fonts/groups', function( $font_groups ) {
    $font_groups['esx_fonts'] = __( 'Elementor Movertheme Fonts' );
    return $font_groups;
} );*/
/**
 * Add Group Fonts
 */
/*add_filter( 'elementor/fonts/additional_fonts', function( $additional_fonts ) {
    // Key/value
    //Font name/font group
    $additional_fonts['Slabo 27px'] = 'esx_fonts';
    $additional_fonts['Mina'] = 'esx_fonts';
    return $additional_fonts;
} );*/
